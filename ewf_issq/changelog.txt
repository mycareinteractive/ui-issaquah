

20120130-1000 bsears

playvideo.js
navigation.js

added code to manage access to the BOOKMARK button from the 'All Programs' function. if the video is already bookmarked: highlight the bookmark button, alter navigation to ignore it, and make the play button active.

+----------

20120130-1030 bsears 

html.js

when more is selected to scroll to additional videos in 'All Programs', the video list jumps to the right. removed two lines of code that added an extra div.

+----------

20120201-1230 bsears

index.css

widened description field; pushing nutrients to the right to accomodate removal of cholesterol

+----------

20120201-0200 bsears

getXML.js

implement place order xml call

+----------

20120202-0130 bsears

index.css
html.js
navigation.js

high priority change: dine exit confirmation panel(s)

+----------

20120203-1100 bsears

index.css
html.js
navigation.js

high priority change: view order button additions and changes

+----------

20120203-0200 bsears

index.css
html.js
navigation.js

high priority change: implement place order confirmation panel

+----------

20120206-0900 bsears

html.js
navigation.js

high priority change: implement previous/next scrolling for view order

+----------

20120206-1000 bsears

html.js
navigation.js
index.css

high priority change: implement previous/next scrolling for publishing groups

+----------

20120207-0900 bsears

getXML.js

dine issue via Tami: correct the menu id value for place order

+----------

20120207-1000 bsears

html.js
navigation.js
index.css

dine issue via Tami: display error panel if place order submission fails

+----------

20120209-1000 bsears

menu.js
index.css

low priority change: textual changes on Visit->About Swedish->Dining pages

+----------

20120210-0900 bsears

navigation.js

dine issue via Tami: view order, return to same recipe after quantity change

+----------

20120210-1400 bsears

getXML.js

dine issue via Tami: compare call info with returned PATRONINFO info

+----------

20120212-1700 bsears

getXML.js

dine issue via Tami: error processing when html error is returned vs. xml with errors

+----------

20120214-0800 bsears

navigation.js

dine issue via Tami: change right/left scrolling to traverse buttons when on buttons

+----------

20120214-1300 bsears

menus.js
watchtv.js
playtv.js

change via Dennis: activate musicradio

+----------

20120215-0830 bsears

navigation.js

dine issue via Tami: change the timer for discharge from 30 minutes to 2 minutes

+----------

20120215-0900 bsears

getJSON.js

change via Dennis: correct problem that excludes channel 10

+----------

20120215-1130 bsears

index.html
getJSON.js

change via Dennis: implement use of channel lineup

+----------

20120215-1700 bsears

html.js
index.css

dine issue via Tami: prev/next goes green with vieworder

+----------

20120216-0900 bsears

getXML.js
index.html
navigation.js

dine issue via Tami: implement unlock menu

+----------

20120216-1300 bsears

getJSON.js
playtv.js

change via Dennis: implement use of music channels 108,109,110 (music only logic)

+----------

20120216-1500 bsears

getJSON.js

change via Dennis: correct use of panel/grid for epg data access

+----------

20120217-0800 bsears

navigation.js
html.js

dine issue via Tami: implement menu for exit dine

+----------

20120217-1300 bsears

getXML.js

dine issue via Tami: check for hasselections=1

+----------

20120223-0700 bsears

navigation.js

dine issue via Tami: clear nutrition panel when not on recipe

+----------

20120223-0730 bsears

html.js
index.css

dine issue via Dennis: swap Previous/Next button positions

+----------

20120223-0800 bsears

navigation.js

dine issue via Dennis: enable left/right arrows when on previous/next buttons

+----------

20120223-0900 bsears  REVERSED 04/21/2012 @ 12:00p

watchtv.js
navigation.js
getJSON.js
index.html

change via Dennis: load/reload channel group/lineup on access to watchtv, musicradio and scenictv

+----------

20120228-1100 bsears

navigation.js
getJSON.js
index.html

change via Dennis: load/reload movie data every time movies are accessed

+----------

20120228-1130 bsears

commonJSON.js
getXML.js

dine change via Tami: define dine xml call ports as variables and retrieve dynamically at call time

+----------

20120229-1700 bsears

navigation.js

dine issue via Dennis: remove animation to reduce latency flashing

+----------

20120315-1930 bsears

commonJSON.js
getXML.js

dine change via Tami: prevent ordering after stop time variable

+----------