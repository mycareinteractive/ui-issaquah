window.portalConfig = {
    "host": "/",
    "method":"stbservlet.php",
    "proxy":"Proxy/ServerProxy",
    "dinehost":"http://10.209.120.11",
    "dinehostTest":"http://10.209.120.11",
// CHANGE BEG: 20120228-1130 bsears - dine change via Tami: define dine xml call ports as variables and retrieve dynamically at call time
// Test ports
//          "patron_info":"8005",
//          "request_patron_menu":"8006",
//          "request_guest_menu":"8007",
//          "select_patron_menu":"8008",
//          "order_guest_tray":"8009",
//          "unlock_patron_menu":"8010",
// Live ports
    "patron_info":"8025",
    "request_patron_menu":"8026",
    "request_guest_menu":"8027",
    "select_patron_menu":"8028",
    "order_guest_tray":"8029",
    "unlock_patron_menu":"8030",
    
// CHANGE END: 20120228-1130 bsears - dine change via Tami: define dine xml call ports as variables and retrieve dynamically at call time
// CHANGE BEG: 20120315-1930 bsears - dine change via Tami: prevent ordering after stop time variable
    "dineStop":"2000",
// CHANGE END: 20120315-1930 bsears - dine change via Tami: prevent ordering after stop time variable
    "applicationUID":"60010001",
    "nodeGroup":"1",
    "VODRootHUID":"LABN3",
    "regionChannelGroup":"1",
    "movies":"LABN11",
    "allprograms":"LABN12",     
    "RTSPTransport":"MP2T/AVP/UDP",
    "ServiceGroup":"1",
    "Poster": "/Poster/",
    "epgFilePath": "/stbservlet?attribute=json_test_epg&time=",
    "EPGChannelType": "0",  
    "EPGChannelFrequency": "57000", 
    "EPGChannelProgramNumber": "2", 
    "EPGChannelQAMMode": "256",
    "EPGProgramSelectionMode": "PATProgram",
    "scenictvChannel":"1"
};
