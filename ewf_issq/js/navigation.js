//
// navigation functions
//

function discharged()	{
	var version = $("#K_version").text();
	if (version=='PROD')	{
		//Nimbus.reload(true);
	}
	window.location = 'discharged.html';		
}

function reloadapp()	{
	var version = $("#K_version").text();
	if (version=='PROD')	{
		Nimbus.reload(true);
	}
	window.location = 'index.html';		
}

function keypressed(keyCode)	{
	
	var keys = Array();
	
	keys[13] = 'ENTER';
	keys[36] = 'HOME';
	keys[37] = 'LEFT';
	keys[38] = 'UP';
	keys[39] = 'RIGHT';
	keys[40] = 'DOWN';
	keys[33] = 'CHDN';
	keys[34] = 'CHUP';
	
	keys[48] = '0';
	keys[49] = '1';
	keys[50] = '2';
	keys[51] = '3';
	keys[52] = '4';
	keys[53] = '5';
	keys[54] = '6';
	keys[55] = '7';
	keys[56] = '8';
	keys[57] = '9';	
	
	keys[61446] = 'ENTER';
	keys[61444] = 'LEFT';
	keys[61442] = 'UP';
	keys[61445] = 'RIGHT';
	keys[61443] = 'DOWN';
	
	keys[61447] = 'MENU';
	
	keys[61441] = 'POWR';	// power
	keys[61507] = 'POWR';	// power
	keys[61508] = 'POWR';	// power

	keys[61521] = 'CC';		// closed caption
	
	keys[61483] = 'CHUP';	// channel+
	keys[61484] = 'CHDN';	// channel-
	//keys[61449] = 'VOLU';	// volume+
	//keys[61448] = 'VOLD';	// volume-
	
	keys[61464] = 'PLAY';	// pause
	keys[61465] = 'STOP';	// stop
	keys[61464] = 'PAUS';	// pause
	keys[61467] = 'RWND';	// rewind
	keys[61468] = 'FFWD';	// fast forward		
	
	var key = keys[keyCode];
	
	if (!key)	{
		msg('keypress not processed... Actual Value: ' + keyCode + ' Translated Value: ' + key);
		return false;
	}
		
	var panel   = $("#K_panel").attr("class");
	var version = $("#K_version").text();
	
	if (version=='PROD')	{
		if (keyCode==61464||keyCode==61465||keyCode==61467||keyCode==61468||keyCode==61447)	{
			msg('KEYPRESSED Actual Value: ' + keyCode + ' Translated Value: ' + key );	
		}
	}
	
	if (key=='POWR')	{
		
		var onoff = checkPower();
		
		if (onoff=='ON')	{
		
			if (panel=='video')	{
				
				stopVideo();
				$("#K_panel").removeClass('video');
				$("#K_panel").addClass('tertiary');
				$("#tertiaryLeft").show();
				$("#tertiaryRight").show();		
				$("#tertiary").show();	
				
			}	else
				if (panel=='tv')		{
					
					stopTV();
					var grid = $("#K_grid").attr("class");
					$("#K_panel").removeClass('tv');
					$("#K_panel").addClass(grid);
					$("#watchtv").show();
					rebuildgrid();
							
				}	else
					if (panel=='scenictv')		{
						
						stopTV();
						$("#K_grid").removeClass('scenictv');
						$("#K_panel").removeClass('scenictv');
						$("#K_panel").addClass('primary');
						$("#primary").show();
						
					}
					
		}	
		
//		var onoff = $("#K_power").text();
//		if (version=='PROD')	{	
//			var TVController = Nimbus.getTVController();
//			if (TVController != null) {
//				if (TVController.getPower()) {
//				//if (onoff=='ON')	{
//					if (panel=='video')	stopVideo();
//					if (panel=='tv'||panel=='scenictv') stopTV();
//
//					reloadapp();	// remove menu reload
//				}
//				//var power = togglePower();
//				//reloadapp();
//			}
//		}
		return false;	
	}
	
	if (key=='CC')	{
		setCC();
		return true;	
	}

	if (key=='MENU'||key=='HOME')	{
		if (panel=='video')	stopVideo();
		if (panel=='tv'||panel=='scenictv')	stopTV();
		if (panel=='video')	{		
			$("#K_panel").removeClass('video');
			$("#K_panel").addClass('tertiary');
			$("#tertiaryLeft").show();
			$("#tertiaryRight").show();		
			$("#tertiary").show();	
			return true;
		}	else
			if (panel=='tv')	{		
				var grid = $("#K_grid").attr("class");
				$("#K_panel").removeClass('tv');
				$("#K_panel").addClass(grid);
				$("#watchtv").show();
				rebuildgrid();
				return true;
			}	else
				if (panel=='scenictv')	{
					$("#K_grid").removeClass('scenictv');
					$("#K_panel").removeClass('scenictv');
					$("#K_panel").addClass('primary');
					$("#primary").show();
					return true;
				}	else
					if (panel=='watchtv')	{
						cleanupTV();
						$("#K_panel").removeClass('watchtv');
						$("#K_panel").addClass('primary');
						$("#K_panel").text('');
						$("#watchtv").hide();
						$("#primary").show();
						return true;
					}	else
						if (panel=='musicradio')	{
							cleanupTV();
							$("#K_panel").removeClass('musicradio');
							$("#K_panel").addClass('primary');
							$("#K_panel").text('');
							$("#watchtv").hide();
							$("#primary").show();
							return true;
						}

// CHANGE BEG: 20120217-0800 bsears - dine issue via Tami: implement menu for exit dine
		var subpanel = $("#K_subpanel").attr("class");
		var step = $("#K_subpanel").text();
		if (subpanel=='dine')
			if (step=='step1'||step=='step2'||step=='step3'||step=='stepA'||step=='stepC'||step=='order')
				menudineexit();		
// CHANGE END: 20120217-0800 bsears - dine issue via Tami: implement menu for exit dine

		return true;	// remove to enable MENU/HOME functionality

		var indexHTML = initIndexHTML();
		$("#kookies").html(indexHTML.kookies);
		$("#secondary").html(indexHTML.secondary);
		$("#tertiary").html(indexHTML.tertiary);
		$("#watchtv").html(indexHTML.watchtv);
		$("#dine").html(indexHTML.dine);
		
		var tag = $("#secondary").attr("class");
		$("#secondary").removeClass(tag);
		tag = $("#tertiary").attr("class");
		$("#tertiary").removeClass(tag);
		tag = $("#dine").attr("class");
		$("#dine").removeClass(tag);
		
		$("#K_panel").addClass('primary');

		$("#dine").hide();
		$("#tertiary").hide();
		$("#secondary").hide();
		$("#primary").show();						
			
//		reloadapp();	// remove menu reload
		return true;
	}	
	
	if (panel=='primary')	{
	
		var currmenu    = $("#K_menu").attr("class");
		var currsubmenu = $("#K_submenu").attr("class");
		
		if (currmenu!='')	{
		
			if (key=='LEFT')	{
				var menu = $("#menuitem."+currmenu).attr("rev");
				gotomenu(menu);
			}	else
				if(key=='RIGHT')	{
					var menu = $("#menuitem."+currmenu).attr("rel");
					gotomenu(menu);
				}	else
					if(key=='UP')	{
						if (currsubmenu!='')	{
							var currsubmenu = $("#submenuitem."+currsubmenu).attr("rev");
						}
						gotosubmenu(currsubmenu);
					}	else
						if(key=='DOWN')	{
							if (currsubmenu!='')	{
								var currsubmenu = $("#submenuitem."+currsubmenu).attr("rel");
							}
							gotosubmenu(currsubmenu);
						}		else
							if(key=='ENTER')	{
								if (currsubmenu)
									gotosecondary();
							}
												
			return true;
		}

		var menu = $("#K_menufirst").attr("class");
		
		gotomenu(menu);
		
		return true;
	}
	
	
	if (panel=='secondary')	{
		
		var subpanel   = $("#K_subpanel").attr("class");
		var submenu    = $("#K_submenu").attr("class");
		var currchoice = $("#K_choice").attr("class");
		
		if (subpanel=='selections')	{
		
			var currselection = $("#K_selection").attr("class");
			
			if(key=='UP')	{
				var selection = $("#selection."+currselection).attr("rev");
				gotoselections(selection);
			}	else
				if(key=='DOWN')	{
					var selection = $("#selection."+currselection).attr("rel");
					gotoselections(selection);
				}		else
					if(key=='LEFT')	{
						removeselections();
					}		else
						if(key=='ENTER')	{
							gotoselection(currselection);
						}
			return true;			
		}
		
		if (subpanel=='dine')	{
			
			var step = $("#K_subpanel").text();
			
			if (step=='step1')	{
			
				if(key=='UP'||key=='DOWN')	{
					nextdine(key);
					}	else
						if(key=='ENTER'||key=='LEFT'||key=='RIGHT')	{
							stepdine(key);
						}
						
			}	else
			if (step=='step2')	{
			
				if(key=='UP'||key=='DOWN'||key=='RIGHT')	{
					nextdine(key);
					}	else
						if(key=='ENTER'||key=='LEFT')	{
							stepdine(key);
						}	
						
			}	else
			if (step=='step3')	{
			
				if(key=='UP'||key=='DOWN'||key=='RIGHT')	{
					nextdine(key);
					}	else
						if(key=='ENTER'||key=='LEFT')	{
							stepdine(key);
						}	
						
			}	else
			if (step=='stepX')	{
			
				if(key=='ENTER')	{
					stepdine();
				}	
				
			}	else
			if (step=='stepA')	{
				
				if(key=='UP')	{
					nextgroup(key);
				}	else
					if(key=='DOWN')	{
						nextgroup(key);
					}	else										
						if(key=='ENTER'||key=='LEFT'||key=='RIGHT')	{
							selectgroup(key);
						}	
						
			}	else
//			if (step=='stepB')	{
//				
//				if(key=='UP')	{
//					nextmenus(key);
//				}	else
//					if(key=='DOWN')	{
//						nextmenus(key);
//					}	else										
//						if(key=='ENTER'||key=='LEFT'||key=='RIGHT')	{
//							selectmenus(key);
//						}
//						
//			}	else
			if (step=='stepC')	{
				
				if(key=='UP')	{
					nextrecipes(key);
				}	else
					if(key=='DOWN')	{
						nextrecipes(key);
					}	else										
						if(key=='ENTER'||key=='LEFT'||key=='RIGHT')	{
							selectrecipes(key);
						}	
						
			}	else
			if (step=='stepE')	{
								
				if(key=='ENTER'||key=='LEFT')	{
					closeerror();
				}	
						
			}	else
			if (step=='stepW')	{
								
				if(key=='ENTER'||key=='LEFT'||key=='RIGHT')	{
					processexit(key);
				}	
						
			}	else
			if (step=='stepQ')	{
				
				if(key=='UP'||key=='DOWN')	{
					nextquantity(key);
				}	else
					if(key=='LEFT'||key=='RIGHT')	{
						navquantity(key);
					}	else										
						if(key=='ENTER')	{
							selectquantity(key);
						}	
						
			}	else
			if (step=='order')	{
				
// CHANGE BEG: 20120203-1100 bsears - high priority change: view order button additions and changes			
//				if(key=='UP'||key=='DOWN'||key=='LEFT'||key=='RIGHT')	{
//					nextorder(key);
//				}	else		
//					if(key=='ENTER')	{
//						confirmorder(key);
//					}
				if(key=='UP'||key=='DOWN')	{
					nextorder(key);
				}	else		
					if(key=='ENTER'||key=='LEFT'||key=='RIGHT')	{
						confirmorder(key);
					}
// CHANGE END: 20120203-1100 bsears - high priority change: view order button additions and changes
					
// CHANGE BEG: 20120203-0200 bsears - high priority change: implement place order confirmation panel
			}	else
			if (step=='placeorder')	{
				
				if(key=='ENTER'||key=='LEFT'||key=='RIGHT')	{
					placeorder(key);
				}	
// CHANGE END: 20120203-0200 bsears - high priority change: implement place order confirmation panel
					
			}	else
			if (step=='confirm')	{
				
				if(key=='ENTER')	{
					confirmexit();	
				}
				
// CHANGE BEG: 20120202-0130 bsears - high priority change: dine exit confirmation panel(s)			
			}	else
			if (step=='exit')	{
				
				if(key=='ENTER'||key=='LEFT'||key=='RIGHT')	{
					processexit(key);
				}				
// CHANGE BEG: 20120202-0130 bsears - high priority change: dine exit confirmation panel(s)

// CHANGE BEG: 20120217-0800 bsears - dine issue via Tami: implement menu for exit dine
			}	else
			if (step=='menuexit')	{
				
				if(key=='ENTER'||key=='LEFT'||key=='RIGHT')	{
					processexit(key);
				}
// CHANGE END: 20120217-0800 bsears - dine issue via Tami: implement menu for exit dine

// CHANGE BEG: 20120207-1000 bsears - dine issue via Tami: display error panel if place order submission fails			
			}	else
			if (step=='error')	{
				
				if(key=='ENTER')	{
					errorexit(key);
				}				
// CHANGE BEG: 20120207-1000 bsears - dine issue via Tami: display error panel if place order submission fails	
					
			}
					
			return true;			
		}		
		
		if (submenu=='movies'&&(currchoice!='more'&&currchoice!='back'&&currchoice!='mainmenu'))	{
			
			var initiate = $("#K_carousel").text();
			if(initiate=='initiate')	{
				if(key=='RIGHT'||key=='ENTER')	{
					initiateCarousel();
					return true;
				}	else
				if (key=='LEFT')	{
					return true;
				}
			}
			
			if(key=='LEFT'||key=='RIGHT')	{
				shiftCarousel(key);
				return true;
			}	else					
				if(key=='ENTER')	{
					gotoplaymovie();
					return true;
				}
		}
			
		if(key=='UP')	{
			var choice = $("#choice."+currchoice).attr("rev");
			nextchoice(choice);
		}	else
			if(key=='DOWN')	{
				var choice = $("#choice."+currchoice).attr("rel");
				nextchoice(choice);
			}	else					
				if(key=='LEFT')	{
					nextpanel(key);
				}	else					
					if(key=='RIGHT')	{
						nextpanel(key);
					}	else		
						if(key=='ENTER')	{
							gotochoice();
						}

		return true;					
	}
	
	
	if (panel=='tertiary')	{
	
		var currcontrol = $("#K_control").attr("class");
		
		if(key=='UP')	{
			var control = $("#control."+currcontrol).attr("rev");
			gotocontrols(control);
		}	else
			if(key=='DOWN')	{
				var control = $("#control."+currcontrol).attr("rel");
				gotocontrols(control);
			}		else
				if(key=='LEFT'||key=='RIGHT')	{
					nextmovie(key);
					return true;
				}	else	
					if(key=='ENTER')	{
						gotocontrol();
					}
			
		return true;						
	}

//	if (panel=='watchtv'||panel=='musicradio'||panel=='scenictv')	{
	if (panel=='watchtv'||panel=='musicradio')	{	
				
		if (key=='LEFT')	{
			moveCurrent(key);
		}	else
			if(key=='RIGHT')	{
				moveCurrent(key);
			}	else
				if(key=='UP')	{
					moveCurrent(key);
				}	else
					if(key=='DOWN')	{
						moveCurrent(key);
					}		else
						if(key=='ENTER')	{
							gotochannel();
						}
//		if (panel=='scenictv')	{		
//			if(key=='CHUP'||key=='CHDN')	{
//				updnChannel(key);
//			}
//			if (key=='0'||key=='1'||key=='2'||key=='3'||key=='4'||key=='5'||key=='6'||key=='7'||key=='8'||key=='9')	{
//				keyChannel(key);	
//			}	
//		}
			
		return true;						
	}
	
	if (panel=='scenictv')	{	
		if(key=='CHUP'||key=='CHDN')	{
			updnChannel(key);
		}
		if (key=='0'||key=='1'||key=='2'||key=='3'||key=='4'||key=='5'||key=='6'||key=='7'||key=='8'||key=='9')	{
			keyChannel(key);	
		}
		
		return true;
	}	
	
	if (panel=='tv')	{	

		if(key=='CHUP'||key=='CHDN')	{
			updnChannel(key);
		}
		
		if (key=='0'||key=='1'||key=='2'||key=='3'||key=='4'||key=='5'||key=='6'||key=='7'||key=='8'||key=='9')	{
			keyChannel(key);	
		}
			
		return true;						
	}	
	
	if (panel=='video')	{
		
		if (key=='PLAY'||key=='STOP'||key=='PAUS'||key=='RWND'||key=='FFWD')	{
			var submenu = $("#K_submenu").attr("class");
			if (key=='FFWD'&&(submenu=='myprograms'||submenu=='allprograms'))		
				return true;
			updateVideo(key);
			if (key=='STOP')	{
				$("#K_panel").removeClass('video');
				$("#K_panel").addClass('tertiary');
				$("#tertiaryLeft").show();
				$("#tertiaryRight").show();		
				$("#tertiary").show();	
			}			
			return true;
		}
		
		if (key=='ENTER') {
		    togglePictureFormat();
		}
		
	}

	msg('keypress not caught/processed... Actual Value: ' + keyCode + ' Translated Value: ' + key);
	return true;
}


function buildmenu()	{
	
	// initialize index.html
	
	var indexHTML = initIndexHTML();
	$("#kookies").html(indexHTML.kookies);
	$("#secondary").html(indexHTML.secondary);
	$("#tertiary").html(indexHTML.tertiary);
	$("#watchtv").html(indexHTML.watchtv);
	$("#dine").html(indexHTML.dine);
	
	// build menu
	
	$("#K_panel").addClass('primary');
	
	var patientHTML = getPatientHTML();
	$("#patient").html(patientHTML.patient);
	
	checkDischarge();
// CHANGE BEG: 20120215-0830 bsears - dine issue via Tami: change the timer for discharge from 30 minutes to 2 minutes
//	setInterval( "checkDischarge()", 1800000 );
	setInterval( "checkDischarge()", 120000 );
// CHANGE END: 20120215-0830 bsears - dine issue via Tami: change the timer for discharge from 30 minutes to 2 minutes

	datetime();
	setInterval( "datetime()", 60000 );
	
	var menuHTML = getMenuHTML();
	
	$("#K_menufirst").addClass(menuHTML.menufirst);
	$("#K_menufirst").text(menuHTML.menulabel);
	
	$("#K_menu").addClass('');
	
	$("#menu").html(menuHTML.menu);
	$("#menuitems").html(menuHTML.menuitems);
	$("#pictures").html(menuHTML.menuimage);
	
	blink('#pictures #mainpanel img.arrows');
	setInterval( "blink('#pictures #mainpanel img.arrows')", 3000 );
	
	var version = $("#K_version").text();
	if (version=='TEST'||version=='PROD')	{
		var productofferingDATA 	= getProductOfferingDATA();
		var myprogramsDATA 			= getMyProgramsDATA();
		var allprogramsDATA 		= getAllProgramsDATA();	
// CHANGE BEG: 20120228-1100 bsears - change via Dennis: load/reload movie data every time movies are accessed
//		var moviecategoryDATA 		= getMovieCategoryDATA();
// CHANGE END: 20120228-1100 bsears - change via Dennis: load/reload movie data every time movies are accessed
// CHANGE BEG: 20120223-0900 bsears - change via Dennis: load/reload channel group/lineup on access to watchtv, musicradio and scenictv (PENDING)
		var channelsDATA 			= getChannelsDATA();	// need to comment out to implement
// CHANGE END: 20120223-0900 bsears - change via Dennis: load/reload channel group/lineup on access to watchtv, musicradio and scenictv (PENDING)
	}	
	
	$("#loading").fadeOut('slow');	
	$("#primary").fadeIn('slow');
	
	$("#K_versionoverlay").text('');
	
	var pathname = window.location;
	//msg(pathname);
	if (pathname=='http://10.209.120.28:9090/ewf_test_test/index.html')			$("#K_versionoverlay").text('TEST');
	if (pathname=='http://10.209.120.28:9090/ewf_test/index.html')				$("#K_versionoverlay").text('TEST');
	if (pathname=='http://10.209.120.28:9090/ewf_test_test/')					$("#K_versionoverlay").text('TEST');
	if (pathname=='http://10.209.120.28:9090/ewf_test/')						$("#K_versionoverlay").text('TEST');
	
	if (pathname=='http://192.168.100.60:9090/ewf_003/index.html')				$("#K_versionoverlay").text('TEST');
	if (pathname=='http://192.168.100.60:9090/ewf_003/')						$("#K_versionoverlay").text('TEST');
	if (pathname=='http://192.168.100.60:9090/ewf001-test/index.html')			$("#K_versionoverlay").text('TEST');
	if (pathname=='http://192.168.100.60:9090/ewf001-test/')					$("#K_versionoverlay").text('TEST');
	
	if (pathname=='http://192.168.100.60:9090/dine_test_test/index.html')		$("#K_versionoverlay").text('DINE');
	if (pathname=='http://192.168.100.60:9090/dine_test_test/')					$("#K_versionoverlay").text('DINE');
		
	var versionoverlay = $("#K_versionoverlay").text();
	if (versionoverlay)	{
//		$("#testoverlay p").text(versionoverlay);
		$("#testoverlay").text(versionoverlay);
		$("#testoverlay").show();
	}
		
	return;
}

function blink(div)	{
	
	$(div).delay(200).fadeTo(100,0.5).delay(200).fadeTo(100,1).delay(200).fadeTo(100,0.5).delay(200).fadeTo(100,1).delay(200).fadeTo(100,0.5).delay(200).fadeTo(100,1).delay(200).fadeTo(100,0.5);
}

// primary functions...

function gotomenu(menu)	{

	var prevmenu = $("#K_menu").attr("class");
	if (prevmenu != '')	{
		$("#K_menu").removeAttr("class");
		$("#K_menu").text('');
		$("#K_submenu").removeAttr("class");
		$("#K_submenu").text('');
		$("#K_submenufirst").removeAttr("class");
		$("#K_submenufirst").text('');

		$("#menuitem."+prevmenu).removeClass('active');	
		$("#submenu."+prevmenu).html('');
		
		$("#pictures").hide();
	}	else	{
			var menu = $("#K_menufirst").attr("class");
		}
		
	var submenuHTML = getSubmenuHTML(menu);	
		
	$("#K_submenufirst").addClass(submenuHTML.submenufirst);
	$("#K_submenufirst").text(submenuHTML.submenulabel);
		
	$("#K_menu").addClass(menu);
	var menulabel = $("#menuitem."+menu).text();
	$("#K_menu").text(menulabel);
	
	var image = '<img src="images/'+$("#menuitem."+menu).attr("title")+'" />';
	
	$("#menuitem."+menu).addClass('active');
	//$("#pictures").html(image).fadeIn('slow');
	//$("#submenu."+menu).html(submenuHTML.menuitems).show("slide", { direction: "up" }, 500);	
	$("#pictures").html(image).show();
	$("#submenu."+menu).html(submenuHTML.menuitems).show();
	
	return;
}

function gotosubmenu(submenuitem)	{

	var prevsubmenu = $("#K_submenu").attr("class");
	if (prevsubmenu != '')	{
		$("#K_submenu").removeAttr("class");
		$("#K_submenu").text('');
		$("#submenuitem."+prevsubmenu+" span").removeClass('active');
	}	else	{
			var submenuitem = $("#K_submenufirst").attr("class");
		}
		
	$("#K_submenu").addClass(submenuitem);
	var submenulabel = $("#submenuitem."+submenuitem+' span').text();
	$("#K_submenu").text(submenulabel);
	
	var image = '<img src="images/'+$("#submenuitem."+submenuitem).attr("title")+'" />';
	
	//$("#pictures").html(image).fadeIn('slow');
	$("#pictures").html(image).show();
	$("#submenuitem."+submenuitem+" span").addClass('active');
	
	return;
}

// secondary functions...

function gotosecondary()	{

	$("#K_panel").removeClass('primary');
	$("#K_panel").addClass('secondary');
	
	$("#K_choice").removeAttr("class");
	$("#K_choice").text('');
	$("#K_choicefirst").removeAttr("class");
	$("#K_choicefirst").text('');

	var menu = $("#K_menu").attr("class");
	var menulabel = $("#K_menu").text();
	
	var submenu = $("#K_submenu").attr("class");
	var submenulabel = $("#K_submenu").text();
	
	if (submenu=='mymenu'||submenu=='visitorsmenu')	{
		gotodine();	
		return;
	}
	
// CHANGE BEG: 20120228-1100 bsears - change via Dennis: load/reload movie data every time movies are accessed
	if (submenu=='movies')
		var moviecategoryDATA = getMovieCategoryDATA();
// CHANGE END: 20120228-1100 bsears - change via Dennis: load/reload movie data every time movies are accessed
	
	$("#secondaryTitle.menu").html('<p class="menu">'+menulabel+'</p>');	
	$("#secondaryTitle.submenu").html('<p class="submenu">'+submenulabel+'</p>');
	
	var choicesHTML = getChoicesHTML(submenu);
		
	$("#K_panel").text(choicesHTML.background);
	$("#K_choice").addClass(choicesHTML.choicefirst);
	$("#K_choice").text(choicesHTML.choicelabel);
	$("#K_choicefirst").addClass(choicesHTML.choicefirst);
	$("#K_choicefirst").text(choicesHTML.choicelabel);
	
	$("#choices").html(choicesHTML.choices);
	$("#choice."+choicesHTML.choicefirst+" span").addClass('active');
	$("#secondaryRight").html(choicesHTML.choiceimage);
	
	$("#secondaryLeft").hide();
	$("#secondaryRight").hide();
	
	var grid = choicepanel(choicesHTML.choicepanel);
	
	if(grid) return;
	
	$("#secondary").addClass(choicesHTML.background);
	//$("#secondary").animate({ width: 'toggle', opacity: 'toggle' }, 1000);
	//$("#secondaryLeft").delay(800).fadeIn(50);
	//$("#secondaryRight").delay(800).fadeIn(50);
	//$("#primary").fadeOut(1000);
	
	$("#primary").hide();
	$("#secondaryLeft").show();
	$("#secondaryRight").show();
	$("#secondary").show();

}

function morechoices()	{

	$("#K_choice").removeAttr("class");
	$("#K_choice").text('');
	$("#K_choicefirst").removeAttr("class");
	$("#K_choicefirst").text('');

	var submenu = $("#K_submenu").attr("class");
	var choicelast = $("#choice.more").attr("rev");

	var choicesHTML = getChoicesHTML(submenu,choicelast);
	
	$("#K_panel").text(choicesHTML.background);
	$("#K_choice").addClass(choicesHTML.choicefirst);
	$("#K_choice").text(choicesHTML.choicelabel);
	$("#K_choicefirst").addClass(choicesHTML.choicefirst);
	$("#K_choicefirst").text(choicesHTML.choicelabel);
	
	$("#choices").html(choicesHTML.choices);
	$("#choice."+choicesHTML.choicefirst+" span").addClass('active');
	$("#secondaryRight").html(choicesHTML.choiceimage);
		
	choicepanel(choicesHTML.choicepanel);
	
	return;
}

function nextchoice(choice)	{
	
	$("#secondary div#secondaryLeft div#choices a span.active").css("color","#ffffff");

	var prevchoice = $("#K_choice").attr("class");
	
	$("#K_choice").removeAttr("class");
	$("#K_choice").text('');
	$("#choice."+prevchoice+" span").removeClass('active');

	$("#K_choice").addClass(choice);
	var choicelabel = $("#choice."+choice+' span').text();
	$("#K_choice").text(choicelabel);	
	
	$("#choice."+choice+" span").addClass('active');
	if ($("#choice."+choice).attr("title")>'')
		$("#secondaryRight").html('<div id="choicepanel"><img src="images/'+$("#choice."+choice).attr("title")+'" /></div>');
	
	choicepanel(choice);
		
	return;
}

function choicepanel(choice)	{
	
	var options = getChoice(choice);
	
	if (options.tag=='EMPTY1'||options.tag=='EMPTY2'||options.tag=='EMPTY3')	{	
		return;
	}
	
	$("#secondaryRight").css( { "background": "none" } ); 
	
	if (options.type=='panel')	{
		var choiceHTML = getChoiceHTML(choice);
		$("#secondaryRight").html(choiceHTML.choicelist);
	}	else
		if (options.type=='video')	{
			var commentsHTML = getCommentsHTML(choice);
			$("#secondaryRight").html(commentsHTML.choicepanel);
		}	else	
			if (options.type=='image')	{
				var imageHTML = getImageHTML(choice,options);
				$("#secondaryRight").html(imageHTML.choicepanel);
			}	else	
				if (options.type=='carousel')	{
					$("#K_carousel").text('initiate');
					var carouselHTML = getCarouselHTML(choice);
					$("#secondaryRight").html(carouselHTML.carousel);
					var title = $("#carouselPosters img.current").attr("title");
					$("#carouselTitle").html('<p>'+title+'</p>');
				}	else
				
					if (options.type=='info')	{
						var infoHTML = getInfoHTML(options);
						$("#secondaryRight").html(infoHTML.choicepanel);
						$("#secondaryRight").css( { "background-image": "url(./images/"+infoHTML.background+")","background-position": "bottom right","background-repeat": "no-repeat" } ); 
					}	else
					
						if (options.type=='slider')	{
							$("#K_slider").removeAttr("class");		//number of sliders
							$("#K_slider").text('');				//current slider						
							var slidesHTML = getSlidesHTML(options);
							$("#K_slider").addClass(slidesHTML.count);		//number of sliders
							$("#K_slider").text(slidesHTML.active);		//current slider
							$("#secondaryRight").html(slidesHTML.sliders);				
						}	else
					
							if (options.type=='grid')	{
								
								buildgrid(options.choice);

								return options.type;
							}			
				
	return;
}

function refreshChoice()	{

}

function gotochoice()	{

	var currchoice = $("#K_choice").attr("class");
	
	if (currchoice=='more')	{

		morechoices();
		
	}	else
		if (currchoice=='back')	{
			
			var background = $("#K_panel").text();
			
			//$("#secondary").removeClass(background).fadeOut('fast');
			//$("#primary").fadeIn('fast');
			$("#secondary").removeClass(background).hide();
			$("#primary").show();
			$("#K_panel").removeClass('secondary').text('');
			$("#K_panel").addClass('primary');
			var submenu = $("#K_submenu").attr("class");
			gotosubmenu(submenu);
			
		}	else	
			if (currchoice=='mainmenu')	{	

				var background = $("#K_panel").text();
			
				//$("#secondary").removeClass(background).fadeOut('fast');
				//$("#primary").fadeIn('fast');
				$("#secondary").removeClass(background).hide();
				$("#primary").show();
				$("#K_panel").removeClass('secondary').text('');
				$("#K_panel").addClass('primary');
				var menu = $("#K_menufirst").attr("class");
				gotomenu(menu);
				
			}	else	{
			
					var options = getChoice(currchoice);
					
					if (options.tag=='EMPTY1'||options.tag=='EMPTY2'||options.tag=='EMPTY3')	{	
						return;
					}
					
					if (options.type=='video')	{
						gotoplayvideo(options);
						}	else					
							if (options.type=='panel')	{
								buildselections(options);  //change this to drop background...
							}	else	{
									//msg('gotochoice: '+choice+': '+options.type+' not supported');
									return false;
								}	
				}
	
	return;
}

function nextpanel(key)	{
	
	var count  = $("#K_slider").attr("class");
	
	if (count==0)
		return;
	
	var active = $("#K_slider").text();
	var ondeck = active;
	if (key=='LEFT')	{
		ondeck--;
		if (ondeck == 0)
			ondeck = count;
	}	else
		if (key=='RIGHT')	{
			ondeck++;
			if (ondeck > count)
				ondeck = 1;
		}	
	$("#K_slider").text(ondeck);

	$("#secondary div#secondaryRight div#slides div#slide.slide"+active).css("display","none");
	$("#secondary div#secondaryRight div#slides div#slide.slide"+ondeck).css("display","block");

	return;
}

function initiateCarousel()	{
	
	$("#K_carousel").text('');
	$("#secondary div#secondaryLeft div#choices a span.active").css("color","#344e9b");
	$("#secondary div#secondaryRight div#carousel div#carouselOverlay").fadeIn(500);
	$("#secondary div#secondaryRight div#carousel div#carouselMessage").html('<p>Press Select to Watch a Movie</p>');	
}

function shiftCarousel(direction)	{
	
	var ids = Array();
	
	var id   = $("#carouselPosters img.current").attr("id");
	var end  = $("#carouselPosters img.current").attr("id");
	// Swapped the left and right direction so it matches human brain LOL.
	if (direction=='RIGHT')	{
	
		var last = $("#carouselPosters img.last").attr("id");
		end++; end++;
		if (end==last)
			return;

		id--; id--;
		ids[0] = id;
		var i = 0; 
		for(i=1;i<=5;i++)	{
			id++;	
			ids[i] = id;
		}
		moveCarousel(ids,direction);
	
	}	else	
		if (direction=='LEFT')	{
								 
			var last = 0;
			end--; end--;
			if (end==last)
				return;		
				
			id++; id++;
			ids[0] = id;
			var i = 0; 
			for(i=1;i<=5;i++)	{
				id--;	
				ids[i] = id;
			}				
			moveCarousel(ids,direction);				
												 
		}	
		
	var title = $("#carouselPosters img.current").attr("title");
	$("#secondary div#secondaryRight div#carousel div#carouselTitle").html('<p>'+title+'</p>');
	
	var ewf = ewfObject();
	// By default animation is false.  Feedback came saying the carousel is sluggish.
	if(ewf.carouselAnimation == 'true' || ewf.carouselAnimate == 'True') {
    	$("#secondary div#secondaryRight div#carousel div#carouselTitle").delay(500).fadeIn(250);
    	$("#secondary div#secondaryRight div#carousel div#carouselOverlay").delay(500).fadeIn(250);
	}
	else {
	    $("#secondary div#secondaryRight div#carousel div#carouselTitle").show();
        $("#secondary div#secondaryRight div#carousel div#carouselOverlay").show();
	}
	$("#secondary div#secondaryRight div#carousel div#carouselMessage").html('<p>Press Select to Watch a Movie</p>');
	
	return
}

function moveCarousel(ids,direction)	{
	
		$("#secondary div#secondaryRight div#carousel div#carouselTitle").hide();
		$("#secondary div#secondaryRight div#carousel div#carouselOverlay").hide();

		$("#carouselPosters img.currentb").removeClass("currentb");
		$("#carouselPosters img.currenta").removeClass("currenta");
		$("#carouselPosters img.current").removeClass("current");
		$("#carouselPosters img.currenta").removeClass("currenta");
		$("#carouselPosters img.currentb").removeClass("currentb");
		
		
		// By default animation is false.  Feedback came saying the carousel is sluggish.
		// Also swapped the left and right direction so it matches human brain LOL.
		var ewf = ewfObject();
		if(ewf.carouselAnimation == 'true' || ewf.carouselAnimate == 'True')
		{
    		if (direction=='RIGHT')	{
    	
    			$("#carouselPosters img#"+ids[0]).hide().animate({ "margin-top": "75px","margin-left": "0px","width": "1px","height": "1px" }, "slow");
    			$("#carouselPosters img#"+ids[1]).animate({ "margin-top": "75px","margin-left": "0px","width": "75px","height": "103px","padding-top": "3px","padding-right": "3px","padding-bottom": "0px","padding-left": "3px" }, "slow").addClass("currentb");
    			$("#carouselPosters img#"+ids[2]).animate({ "margin-top": "50px","margin-left": "25px","width": "120px","height": "165px","padding-top": "4px","padding-right": "4px","padding-bottom": "0px","padding-left": "4px" }, "slow").addClass("currenta");
    			$("#carouselPosters img#"+ids[3]).animate({ "margin-top": "0px","margin-left": "25px","width": "240px","height": "330px","padding-top": "4px","padding-right": "5px","padding-bottom": "0px","padding-left": "5px" }, "slow").addClass("current");
    			$("#carouselPosters img#"+ids[4]).animate({ "margin-top": "50px","margin-left": "25px","width": "120px","height": "165px","padding-top": "4px","padding-right": "4px","padding-bottom": "0px","padding-left": "4px" }, "slow").addClass("currenta");
    			$("#carouselPosters img#"+ids[5]).show("fast").animate({ "margin-top": "75px","margin-left": "25px","width": "75px","height": "103px","padding-top": "3px","padding-right": "3px","padding-bottom": "0px","padding-left": "3px" }, "slow").addClass("currentb");	
    		
    		}
    		else if (direction=='LEFT')	{
    	
    			$("#carouselPosters img#"+ids[0]).hide().animate({ "margin-top": "75px","margin-left": "0px","width": "1px","height": "1px" }, "slow");
    			$("#carouselPosters img#"+ids[1]).animate({ "margin-top": "75px","margin-left": "25px","width": "75px","height": "103px","padding-top": "3px","padding-right": "3px","padding-bottom": "0px","padding-left": "3px"  }, "slow").addClass("currentb");
    			$("#carouselPosters img#"+ids[2]).animate({ "margin-top": "50px","margin-left": "25px","width": "120px","height": "165px","padding-top": "4px","padding-right": "4px","padding-bottom": "0px","padding-left": "4px" }, "slow").addClass("currenta");
    			$("#carouselPosters img#"+ids[3]).animate({ "margin-top": "0px","margin-left": "25px","width": "240px","height": "330px","padding-top": "4px","padding-right": "5px","padding-bottom": "0px","padding-left": "5px" }, "slow").addClass("current");
    			$("#carouselPosters img#"+ids[4]).animate({ "margin-top": "50px","margin-left": "25px","width": "120px","height": "165px","padding-top": "4px","padding-right": "4px","padding-bottom": "0px","padding-left": "4px" }, "slow").addClass("currenta");
    			$("#carouselPosters img#"+ids[5]).show("fast").animate({ "margin-top": "75px","margin-left": "0px","width": "75px","height": "103px","padding-top": "3px","padding-right": "3px","padding-bottom": "0px","padding-left": "3px" }, "slow").addClass("currentb");	
    		
            }
		}
		else {
		    if (direction=='RIGHT')    {
        
                $("#carouselPosters img#"+ids[0]).hide().css({ "margin-top": "75px","margin-left": "0px","width": "1px","height": "1px" });
                $("#carouselPosters img#"+ids[1]).css({ "margin-top": "75px","margin-left": "0px","width": "75px","height": "103px","padding-top": "3px","padding-right": "3px","padding-bottom": "0px","padding-left": "3px" }).addClass("currentb");
                $("#carouselPosters img#"+ids[2]).css({ "margin-top": "50px","margin-left": "25px","width": "120px","height": "165px","padding-top": "4px","padding-right": "4px","padding-bottom": "0px","padding-left": "4px" }).addClass("currenta");
                $("#carouselPosters img#"+ids[3]).css({ "margin-top": "0px","margin-left": "25px","width": "240px","height": "330px","padding-top": "4px","padding-right": "5px","padding-bottom": "0px","padding-left": "5px" }).addClass("current");
                $("#carouselPosters img#"+ids[4]).css({ "margin-top": "50px","margin-left": "25px","width": "120px","height": "165px","padding-top": "4px","padding-right": "4px","padding-bottom": "0px","padding-left": "4px" }).addClass("currenta");
                $("#carouselPosters img#"+ids[5]).show().css({ "margin-top": "75px","margin-left": "25px","width": "75px","height": "103px","padding-top": "3px","padding-right": "3px","padding-bottom": "0px","padding-left": "3px" }).addClass("currentb");    
            
            }
            else if (direction=='LEFT') {
        
                $("#carouselPosters img#"+ids[0]).hide().css({ "margin-top": "75px","margin-left": "0px","width": "1px","height": "1px" });
                $("#carouselPosters img#"+ids[1]).css({ "margin-top": "75px","margin-left": "25px","width": "75px","height": "103px","padding-top": "3px","padding-right": "3px","padding-bottom": "0px","padding-left": "3px"  }).addClass("currentb");
                $("#carouselPosters img#"+ids[2]).css({ "margin-top": "50px","margin-left": "25px","width": "120px","height": "165px","padding-top": "4px","padding-right": "4px","padding-bottom": "0px","padding-left": "4px" }).addClass("currenta");
                $("#carouselPosters img#"+ids[3]).css({ "margin-top": "0px","margin-left": "25px","width": "240px","height": "330px","padding-top": "4px","padding-right": "5px","padding-bottom": "0px","padding-left": "5px" }).addClass("current");
                $("#carouselPosters img#"+ids[4]).css({ "margin-top": "50px","margin-left": "25px","width": "120px","height": "165px","padding-top": "4px","padding-right": "4px","padding-bottom": "0px","padding-left": "4px" }).addClass("currenta");
                $("#carouselPosters img#"+ids[5]).show().css({ "margin-top": "75px","margin-left": "0px","width": "75px","height": "103px","padding-top": "3px","padding-right": "3px","padding-bottom": "0px","padding-left": "3px" }).addClass("currentb"); 
            
            }
		    
		}		
}

// tertiary functions...

function gotoplayvideo(options)	{
	
	$("#tertiary").removeAttr("class");

	$("#K_panel").removeClass('secondary');
	$("#K_panel").addClass('tertiary');

	$("#K_control").removeAttr("class");
	$("#K_control").text('');
	$("#K_controlfirst").removeAttr("class");
	$("#K_controlfirst").text('');

	var videoHTML = getVideoHTML(options);
	
	$("#tertiaryTitle.menu").html(videoHTML.menulabel);	
	$("#tertiaryTitle.submenu").html(videoHTML.submenulabel);	
		
	$("#K_control").addClass(videoHTML.controlfirst);
	$("#K_control").text(videoHTML.controllabel);
	$("#K_controlfirst").addClass(videoHTML.controlfirst);
	$("#K_controlfirst").text(videoHTML.controllabel);
	
	$("#controls").html(videoHTML.controls);
	$("#controlimage").html(videoHTML.controlimage);
	$("#control."+videoHTML.controlfirst+" span").addClass('active');	
	
// CHANGE BEG: 20120130-1000 bsears - added code to check if bookmarked and adjust buttons (PENDING)
	var submenu = $("#K_submenu").attr("class");					
	if (submenu=='allprograms')	{									
		var video = $("#K_selection").attr("class");				
		var exists = bookmarkCHECK(video);							
		if(exists)	{												
			$("#controls a span.bookmark").removeClass('active');	
			$("#controls a span.bookmark").addClass('working');		
			$("#controls a span.play").addClass('active');			
			$("#controls a.play").attr('rev','mainmenu');			
			$("#controls a.mainmenu").attr('rel','play');			
			$("#K_control").removeAttr("class");					
			$("#K_control").text('');								
			$("#K_controlfirst").removeAttr("class");				
			$("#K_controlfirst").text('');							
			$("#K_control").addClass('play');						
			$("#K_control").text('PLAY');							
			$("#K_controlfirst").addClass('play');					
			$("#K_controlfirst").text('PLAY');				
		}															
	}												
// CHANGE END: 20120130-1000 bsears - added code to check if bookmarked and adjust buttons
	
	$("#tertiaryLeft").show();
	$("#tertiaryRight").show();

	$("#secondary").hide();
	$("#tertiary").show();

	return;
}

function gotoplaymovie()	{
	
	var selection = $("#carouselPosters img.current").attr("alt");
	
	if (!selection||selection==''||selection==' '||selection=='EMPTY3')	{	
		return;
	}
	
	$("#tertiary").removeAttr("class");
	$("#tertiary").addClass('movies');
	
	$("#K_selection").removeAttr("class");
	$("#K_selection").text('');
	// selection statement was here //
	$("#K_selection").addClass(selection);

	$("#K_panel").removeClass('secondary');
	$("#K_panel").addClass('tertiary');

	$("#K_control").removeAttr("class");
	$("#K_control").text('');
	$("#K_controlfirst").removeAttr("class");
	$("#K_controlfirst").text('');
	
	var menulabel = $("#K_menu").text();
	var submenulabel = $("#K_submenu").text();
	
	$("#tertiaryTitle.menu").html('<p class="menu">'+menulabel+'</p>');	
	$("#tertiaryTitle.submenu").html('<p class="submenu">'+submenulabel+'</p>');
	
	$("#K_movie").removeAttr("class");
	$("#K_movie").text('');

	var movieHTML = getMovieHTML();
	
	$("#K_movie").addClass(movieHTML.selection);
		
	$("#K_control").addClass(movieHTML.controlfirst);
	$("#K_control").text(movieHTML.controllabel);
	$("#K_controlfirst").addClass(movieHTML.controlfirst);
	$("#K_controlfirst").text(movieHTML.controllabel);
	
	$("#controls").html(movieHTML.controls);
	$("#controlimage").html(movieHTML.movie);
	$("#control."+movieHTML.controlfirst+" span").addClass('active');	
	
	$("#tertiaryLeft").show();
	$("#tertiaryRight").show();

	$("#secondary").hide();
	$("#tertiary").show();

	return;
}

function nextmovie(direction)	{
	
	var current = $("#K_movie").attr("class");
	$("#K_movie").removeAttr("class");
	// Swapped the left and right direction so it matches human brain LOL.
	if (direction=='RIGHT')	{
		var next = $("#tertiaryRight #controlimage #movie p#next."+current).attr("title");
		$("#K_movie").addClass(next);
		if (next==current) return;
		$("#tertiaryRight #controlimage #movie."+current).hide();
		$("#tertiaryRight #controlimage #movie."+next).show("slide", { direction: "right" }, 500);
	}	else	
		if (direction=='LEFT')	{
			var prev = $("#tertiaryRight #controlimage #movie p#prev."+current).attr("title");
			$("#K_movie").addClass(prev);
			if (prev==current) return;
			$("#tertiaryRight #controlimage #movie."+current).hide();
			$("#tertiaryRight #controlimage #movie."+prev).show("slide", { direction: "left" }, 500);
		}
		
	return;
}



function gotocontrols(control)	{

	var prevcontrol = $("#K_control").attr("class");

	$("#K_control").removeAttr("class");
	$("#K_control").text('');
	$("#control."+prevcontrol+" span").removeClass('active');

	$("#K_control").addClass(control);
	var controllabel = $("#control."+control+' span').text();
	$("#K_control").text(controllabel);	
	
	$("#control."+control+" span").addClass('active');
	
	return;
}

function gotocontrol()	{

	var currcontrol = $("#K_control").attr("class");
	var submenu 	= $("#K_submenu").attr("class");
	
	if (currcontrol=='play')	{
		
		$("#K_panel").removeClass('tertiary');
		$("#K_panel").addClass('video');
		$("#tertiaryLeft").hide();
		$("#tertiaryRight").hide();		
		$("#tertiary").hide();
	
		var version = $("#K_version").text();
		if (version=='TEST'||version=='PROD')	{
//			if (submenu=='myprograms')
//				var video = $("#K_choice").attr("class");
////				else	var video = $("#K_selection").attr("class");
//				else	var video = $("#K_movie").attr("class");

			if (submenu=='myprograms')
				var video = $("#K_choice").attr("class");
				else
				if (submenu=='allprograms')				
					var video = $("#K_selection").attr("class");
					else	
						var video = $("#K_movie").attr("class");

			var url = playVideo(video);
			if (submenu=='allprograms')	{								
// CHANGE BEG: 20120130-1000 bsears - added code to check if bookmarked and adjust buttons (PENDING)
				$("#controls a span.bookmark").addClass('working');	
				$("#controls a.play").attr('rev','mainmenu');		
				$("#controls a.mainmenu").attr('rel','play');		
// CHANGE END: 20120130-1000 bsears - added code to check if bookmarked and adjust buttons (PENDING)
				var choice = $("#K_choice").attr("class");				
				var choiceHTML = getChoiceHTML(choice);					
				$("#secondaryRight").html(choiceHTML.choicelist);		
				var options = getChoice(choice);						
				buildselections(options); 								
			}															
		}	
		return;		
		
	}	else
	if (currcontrol=='bookmark')	{
		
		var version = $("#K_version").text();
		if (version=='TEST'||version=='PROD')	{
			if (submenu=='myprograms')
				var video = $("#K_choice").attr("class");
					else	var video = $("#K_selection").attr("class");
			var bookmarks = bookmarkVideo(video);	
		}
		var choice = $("#K_choice").attr("class");
		var choiceHTML = getChoiceHTML(choice);
		$("#secondaryRight").html(choiceHTML.choicelist);
		var options = getChoice(choice);
		buildselections(options); 
		$("#K_panel").removeClass('tertiary');
		$("#K_panel").addClass('secondary');
		$("#tertiary").hide();
		$("#secondary").show();
		
	}	else
	if (currcontrol=='remove')	{
	
		var version = $("#K_version").text();
		if (version=='TEST'||version=='PROD')	{
			var video = $("#K_choice").attr("class");
			var bookmark = cancelVideo(video);
		}		
		$("#K_panel").removeClass('tertiary');
		$("#tertiaryLeft").hide();
		$("#tertiaryRight").hide();		
		$("#tertiary").hide();
		gotosecondary();
		
	}	else	
	if (currcontrol=='back')	{
	
		$("#K_panel").removeClass('tertiary');
		$("#K_panel").addClass('secondary');
		//$("#tertiary").fadeOut('fast');
		//$("#secondary").fadeIn('fast');
		$("#tertiary").hide();
		$("#secondary").show();
		
	}	else
	if (currcontrol=='mainmenu')	{
	
		$("#K_panel").removeClass('tertiary');
		$("#K_panel").addClass('primary');
		$("#K_panel").text('');
		$("#K_subpanel").removeAttr('class');
		$("#K_subpanel").text('');
		$("#K_choice").removeAttr('class');
		$("#K_choice").text('');
		$("#K_selection").removeAttr('class');
		$("#K_selection").text('');
		$("#K_control").removeAttr('class');
		$("#K_control").text('');
		$("#K_choicefirst").removeAttr('class');
		$("#K_choicefirst").text('');
		$("#K_selectionfirst").removeAttr('class');
		$("#K_selectionfirst").text('');
		$("#K_controlfirst").removeAttr('class');
		$("#K_controlfirst").text('');
		$("#secondary div#secondaryLeft div#choices").text('');
		$("#secondary div#secondaryRight").text('');
		//$("#tertiary").fadeOut('fast');
		//$("#primary").fadeIn('fast');
		$("#tertiary").hide();
		$("#primary").show();		
		
	}
	
	return;
}

function buildselections(options)	{
	
	$("#secondary.allprograms div#secondaryLeft div#choices a span.active").css("color","#344e9b");

	$("#K_subpanel").addClass('selections');
	
	$("#K_selection").removeAttr("class");
	$("#K_selection").text('');
	$("#K_selectionfirst").removeAttr("class");
	$("#K_selectionfirst").text('');	

	var selectionsHTML = getSelectionsHTML(options.panel);
	
	$("#K_selection").addClass(selectionsHTML.selectionfirst);
	$("#K_selection").text(selectionsHTML.selectionlabel);
	$("#K_selectionfirst").addClass(selectionsHTML.selectionfirst);
	$("#K_selectionfirst").text(selectionsHTML.selectionlabel);	
	
	$("#secondaryRight #choicepanel4").html(selectionsHTML.selections);		
	$("#secondaryRight #choicepanel #choicepanel4 #selection."+selectionsHTML.selectionfirst+" span").addClass('active');
	$("#choicepanel3").html('Press SELECT to Bookmark or Watch');

	$("#secondaryRight #choicepanel4").show("slide", { direction: "up" }, 300);

	return;
}

// myprograms, allprograms selections 

function gotoselections(selection)	{
	
	var prevselection = $("#K_selection").attr("class");

	$("#K_selection").removeAttr("class");
	$("#K_selection").text('');
	$("#selection."+prevselection+" span").removeClass('active');
	
	$("#K_selection").addClass(selection);
	var selectionlabel = $("#selection."+selection+" span").text();
	
	$("#K_selection").text(selectionlabel);	
	
	$("#selection."+selection+" span").addClass('active');
	
	return;
}

function gotoselection(selection)	{

	if (selection=='more')	{
	
		moreselections();
		
	}	else
		if (selection=='back')	{	

			removeselections();
			
		}	else	{
			
				var options = getChoice(selection);
				
				if (options.tag=='EMPTY1'||options.tag=='EMPTY2'||options.tag=='EMPTY3')	{	
					return;
				}
				
				if (options.type=='video')	{
					gotoplayvideo(options);
				}	else
					if (options.type=='movie')	{
						gotoplaymovie();			
					}	else	{
							//msg('gotoselection: '+selection+': '+options.type+' not supported');
							return false;
						}	
		
			}
	
	return;
}

function moreselections()	{

	$("#K_selection").removeAttr("class");
	$("#K_selection").text('');
	$("#K_selectionfirst").removeAttr("class");
	$("#K_selectionfirst").text('');

	var selection = $("#K_choice").attr("class");
	var selectionlast = $("#selection.more").attr("rev");
	
	var selectionsHTML = getSelectionsHTML(selection,selectionlast);
	
	$("#K_selection").addClass(selectionsHTML.selectionfirst);
	$("#K_selection").text(selectionsHTML.selectionlabel);
	$("#K_selectionfirst").addClass(selectionsHTML.selectionfirst);
	$("#K_selectionfirst").text(selectionsHTML.selectionlabel);	
	
	$("#secondaryRight #choicepanel1").html(selectionsHTML.choicelist);
	$("#secondaryRight #choicepanel4").html(selectionsHTML.selections);
	$("#secondaryRight #choicepanel #choicepanel4 #selection."+selectionsHTML.selectionfirst+" span").addClass('active');
	//$("#choicepanel3").html('Press SELECT to Bookmark or Watch');
	
	//$("#secondaryRight #choicepanel4").show("slide", { direction: "up" }, 300);
	
	return;
}

function removeselections()	{
	
	$("#secondary.allprograms div#secondaryLeft div#choices a span.active").css("color","#ffffff");

	$("#K_subpanel").removeAttr("class");
	
	$("#K_selection").removeAttr("class");
	$("#K_selection").text('');
	$("#K_selectionfirst").removeAttr("class");
	$("#K_selectionfirst").text('');

	$("#choicepanel3").html('Press SELECT to Select a Video');

	$("#choicepanel4").hide("slide", { direction: "up" }, 300).html('');
	
}

// Dine Functionality

function gotodine()	{
	
	var indexHTML = initIndexHTML();
	$("#secondary").html(indexHTML.secondary);
	$("#dine").html(indexHTML.dine);	
	
	var menu = $("#K_menu").attr("class");
	var menulabel = $("#K_menu").text();
	var submenu = $("#K_submenu").attr("class");
	var submenulabel = $("#K_submenu").text();
	
	$("#K_subpanel").addClass('dine');
	$("#K_subpanel").text('step1');
	$("#K_dine").addClass(submenu);
	
	$("#secondary").addClass('dine');
	$("#primary").hide();
	$("#secondary").show();
	$("#dine").show();
	
	var dineHTML = getDineHTML();
	
	$("#dine #step1").html(dineHTML);
	$("#dine #step1 #navigation a span."+submenu).addClass('active');
	$("#dine #cover").show();	
	$("#dine #step1").fadeIn('fast');
	
	return;

}

function nextdine(key)	{
	
	var active = $("#K_dine").attr("class");
	var step   = $("#K_subpanel").text();
	var next   = 'mainmenu';
	
	$("#K_dine").removeClass(active);
	$("#dine #"+step+" #navigation a."+active+" span."+active).removeClass('active');
	if (key=='UP'||key=='LEFT')
		var next = $("#dine #"+step+" #navigation a."+active).attr("rev");
		else
		if (key=='DOWN'||key=='RIGHT')
			var next = $("#dine #"+step+" #navigation a."+active).attr("rel");
	$("#K_dine").addClass(next);
	
	$("#dine #"+step+" #navigation a."+next+" span."+next).addClass('active');
		
	return;
}

function dineERROR()	{

	$("#K_subpanel").text('stepX');
	var dineHTML = getDineHTML();
	$("#dine #step1").html(dineHTML);
	$("#dine #step1 #navigation.neither a span.close").addClass('active');
	
	return;	
}

function stepdine(key)	{
	
	var active  = $("#K_dine").attr("class");
	var step    = $("#K_subpanel").text();
	var submenu = $("#K_submenu").attr("class");
	
	if (step=='stepX')	{
		$("#K_panel").removeClass('secondary');
		$("#K_panel").addClass('primary');
		$("#K_subpanel").removeClass('dine');
		$("#K_subpanel").text('');
		$("#K_submenu").removeClass(submenu);
		$("#submenuitem."+submenu+" span").removeClass('active');
		$("#K_dine").removeClass(submenu);			
		$("#secondary").removeClass('dine');
		$("#secondary").hide();
		$("#dine").hide();
		$("#dine #cover").hide();	
		$("#dine #step1").hide();
		$("#primary").show();
		return;						
	}
	
	if (key!='ENTER')	{
		if (step=='step1')	{
			if (key=='RIGHT'&&(active=='back'||active=='mainmenu'))	{
				nextdine(key);
				return;
			}	else
			if (key=='LEFT'&&active=='mainmenu')	{
				nextdine(key);
				return;
			}
			if (key=='LEFT')	{
				$("#K_dine").removeClass(active);
				active = 'back';
				$("#K_dine").addClass(active);		
			}	
		}	else
		if (step=='step2'||step=='step3')	{
			if (key=='LEFT'&&active=='back')	{
				nextdine(key);
				return;
			}
			if (key=='LEFT'&&active=='next')	{
				$("#K_dine").removeClass(active);
				active = 'back';
				$("#K_dine").addClass(active);
			}		
		}
	}	
	
	$("#K_dine").removeClass(active);	
	$("#dine #"+step+" #navigation a."+active+" span."+active).removeClass('active');
	
	if (step=='step1')	{
		
		if (active=='mymenu')	{
			$("#submenuitem."+submenu+" span").removeClass('active');
			$("#K_submenu").removeClass(submenu);
			$("#K_submenu").addClass(active);
			$("#K_submenu").text('My Menu');
			submenu = $("#K_submenu").attr("class");
			$("#submenuitem."+submenu+" span").addClass('active');
			
			initORDER(active);
			var xmlError = $("#XMLCALL_error").text();
			if(xmlError)	{	
				dineERROR();
				return;
			}

			$("#K_subpanel").text('step2');
			var dineHTML = getDineHTML();
			$("#dine #step2").html(dineHTML);
			$("#dine #step2 #navigation a span.next").addClass('active');
			$("#K_dine").addClass('next');
			$("#dine #step1").fadeOut('fast');
			$("#dine #step2").fadeIn('fast');
			
			return;
		}	else
			if (active=='visitorsmenu')	{
				$("#submenuitem."+submenu+" span").removeClass('active');
				$("#K_submenu").removeClass(submenu);
				$("#K_submenu").addClass(active);
				$("#K_submenu").text('Visitors&#39; Menu');
				submenu = $("#K_submenu").attr("class");
				$("#submenuitem."+submenu+" span").addClass('active');
				
				initORDER(active);
				var xmlError = $("#XMLCALL_error").text();
				if(xmlError)	{	
					dineERROR();
					return;
				}
				
				$("#K_subpanel").text('step3');
				var dineHTML = getDineHTML();
				$("#dine #step3").html(dineHTML);
				$("#dine #step3 #navigation a span.next").addClass('active');
				$("#K_dine").addClass('next');
				$("#dine #step1").fadeOut('fast');
				$("#dine #step3").fadeIn('fast');
					
				return;
			}	else
				if (active=='neither')	{
					$("#K_subpanel").text('stepX');
					var dineHTML = getDineHTML();
					$("#dine #step1").html(dineHTML);
					$("#dine #step1 #navigation.neither a span.close").addClass('active');
					return;				
				}	else
					if (active=='back')	{
						$("#K_panel").removeClass('secondary');
						$("#K_panel").addClass('primary');
						$("#K_subpanel").removeClass('dine');
						$("#K_subpanel").text('');
						$("#K_dine").removeClass(submenu);			
						$("#secondary").removeClass('dine');
						$("#secondary").hide();
						$("#dine").hide();
						$("#dine #cover").hide();	
						$("#dine #step1").hide();
						$("#primary").show();
						return;
					}	else
						if (active=='mainmenu')	{
							$("#K_panel").removeClass('secondary');
							$("#K_panel").addClass('primary');
							$("#K_subpanel").removeClass('dine');
							$("#K_subpanel").text('');
							$("#K_submenu").removeClass(submenu);
							$("#submenuitem."+submenu+" span").removeClass('active');
							$("#K_dine").removeClass(submenu);			
							$("#secondary").removeClass('dine');
							$("#secondary").hide();
							$("#dine").hide();
							$("#dine #cover").hide();	
							$("#dine #step1").hide();
							$("#primary").show();
							return;						
						}
						
	}	else
	if (step=='step2')	{
	
		if (active=='next')	{
			$("#K_subpanel").text('step3');
			var dineHTML = getDineHTML();
			$("#dine #step3").html(dineHTML);
			$("#dine #step3 #navigation a span.next").addClass('active');
			$("#K_dine").addClass('next');
			$("#dine #step2").fadeOut('fast');
			$("#dine #step3").fadeIn('fast');
			return;				
		}	else
			if (active=='back')	{
				$("#K_subpanel").text('step1');
				var dineHTML = getDineHTML();
				$("#dine #step1").html(dineHTML);
				$("#dine #step1 #navigation a span."+submenu).addClass('active');
				$("#K_dine").addClass(submenu);
				$("#dine #step2").hide();
				$("#dine #step1").show();
				return;				
				}
						
	}	else
	if (step=='step3')	{
	
		if (active=='next')	{
			$("#K_subpanel").text('stepA');

			$("#dine").hide();
			$("#dine #cover").hide();	
			$("#dine #step3").hide();
// CHANGE BEG: 20120202-0130 bsears - high priority change: dine exit confirmation panel(s)
			$("#dine #step1").text('');
			$("#dine #step2").text('');
			$("#dine #step3").text('');
			$("#dine #step1").hide();
			$("#dine #step2").hide();
			$("#dine #step3").hide();
// CHANGE END: 20120202-0130 bsears - high priority change: dine exit confirmation panel(s)
			loaddine();
			return;
		}	else
			if (active=='back')	{
				if (submenu=='mymenu')	{			
					$("#K_subpanel").text('step2');
					$("#dine #step2 #navigation a span.next").addClass('active');
					$("#K_dine").addClass('next');
					$("#dine #step3").hide();
					$("#dine #step2").show();
				}	else
				if (submenu=='visitorsmenu')	{			
					$("#K_subpanel").text('step1');
					var dineHTML = getDineHTML();
					$("#dine #step1").html(dineHTML);					
					$("#dine #step1 #navigation a span."+submenu).addClass('active');
					$("#K_dine").addClass(submenu);
					$("#dine #step3").hide();
					$("#dine #step1").show();
				}				
				return;					
			}
						
	}

		
	return;
}

function loaddine()	{
	
	$("#secondary.dine #secondaryLeft").hide();
	$("#secondary.dine #secondaryRight").hide();
	
	$("#K_groups").removeAttr("class");
	$("#K_groups").text('');
	$("#K_group").removeAttr("class");
	$("#K_group").text('');
	$("#K_recipes").removeAttr("class");
	$("#K_recipes").text('');
	$("#K_recipe").removeAttr("class");
	$("#K_recipe").text('');
	
	var menu = $("#K_menu").attr("class");
	var menulabel = $("#K_menu").text();
	
	var submenu = $("#K_submenu").attr("class");
	var submenulabel = $("#K_submenu").text();
	
	$("#secondary.dine #secondaryTitle.menu").html('<p class="menu">'+menulabel+'</p>');	
	$("#secondary.dine #secondaryTitle.submenu").html('<p class="submenu">'+submenulabel+'</p>');
	
	var groupsHTML = getGroupsHTML();
		
	$("#K_groups").addClass(groupsHTML.active);
	$("#K_groups").text(groupsHTML.last);
	$("#K_group").addClass(groupsHTML.first);
	$("#K_group").text();
	
	$("#secondary.dine #secondaryLeft #choices").html(groupsHTML.groups);
	$("#secondary.dine #secondaryLeft #choices #choice."+groupsHTML.active+" span."+groupsHTML.active).addClass('active');
	
	var panelHTML = dinePanelHTML();
	$("#secondary.dine #secondaryRight").html(panelHTML.dine);
	
	var recipesHTML = getRecipesHTML(groupsHTML.active);
	
	$("#secondary.dine #secondaryRight #choicepanel #choicepanel5").html(recipesHTML.recipes);
	$("#secondary.dine #secondaryRight #choicepanel #choicepanel6").html(recipesHTML.panel);
	$("#secondary.dine #secondaryRight #choicepanel #choicepanel7").html(recipesHTML.nutrients);
	
	if(recipesHTML.overflow>0)	{
//		$("#secondary.dine #secondaryRight #choicepanel #choicepanel0").html(recipesHTML.instruction);
		$("#secondary.dine #secondaryRight #choicepanel #choicepanel5 p.overflow").show();
	}
	
	$("#secondary.dine #secondaryRight #choicepanel #choicepanel6 #selection."+recipesHTML.active+" span."+recipesHTML.active).addClass('active');
	
	$("#K_recipes").addClass(recipesHTML.active);
	$("#K_recipes").text(recipesHTML.last);
	$("#K_recipe").addClass(recipesHTML.first);
	$("#K_recipe").text(recipesHTML.start);
	
//	//$("#secondary").animate({ width: 'toggle', opacity: 'toggle' }, 1000);
//	//$("#secondaryLeft").delay(800).fadeIn(50);
//	//$("#secondaryRight").delay(800).fadeIn(50);
//	//$("#primary").fadeOut(1000);
	
	$("#secondary.dine #secondaryLeft").show();
	$("#secondary.dine #secondaryRight").show();
	$("#secondary.dine #secondaryRight #choicepanel #choicepanel5").show();
	$("#secondary.dine").fadeIn('fast');	
	
	return;
}

function nextgroup(key)	{

	var active = $("#K_groups").attr("class");
	
	$("#K_groups").removeClass(active);
	$("#secondary.dine #secondaryLeft #choices #choice."+active+" span."+active).removeClass('active');
	if (key=='UP'||key=='LEFT')
		var next = $("#secondary.dine #secondaryLeft #choices #choice."+active).attr("rev");
		else
		if (key=='DOWN'||key=='RIGHT')
			var next = $("#secondary.dine #secondaryLeft #choices #choice."+active).attr("rel");
	$("#K_groups").addClass(next);
	
	$("#secondary.dine #secondaryLeft #choices #choice."+next+" span."+next).addClass('active');
	
// CHANGE BEG: 20120206-1000 bsears - implement previous/next scrolling for publishing groups
//	if (next!='more'&&next!='vieworder'&&next!='back'&&next!='mainmenu')	{
	if (next!='prev'&&next!='more'&&next!='vieworder'&&next!='back'&&next!='mainmenu')	{
// CHANGE END: 20120206-1000 bsears - implement previous/next scrolling for publishing groups
		
		$("#K_recipes").removeAttr("class");
		$("#K_recipes").text('');
		$("#K_recipe").removeAttr("class");
		$("#K_recipe").text('');
		
		var recipesHTML = getRecipesHTML(next);
		$("#secondary.dine #secondaryRight #choicepanel #choicepanel5").html(recipesHTML.recipes);
		$("#secondary.dine #secondaryRight #choicepanel #choicepanel6").html(recipesHTML.panel);
		$("#secondary.dine #secondaryRight #choicepanel #choicepanel7").html(recipesHTML.nutrients);
		
		if(recipesHTML.overflow>0)
			$("#secondary.dine #secondaryRight #choicepanel #choicepanel5 p.overflow").show();	
		
		$("#secondary.dine #secondaryRight #choicepanel #choicepanel6 #selection."+recipesHTML.active+" span."+recipesHTML.active).addClass('active');
		
		$("#K_recipes").addClass(recipesHTML.active);
		$("#K_recipes").text(recipesHTML.last);
		$("#K_recipe").addClass(recipesHTML.first);
		$("#K_recipe").text(recipesHTML.start);
		
	}
		
	return;
}

function selectgroup(key)	{
	
	var active  = $("#K_groups").attr("class");
	var step    = $("#K_subpanel").text();
	var submenu = $("#K_submenu").attr("class");
	
	if (key!='ENTER')	{
		if (step=='stepA')	{
			
// CHANGE BEG: 20120223-0800 bsears - dine issue via Dennis: enable left/right arrows when on previous/next buttons
			if (key=='RIGHT'||key=='LEFT')	{
				if (active=='prev'||active=='more')	{	
					nextgroup(key);
					return;
				}
			}
// CHANGE BEG: 20120223-0800 bsears - dine issue via Dennis: enable left/right arrows when on previous/next buttons				
			
//			if (key=='RIGHT'&&(active=='back'||active=='mainmenu'))	{
//			if (key=='RIGHT'&&active=='mainmenu')	{
//				nextgroup(key);
//				return;
//			}	else
// CHANGE BEG: 20120206-1000 bsears - implement previous/next scrolling for publishing groups
//			if (key=='RIGHT'&&(active=='more'||active=='vieworder'||active=='mainmenu'))	{
			if (key=='RIGHT'&&(active=='prev'||active=='more'||active=='vieworder'||active=='mainmenu'))	{
// CHANGE END: 20120206-1000 bsears - implement previous/next scrolling for publishing groups
				return;
			}
			if(key=='LEFT')	{
				return;	
			}
//			if (key=='LEFT'&&active=='mainmenu')	{
//				nextgroup(key);
//				return;
//			}
//			if (key=='LEFT'&&active=='more')	{
//		
//			}	else
//			if (key=='LEFT')	{
//				$("#K_groups").removeClass(active);
//				active = 'back';
//				$("#K_groups").addClass(active);		
//			}	
		}
	}	
	
// CHANGE BEG: 20120206-1000 bsears - implement previous/next scrolling for publishing groups
//	if (active=='more')	{
	if (active=='more'||active=='prev')	{
// CHANGE END: 20120206-1000 bsears - implement previous/next scrolling for publishing groups
		
// CHANGE BEG: 20120206-1000 bsears - implement previous/next scrolling for publishing groups
//		var last = $("#K_groups").text();
// CHANGE END: 20120206-1000 bsears - implement previous/next scrolling for publishing groups
		
		$("#K_groups").removeAttr("class");
// CHANGE BEG: 20120206-1000 bsears - implement previous/next scrolling for publishing groups
//		$("#K_groups").text('');
// CHANGE BEG: 20120206-1000 bsears - implement previous/next scrolling for publishing groups
		$("#K_group").removeAttr("class");
		$("#K_group").text('');
		
// CHANGE BEG: 20120206-1000 bsears - implement previous/next scrolling for publishing groups
//		var groupsHTML = getGroupsHTML(last);
		var groupsHTML = getGroupsHTML(active);
// CHANGE END: 20120206-1000 bsears - implement previous/next scrolling for publishing groups		
			
		$("#K_groups").addClass(groupsHTML.active);
		$("#K_groups").text(groupsHTML.last);
		$("#K_group").addClass(groupsHTML.first);
		$("#K_group").text();
		
		$("#secondary.dine #secondaryLeft #choices").html(groupsHTML.groups);
		$("#secondary.dine #secondaryLeft #choices #choice."+groupsHTML.active+" span."+groupsHTML.active).addClass('active');
		
		$("#K_recipes").removeAttr("class");
		$("#K_recipes").text('');
		$("#K_recipe").removeAttr("class");
		$("#K_recipe").text('');
		
		var recipesHTML = getRecipesHTML(groupsHTML.active);
		$("#secondary.dine #secondaryRight #choicepanel #choicepanel5").html(recipesHTML.recipes);
		$("#secondary.dine #secondaryRight #choicepanel #choicepanel6").html(recipesHTML.panel);
		$("#secondary.dine #secondaryRight #choicepanel #choicepanel7").html(recipesHTML.nutrients);
		
		if(recipesHTML.overflow>0)
			$("#secondary.dine #secondaryRight #choicepanel #choicepanel5 p.overflow").show();
		
		$("#secondary.dine #secondaryRight #choicepanel #choicepanel6 #selection."+recipesHTML.active+" span."+recipesHTML.active).addClass('active');	
		
		$("#K_recipes").addClass(recipesHTML.active);
		$("#K_recipes").text(recipesHTML.last);
		$("#K_recipe").addClass(recipesHTML.first);
		$("#K_recipe").text(recipesHTML.start);

		return;
	}	else
		if (active=='vieworder')	{
			vieworder();
			return;						
		}	else
		if (active=='back')	{
			
			return;
			
//			$("#K_groups").removeClass(active);
//			$("#K_groups").text('');
//			$("#K_group").removeAttr('class');
//			$("#K_group").text('');
//			$("#secondary.dine #secondaryLeft #choices #choice."+active+" span."+active).removeClass('active');
//			$("#K_subpanel").text('step3');
//			$("#dine #step3 #navigation a span.next").addClass('active');
//			$("#K_dine").addClass('next');
//			$("#dine").show();
//			$("#dine #cover").show();	
//			$("#dine #step3").show();	
//			return;
		}	else
			if (active=='mainmenu')	{
// CHANGE BEG: 20120202-0130 bsears - high priority change: dine exit confirmation panel(s)
				var exitHTML = dineExitHTML();
				$("#dine #exit").html(exitHTML.exit);
// CHANGE END: 20120202-0130 bsears - high priority change: dine exit confirmation panel(s)				
				
				// display yes/no panel
				$("#secondary.dine #secondaryLeft #choices #choice."+active+" span."+active).removeClass('active');
				$("#K_subpanel").text('stepW');
// CHANGE BEG: 20120202-0130 bsears - high priority change: dine exit confirmation panel(s)
//				$("#secondary.dine #secondaryRight #choicepanel #choicepanel8 a span.yes").addClass('active');
//				$("#secondary.dine #secondaryRight #choicepanel #choicepanel8").fadeIn('fast');
				$("#dine").show();
				$("#dine #cover").show();
				$("#dine #exit a span.no").addClass('active');
				$("#dine #exit").fadeIn('fast');
// CHANGE END: 20120202-0130 bsears - high priority change: dine exit confirmation panel(s)
				
				return;
				
//				$("#K_panel").removeClass('secondary');
//				$("#K_panel").addClass('primary');
//				$("#K_subpanel").removeClass('dine');
//				$("#K_subpanel").text('');
//				$("#K_submenu").removeClass(submenu);
//				$("#submenuitem."+submenu+" span").removeClass('active');
//				$("#K_groups").removeClass(active);
//				$("#K_groups").text('');
//				$("#K_group").removeAttr('class');
//				$("#K_group").text('');
//				$("#secondary").removeClass('dine');
//				$("#secondary").hide();
//				$("#primary").show();
//				return;						
			}

	$("#K_subpanel").text('stepC');
	
	$("#K_recipes").removeAttr("class");
	$("#K_recipes").text('');
	$("#K_recipe").removeAttr("class");
	$("#K_recipe").text('');
	
	var recipesHTML = getRecipesHTML(active);
	$("#secondary.dine #secondaryRight #choicepanel #choicepanel5").html(recipesHTML.recipes);
	$("#secondary.dine #secondaryRight #choicepanel #choicepanel6").html(recipesHTML.panel);
	$("#secondary.dine #secondaryRight #choicepanel #choicepanel7").html(recipesHTML.nutrients);
	
//	if(recipesHTML.overflow>0)
//		$("#secondary.dine #secondaryRight #choicepanel #choicepanel5 p.overflow").show();
		
	if(recipesHTML.overflow>0)	{
		$("#secondary.dine #secondaryRight #choicepanel #choicepanel0").html(recipesHTML.instruction);
		$("#secondary.dine #secondaryRight #choicepanel #choicepanel5 p.overflow").show();
	}
	
	$("#secondary.dine #secondaryRight #choicepanel #choicepanel6 #selection."+recipesHTML.active+" span."+recipesHTML.active).addClass('active');	
	
	$("#K_recipes").addClass(recipesHTML.active);
	$("#K_recipes").text(recipesHTML.last);
	$("#K_recipe").addClass(recipesHTML.first);
	$("#K_recipe").text(recipesHTML.start);

	$("#secondary.dine #secondaryRight #choicepanel #choicepanel5 p.overflow").hide();
	
	//$("#secondary.dine #secondaryRight").css("background-image", "url(images/imgs_orderfood2.png)");
	$("#secondary.dine #secondaryRight").css( { "background-image": "url(./images/imgs_orderfood2.png)","background-position": "bottom right","background-repeat": "no-repeat" } )	

// CHANGE BEG: 20120229-1700 bsears - dine issue via Dennis: remove animation to reduce latency flashing
//	$("#secondary.dine #secondaryRight #choicepanel #choicepanel6").show("slide", { direction: "up" }, 300);
//	$("#secondary.dine #secondaryRight #choicepanel #choicepanel7").show("slide", { direction: "up" }, 300);
	$("#secondary.dine #secondaryRight #choicepanel #choicepanel6").show();
	$("#secondary.dine #secondaryRight #choicepanel #choicepanel7").show();
// CHANGE END: 20120229-1700 bsears - dine issue via Dennis: remove animation to reduce latency flashing
		
	return;
}

//function nextmenus(key)	{
//
//	var active = $("#K_menus").attr("class");
//	
//	$("#K_menus").removeClass(active);
//	$("#secondary.dine #secondaryRight #choicepanel #choicepanel4 #selection."+active+" span."+active).removeClass('active');
//	if (key=='UP'||key=='LEFT')
//		var next = $("#secondary.dine #secondaryRight #choicepanel #choicepanel4 #selection."+active).attr("rev");
//		else
//		if (key=='DOWN'||key=='RIGHT')
//			var next = $("#secondary.dine #secondaryRight #choicepanel #choicepanel4 #selection."+active).attr("rel");
//	$("#K_menus").addClass(next);
//	
//	$("#secondary.dine #secondaryRight #choicepanel #choicepanel4 #selection."+next+" span."+next).addClass('active');
//		
//	return;
//}

//function selectmenus(key)	{
//	
//	var active  = $("#K_menus").attr("class");
//	var step    = $("#K_subpanel").text();
//	var submenu = $("#K_submenu").attr("class");
//	
//	
//	if (key!='ENTER')	{
//		if (step=='stepB')	{
//			if (key=='RIGHT'&&(active=='more'||active=='back'))	{
//				nextmenus(key);
//				return;
//			}	else
//			if (key=='LEFT'&&active=='back')	{
//				nextmenus(key);
//				return;
//			}
//			if (key=='LEFT')	{
//				$("#K_menus").removeClass(active);
//				active = 'back';
//				$("#K_menus").addClass(active);		
//			}	
//		}
//	}	
//	
//	if (active=='more')	{
//
//		return;
//	}	else
//		if (active=='back')	{
//			$("#K_menus").removeClass(active);
//			$("#secondary.dine #secondaryRight #choicepanel #choicepanel4 #selection."+active+" span."+active).removeClass('active');
//			$("#K_subpanel").text('stepA');
//			active = $("#K_meals").attr("class");
//			$("#secondary.dine #secondaryLeft #choices #choice."+active+" span."+active).addClass('active');
//			$("#secondary.dine #secondaryRight #choicepanel4").hide("slide", { direction: "down" }, 300);	
//			return;
//		}
//
//	$("#K_subpanel").text('stepC');
//	
//	$("#K_recipes").removeAttr("class");
//	$("#K_recipes").text('');
//	$("#K_recipesfirst").removeAttr("class");
//	$("#K_recipesfirst").text('');	
//
//	var recipesHTML = selectRecipesHTML(active);
//	
//	$("#K_recipes").addClass(recipesHTML.recipesfirst);
//	$("#K_recipes").text(recipesHTML.recipeslabel);
//	$("#K_recipesfirst").addClass(recipesHTML.recipesfirst);
//	$("#K_recipesfirst").text(recipesHTML.recipeslabel);
//	
//	$("#secondary.dine #secondaryRight #choicepanel #choicepanel4 #selection."+active+" span."+active).removeClass('active');
//	
//	$("#secondary.dine #secondaryRight #choicepanel #choicepanel5").html(recipesHTML.recipes);
//	$("#secondary.dine #secondaryRight #choicepanel #choicepanel6").html(recipesHTML.panel);
//	$("#secondary.dine #secondaryRight #choicepanel #choicepanel7").html(recipesHTML.nutrients);
//	
//	$("#secondary.dine #secondaryRight #choicepanel #choicepanel6 #selection."+recipesHTML.recipesfirst+" span").addClass('active');
//
//	$("#secondary.dine #secondaryRight #choicepanel #choicepanel1").hide();
//	$("#secondary.dine #secondaryRight #choicepanel #choicepanel4").hide();	
//	$("#secondary.dine #secondaryRight #choicepanel #choicepanel5").show();
//	$("#secondary.dine #secondaryRight #choicepanel #choicepanel6").show();	
//	$("#secondary.dine #secondaryRight #choicepanel #choicepanel7").show("slide", { direction: "up" }, 300);
//	
//	return;
//}

function nextrecipes(key)	{

	var active = $("#K_recipes").attr("class");
	
	$("#K_recipes").removeClass(active);
	$("#secondary.dine #secondaryRight #choicepanel #choicepanel6 #selection."+active+" span."+active).removeClass('active');
	if (key=='UP'||key=='LEFT')
		var next = $("#secondary.dine #secondaryRight #choicepanel #choicepanel6 #selection."+active).attr("rev");
		else
		if (key=='DOWN'||key=='RIGHT')
			var next = $("#secondary.dine #secondaryRight #choicepanel #choicepanel6 #selection."+active).attr("rel");
	$("#K_recipes").addClass(next);
	
	$("#secondary.dine #secondaryRight #choicepanel #choicepanel6 #selection."+next+" span."+next).addClass('active');
	
	if (next!='more'&&next!='back')	{
		var nutrients = getNutrientsHTML(next);
		$("#secondary.dine #secondaryRight #choicepanel #choicepanel7").html(nutrients);
// CHANGE BEG: 20120223-0700 bsears - dine issue via Tami: clear nutrition panel when not on recipe		
//	}
	}	else	{
			$("#secondary.dine #secondaryRight #choicepanel #choicepanel7").html('');
		}
// CHANGE END: 20120223-0700 bsears - dine issue via Tami: clear nutrition panel when not on recipe	
		
	return;
}

function selectrecipes(key)	{
	
	var active  = $("#K_recipes").attr("class");
	var start   = $("#K_recipe").text();
	var step    = $("#K_subpanel").text();
	var submenu = $("#K_submenu").attr("class");
	
	
	if (key!='ENTER')	{
		if (step=='stepC')	{
			if (key=='RIGHT'&&(active=='more'||active=='back'))	{
				nextrecipes(key);
				return;
			}	else
			if (key=='LEFT'&&active=='back')	{
				nextrecipes(key);
				return;
			}
			if (key=='LEFT')	{
				if (start==1)	{
					$("#K_recipes").removeClass(active);
					active = 'back';
					$("#K_recipes").addClass(active);
				}
			}	
		}
	}	
	
	if (key=='LEFT'&&(active!='more'&&active!='back'))	{
		
		$("#K_recipes").removeClass(active);
		$("#secondary.dine #secondaryRight #choicepanel #choicepanel6 #selection."+active+" span."+active).removeClass('active');
		
		var active = $("#K_groups").attr("class");
		
		var recipesHTML = getRecipesHTML(active,'bkwd');
		$("#secondary.dine #secondaryRight #choicepanel #choicepanel5").html(recipesHTML.recipes);
		$("#secondary.dine #secondaryRight #choicepanel #choicepanel6").html(recipesHTML.panel);
		$("#secondary.dine #secondaryRight #choicepanel #choicepanel7").html(recipesHTML.nutrients);
		
		//if(recipesHTML.overflow>0)
		//	$("#secondary.dine #secondaryRight #choicepanel #choicepanel5 p.overflow").show();		
		
		$("#secondary.dine #secondaryRight #choicepanel #choicepanel6 #selection."+recipesHTML.active+" span."+recipesHTML.active).addClass('active');	
		
		$("#K_recipes").addClass(recipesHTML.active);
		$("#K_recipes").text(recipesHTML.last);	
		$("#K_recipe").addClass(recipesHTML.first);
		$("#K_recipe").text(recipesHTML.start);	

		return;
	}	else
	if (active=='more'||key=='RIGHT')	{
		
		$("#K_recipes").removeClass(active);
		$("#secondary.dine #secondaryRight #choicepanel #choicepanel6 #selection."+active+" span."+active).removeClass('active');
		
		var active = $("#K_groups").attr("class");
		
		var recipesHTML = getRecipesHTML(active);
		$("#secondary.dine #secondaryRight #choicepanel #choicepanel5").html(recipesHTML.recipes);
		$("#secondary.dine #secondaryRight #choicepanel #choicepanel6").html(recipesHTML.panel);
		$("#secondary.dine #secondaryRight #choicepanel #choicepanel7").html(recipesHTML.nutrients);
		
		//if(recipesHTML.overflow>0)
		//	$("#secondary.dine #secondaryRight #choicepanel #choicepanel5 p.overflow").show();		
		
		$("#secondary.dine #secondaryRight #choicepanel #choicepanel6 #selection."+recipesHTML.active+" span."+recipesHTML.active).addClass('active');	
		
		$("#K_recipes").addClass(recipesHTML.active);
		$("#K_recipes").text(recipesHTML.last);	
		$("#K_recipe").addClass(recipesHTML.first);
		$("#K_recipe").text(recipesHTML.start);	

		return;
	}	else
		if (active=='back')	{		
			
			$("#K_recipes").removeClass(active);
			$("#secondary.dine #secondaryRight #choicepanel #choicepanel6 #selection."+active+" span."+active).removeClass('active');
			
			checkvieworder();
			
			$("#K_subpanel").text('stepA');
			active = $("#K_groups").attr("class");
			$("#secondary.dine #secondaryLeft #choices #choice."+active+" span."+active).addClass('active');
			
			//$("#secondary.dine #secondaryRight").css("background-image", "url(images/imgs_orderfood.png)");
			$("#secondary.dine #secondaryRight").css( { "background-image": "url(./images/imgs_orderfood.png)","background-position": "bottom right","background-repeat": "no-repeat" } )
			
			$("#secondary.dine #secondaryRight #choicepanel #choicepanel0").text('');

// CHANGE BEG: 20120229-1700 bsears - dine issue via Dennis: remove animation to reduce latency flashing
//			$("#secondary.dine #secondaryRight #choicepanel7").hide("slide", { direction: "up" }, 300);
//			$("#secondary.dine #secondaryRight #choicepanel6").hide("slide", { direction: "up" }, 300);
			$("#secondary.dine #secondaryRight #choicepanel7").hide();
			$("#secondary.dine #secondaryRight #choicepanel6").hide();
// CHANGE END: 20120229-1700 bsears - dine issue via Dennis: remove animation to reduce latency flashing
			return;
		}
		
//	addORDER('recipe3','1');
//	updateORDER('recipe3','9');
//	var ordered = checkORDER('recipe3');
//	msg('ordered: '+ordered);
//	var ordered = checkORDER('recipe4');
//	msg('ordered: '+ordered);
//	removeORDER('recipe3');
//	var recipeDATA = recipeORDER('recipe3');
//	msg(recipeDATA.nutrients);

	var groupid = $("#K_groups").attr("class");
	var ordered = checkORDER(active);
	//msg('ordered: '+ordered);
	if(ordered)	{
		removeORDER(active);
		updateMAX(groupid,'remove');
		$("#secondary.dine #secondaryRight #choicepanel #choicepanel5 p."+active+" span").removeClass("ordered");
	}	else	{		
			var remaining = groupMAX(groupid);
			if(remaining>0)	{
				updateMAX(groupid,'add');
				addORDER(active);
				$("#secondary.dine #secondaryRight #choicepanel #choicepanel5 p."+active+" span").addClass("ordered");
			}	else	{
					//return; // remove to activate select functionality
					$("#secondary.dine #secondaryRight #choicepanel #choicepanel6 #selection."+active+" span."+active).removeClass('active');
					$("#K_subpanel").text('stepE');
					$("#secondary.dine #secondaryRight #choicepanel #choicepanel9 a span").addClass('active');
					$("#secondary.dine #secondaryRight #choicepanel #choicepanel9").fadeIn('fast');
					return;						
				}
		}
	
	//var description = $("#secondary.dine #secondaryRight #choicepanel #choicepanel5 p.recipe3").text();
	//msg(description);
		
	//$("#secondary.dine #secondaryRight #choicepanel #choicepanel5 p.recipe3 span").addClass("ordered");	
	
		
//	process recipe selection/deselection

//	$("#K_subpanel").text('stepQ');
//	
//	var quantityHTML = getQuantityHTML(active);
//	
//	$("#K_quantity").text(quantityHTML.quantity);
//	$("#K_quantitymax").text(quantityHTML.quantitymax);
//	$("#K_quantitynav").addClass(quantityHTML.active);
//
//	$("#secondary.dine #secondaryRight #choicepanel #choicepanel9").html(quantityHTML.panel);
//	$("#secondary.dine #secondaryRight #choicepanel #choicepanel9 #selection."+quantityHTML.active+" span").addClass('active');
//
//	$("#secondary.dine #secondaryRight #choicepanel #choicepanel9").show();	
	
	return;
}

function closeerror()	{
	
	var active = $("#K_recipes").attr("class");
	$("#secondary.dine #secondaryRight #choicepanel #choicepanel6 #selection."+active+" span."+active).addClass('active');
	$("#K_subpanel").text('stepC');
	$("#secondary.dine #secondaryRight #choicepanel #choicepanel9 a span").removeClass('active');
	$("#secondary.dine #secondaryRight #choicepanel #choicepanel9").fadeOut('fast');	
	
	return;
}

// CHANGE BEG: 20120217-0800 bsears - dine issue via Tami: implement menu for exit dine
function menudineexit()	{
	
	var step = $("#K_subpanel").text();
	if (step=='stepA')		
		$("#K_subpanel").text('stepW');
		else	
		if (step=='order')		
			$("#K_subpanel").text('exit');
			else	{
				$("#K_dinestep").text(step);
				$("#K_subpanel").text('menuexit');
			}
			
	$("#step1").hide();
	$("#step2").hide();
	$("#step3").hide();
	
	var exitHTML = dineExitHTML();
	$("#dine #exit").html(exitHTML.exit);

	$("#dine").show();
	$("#dine #cover").show();
	$("#dine #exit a span.no").addClass('active');
	$("#dine #exit").fadeIn('fast');	
	
}
// CHANGE END: 20120217-0800 bsears - dine issue via Tami: implement menu for exit dine

function processexit(key)	{
	
// CHANGE BEG: 20120202-0130 bsears - high priority change: dine exit confirmation panel(s)

//	var yesno 	= $("#secondary.dine #secondaryRight #choicepanel #choicepanel8 a span.active").attr("class");
//	var yesnoA  = yesno.split(" ");
//	yesno = yesnoA[0];
//	
//	if (key=='LEFT'||key=='RIGHT')	{
//		$("#secondary.dine #secondaryRight #choicepanel #choicepanel8 a span."+yesno).removeClass('active');
//		if (yesno=='yes')
//			$("#secondary.dine #secondaryRight #choicepanel #choicepanel8 a span.no").addClass('active');	
//			else
//			if (yesno=='no')
//				$("#secondary.dine #secondaryRight #choicepanel #choicepanel8 a span.yes").addClass('active');
//	}	else
//		if (key=='ENTER')	{
//			var active  = $("#K_groups").attr("class");
//			$("#secondary.dine #secondaryLeft #choices #choice."+active+" span."+active).addClass('active');
//			$("#K_subpanel").text('stepA');
//			$("#secondary.dine #secondaryRight #choicepanel #choicepanel8 a span.yes").removeClass('active');
//			$("#secondary.dine #secondaryRight #choicepanel #choicepanel8 a span.no").removeClass('active');
//			$("#secondary.dine #secondaryRight #choicepanel #choicepanel8").fadeOut('fast');
//			if (yesno=='yes')	{
//				var submenu = $("#K_submenu").attr("class");
//				$("#K_panel").removeClass('secondary');
//				$("#K_panel").addClass('primary');
//				$("#K_subpanel").removeClass('dine');
//				$("#K_subpanel").text('');
//				$("#K_submenu").removeClass(submenu);
//				$("#submenuitem."+submenu+" span").removeClass('active');
//				$("#K_groups").removeClass(active);
//				$("#K_groups").text('');
//				$("#K_group").removeAttr('class');
//				$("#K_group").text('');
//				$("#secondary").removeClass('dine');
//				$("#secondary").hide();
//				$("#primary").show();
//			}
//		}

	var yesno 	= $("#dine #exit a span.active").attr("class");
	var yesnoA  = yesno.split(" ");
	yesno = yesnoA[0];
	
	if (key=='LEFT'||key=='RIGHT')	{
		$("#dine #exit a span."+yesno).removeClass('active');
		if (yesno=='yes')
			$("#dine #exit a span.no").addClass('active');	
			else
			if (yesno=='no')
				$("#dine #exit a span.yes").addClass('active');
	}	else
		if (key=='ENTER')	{	
// CHANGE BEG: 20120216-0900 bsears - dine issue via Tami: implement unlock menu
			if (yesno=='yes')	{
				unlockMENU();					
				var xmlError = $("#XMLCALL_error").text();
				if(xmlError)	{
					$("#K_subpanel").text('error');
					var errorHTML = submitErrorHTML();
					$("#dine #exit").html(errorHTML.error);
					$("#dine #cover").show();
					$("#dine #exit a span.close").addClass('active');
					$("#dine #exit").fadeIn('fast');
					return;
				}
// CHANGE BEG: 20120217-0800 bsears - dine issue via Tami: implement menu for exit dine
				var indexHTML = initIndexHTML();
				$("#kookies").html(indexHTML.kookies);
				$("#secondary").html(indexHTML.secondary);
				$("#dine").html(indexHTML.dine);
				
				var tag = $("#secondary").attr("class");
				$("#secondary").removeClass(tag);
				tag = $("#dine").attr("class");
				$("#dine").removeClass(tag);
				
				$("#K_panel").addClass('primary');
		
				$("#secondary").hide();
				$("#dine").hide();
				$("#primary").show();
				
				return;
// CHANGE END: 20120217-0800 bsears - dine issue via Tami: implement menu for exit dine
			}
// CHANGE END: 20120216-0900 bsears - dine issue via Tami: implement unlock menu			
			var step = $("#K_subpanel").text();
// CHANGE BEG: 20120217-0800 bsears - dine issue via Tami: implement menu for exit dine					
			if (step=='menuexit')	{
				var dinestep = $("#K_dinestep").text();
				$("#K_subpanel").text(dinestep);
				$("#K_dinestep").text('');
				if (dinestep=='step1'||dinestep=='step2'||dinestep=='step3')	{
					$("#"+dinestep).show();
					$("#dine #exit").hide();
				}	else	{
						$("#dine #exit").hide();
						$("#dine #cover").hide();
					}
				return;
			}
// CHANGE END: 20120217-0800 bsears - dine issue via Tami: implement menu for exit dine			
			if (step=='stepW')	{
				var active  = $("#K_groups").attr("class");
				$("#secondary.dine #secondaryLeft #choices #choice."+active+" span."+active).addClass('active');
				$("#K_subpanel").text('stepA');
			}	else
			if (step=='exit')	{
				var activeO = $("#K_order").attr("class");
				$("#dine #order #navigation a."+activeO+" span."+activeO).addClass('active');	
				$("#K_subpanel").text('order');
			}
			$("#dine #exit a span.yes").removeClass('active');
			$("#dine #exit a span.no").removeClass('active');
			$("#dine #exit").hide();
			$("#dine #cover").hide();
// CHANGE BEG: 20120217-0800 bsears - dine issue via Tami: implement menu for exit dine			
//			if (yesno=='yes')	{
//				if (step=='exit')	{					
//					$("#dine #order").html('');
//					$("#K_order").removeClass(activeO);
//					$("#K_order").text('');
//					$("#dine").hide();
//					$("#dine #order").hide();				
//				}
//				var submenu = $("#K_submenu").attr("class");
//				$("#K_panel").removeClass('secondary');
//				$("#K_panel").addClass('primary');
//				$("#K_subpanel").removeClass('dine');
//				$("#K_subpanel").text('');
//				$("#K_submenu").removeClass(submenu);
//				$("#submenuitem."+submenu+" span").removeClass('active');
//				$("#K_groups").removeClass(active);
//				$("#K_groups").text('');
//				$("#K_group").removeAttr('class');
//				$("#K_group").text('');
//				$("#secondary").removeClass('dine');
//				$("#secondary").hide();
//				$("#primary").show();
//			}
// CHANGE END: 20120217-0800 bsears - dine issue via Tami: implement menu for exit dine
		}
// CHANGE END: 20120202-0130 bsears - high priority change: dine exit confirmation panel(s)

	return;
}

function checkvieworder()	{
	
	//return; // remove to activate select functionality
	
	var total 	 = countORDER();
// CHANGE BEG: 20120206-1000 bsears - implement previous/next scrolling for publishing groups
	var overflow = groupsOverflow();
	var last 	 = $("#K_groups").text();
// CHANGE END: 20120206-1000 bsears - implement previous/next scrolling for publishing groups
	//msg(' total:'+total+' overflow:'+overflow);
	if(total==0)	{
		var checkdim = $("#secondary.dine #secondaryLeft #choices a.dim span").attr('class');
//		msg(checkdim);
		if (checkdim) return;
//		msg('dimming vieworder');
		$("#secondary.dine #secondaryLeft #choices a.vieworder").addClass('dim');
		$("#secondary.dine #secondaryLeft #choices a.vieworder span.vieworder").addClass('dim');		
		$("#secondary.dine #secondaryLeft #choices a.dim").removeClass('vieworder');
		$("#secondary.dine #secondaryLeft #choices a.dim span.dim").removeClass('vieworder');
		$("#secondary.dine #secondaryLeft #choices a.dim").attr('rev','');
		$("#secondary.dine #secondaryLeft #choices a.dim").attr('rel','');
// CHANGE BEG: 20120206-1000 bsears - implement previous/next scrolling for publishing groups
//		$("#secondary.dine #secondaryLeft #choices a.more").attr('rel','mainmenu');
//		$("#secondary.dine #secondaryLeft #choices a.mainmenu").attr('rev','more');
		if (overflow>0)	{
// CHANGE BEG: 20120223-0730 bsears - dine issue via Dennis: swap Previous/Next button positions
//			$("#secondary.dine #secondaryLeft #choices a.more").attr('rel','mainmenu');
//			$("#secondary.dine #secondaryLeft #choices a.mainmenu").attr('rev','more');
			$("#secondary.dine #secondaryLeft #choices a.prev").attr('rel','mainmenu');
			$("#secondary.dine #secondaryLeft #choices a.mainmenu").attr('rev','prev');
// CHANGE END: 20120223-0730 bsears - dine issue via Dennis: swap Previous/Next button positions
		}	else	{
				$("#secondary.dine #secondaryLeft #choices a."+last).attr('rel','mainmenu');
				$("#secondary.dine #secondaryLeft #choices a.mainmenu").attr('rev',last);
			}
// CHANGE END: 20120206-1000 bsears - implement previous/next scrolling for publishing groups
	}	else	{
			var checkdim = $("#secondary.dine #secondaryLeft #choices a.vieworder span").attr('class');
//			msg(checkdim);
			if (checkdim) return;
//			msg('activating vieworder');		
			$("#secondary.dine #secondaryLeft #choices a.dim").addClass('vieworder');
			$("#secondary.dine #secondaryLeft #choices a.dim span.dim").addClass('vieworder');		
			$("#secondary.dine #secondaryLeft #choices a.vieworder").removeClass('dim');
			$("#secondary.dine #secondaryLeft #choices a.vieworder span.vieworder").removeClass('dim');
// CHANGE BEG: 20120206-1000 bsears - implement previous/next scrolling for publishing groups
//			$("#secondary.dine #secondaryLeft #choices a.vieworder").attr('rev','more');
//			$("#secondary.dine #secondaryLeft #choices a.vieworder").attr('rel','mainmenu');
//			$("#secondary.dine #secondaryLeft #choices a.more").attr('rel','vieworder');
//			$("#secondary.dine #secondaryLeft #choices a.mainmenu").attr('rev','vieworder');
			if (overflow>0)	{
// CHANGE BEG: 20120223-0730 bsears - dine issue via Dennis: swap Previous/Next button positions
//				$("#secondary.dine #secondaryLeft #choices a.vieworder").attr('rev','more');
				$("#secondary.dine #secondaryLeft #choices a.vieworder").attr('rev','prev');
// CHANGE END: 20120223-0730 bsears - dine issue via Dennis: swap Previous/Next button positions
				$("#secondary.dine #secondaryLeft #choices a.vieworder").attr('rel','mainmenu');
// CHANGE BEG: 20120223-0730 bsears - dine issue via Dennis: swap Previous/Next button positions
//				$("#secondary.dine #secondaryLeft #choices a.more").attr('rel','vieworder');
				$("#secondary.dine #secondaryLeft #choices a.prev").attr('rel','vieworder');
// CHANGE END: 20120223-0730 bsears - dine issue via Dennis: swap Previous/Next button positions
				$("#secondary.dine #secondaryLeft #choices a.mainmenu").attr('rev','vieworder');				
			}	else	{
					$("#secondary.dine #secondaryLeft #choices a.vieworder").attr('rev',last);
					$("#secondary.dine #secondaryLeft #choices a.vieworder").attr('rel','mainmenu');
					$("#secondary.dine #secondaryLeft #choices a."+last).attr('rel','vieworder');
					$("#secondary.dine #secondaryLeft #choices a.mainmenu").attr('rev','vieworder');		
				}
// CHANGE BEG: 20120206-1000 bsears - implement previous/next scrolling for publishing groups
		}	
	
	return total;
}

//function navquantity(key)	{
//
//	var active = $("#K_quantitynav").attr("class");
//	
//	$("#K_quantitynav").removeClass(active);
//	$("#secondary.dine #secondaryRight #choicepanel #choicepanel9 #selection."+active+" span."+active).removeClass('active');
//	if (key=='LEFT')
//		var next = $("#secondary.dine #secondaryRight #choicepanel #choicepanel9 #selection."+active).attr("rev");
//		else
//		if (key=='RIGHT')
//			var next = $("#secondary.dine #secondaryRight #choicepanel #choicepanel9 #selection."+active).attr("rel");
//	$("#K_quantitynav").addClass(next);
//	
//	$("#secondary.dine #secondaryRight #choicepanel #choicepanel9 #selection."+next+" span."+next).addClass('active');
//		
//	return;
//}
//
//function nextquantity(key)	{
//
//	var quantity 	= $("#K_quantity").text();
//	var quantitymax = $("#K_quantitymax").text();
//	
//	if (key=='UP')	{
//		quantity--;
//		if (quantity<0)	
//			quantity=quantitymax;
//	}
//		else
//		if (key=='DOWN')	{
//		quantity++;
//		if (quantity>quantitymax)	
//			quantity=0;
//		}
//		
//	$("#K_quantity").text(quantity);
//	$("#K_quantitymax").text(quantitymax);
//	
//	$("#secondary.dine div#secondaryRight div#choicepanel div#choicepanel9 div#left p").text(quantity);
//		
//	return;
//}
//
//function selectquantity(key)	{
//	
//	var active  = $("#K_quantitynav").attr("class");
//	
//	if (active=='confirm')	{
//
//	}	else
//		if (active=='cancel')	{
//			
//		}
//		
//	$("#K_quantitynav").removeClass(active);
//	$("#secondary.dine #secondaryRight #choicepanel #choicepanel9 #selection."+active+" span."+active).removeClass('active');
//	//$("#K_subpanel").text('stepC');
//	$("#K_subpanel").text('order');
//
//	$("#secondary.dine #secondaryRight #choicepanel #choicepanel9").hide();
//	
//	return;
//}

function vieworder()	{
	
	//return; // remove to activate select functionality
	
	var total = countORDER();
	if(total==0)	{
		msg('no recipes selected');
		return;
	}
	
	$("#K_subpanel").text('order');
	
	var orderHTML = vieworderHTML();
	
	$("#dine #order").html(orderHTML.panel);
	
	orderHTML = nextorderHTML();	
	
	$("#dine #order #recipes").html(orderHTML.recipes);
	$("#dine #order #totals").html(orderHTML.totals);
	$("#dine #order #navigation").html(orderHTML.navigation);
	$("#dine #order #navigation.bar").html(orderHTML.navbar);
	
	$("#K_order").addClass(orderHTML.active);
	$("#K_order").text(orderHTML.last);
	$("#K_orderfirst").text(orderHTML.first);
	
	$("#dine #order #navigation a."+orderHTML.active+" span."+orderHTML.active).addClass('active');	
	
	$("#secondary.dine #secondaryLeft").hide();
	$("#secondary.dine #secondaryRight").hide();
	$("#secondary.dine").fadeOut('fast');
	$("#dine").fadeIn('fast');
	$("#dine #order").fadeIn('fast');
	
	return;
}

function nextorder(key)	{

	var active = $("#K_order").attr("class");
	
	$("#K_order").removeClass(active);
	$("#dine #order #navigation a."+active+" span."+active).removeClass('active');

	if (key=='UP'||key=='LEFT')
		var next = $("#dine #order #navigation a."+active).attr("rev");
		else
		if (key=='DOWN'||key=='RIGHT')
			var next = $("#dine #order #navigation a."+active).attr("rel");

	$("#K_order").addClass(next);
	
	$("#dine #order #navigation a."+next+" span."+next).addClass('active');
		
	return;
}

function confirmorder(key)	{
	
	var active  = $("#K_order").attr("class");
	var step    = $("#K_subpanel").text();
	
// CHANGE BEG: 20120214-0800 bsears - dine issue via Tami: change right/left scrolling to traverse buttons when on buttons
	if (key=='RIGHT'||key=='LEFT')	{
		if (active=='prev'||active=='more'||active=='back'||active=='exit'||active=='order')	{	
			nextorder(key);
			return;
		}
	}
// CHANGE BEG: 20120214-0800 bsears - dine issue via Tami: change right/left scrolling to traverse buttons when on buttons	
	
	if (active=='back')	{
		
		backtodine();
		
		return;
	}	else
// CHANGE BEG: 20120203-1100 bsears - high priority change: view order button additions and changes
//		if (active=='more')	{
// CHANGE END: 20120206-0900 bsears - high priority change: implement previous/next scrolling for view order
//		if (active=='more'||active=='prev')	{
		if (active=='more'||active=='prev'||key=='RIGHT'||key=='LEFT')	{	
// CHANGE BEG: 20120206-0900 bsears - high priority change: implement previous/next scrolling for view order
// CHANGE END: 20120203-1100 bsears - high priority change: view order button additions and changes
			
			$("#K_order").removeClass(active);
			
// CHANGE BEG: 20120206-0900 bsears - high priority change: implement previous/next scrolling for view order
			if (key=='RIGHT')
				active='more';
				else
				if (key=='LEFT')
					active='prev';
// CHANGE END: 20120206-0900 bsears - high priority change: implement previous/next scrolling for view order
			
// CHANGE BEG: 20120206-0900 bsears - high priority change: implement previous/next scrolling for view order
//			orderHTML = nextorderHTML();
			orderHTML = nextorderHTML(active);
// CHANGE END: 20120206-0900 bsears - high priority change: implement previous/next scrolling for view order

			$("#dine #order #recipes").html(orderHTML.recipes);
			$("#dine #order #navigation").html(orderHTML.navigation);
			$("#dine #order #navigation.bar").html(orderHTML.navbar);
			
			$("#K_order").addClass(orderHTML.active);
			$("#K_order").text(orderHTML.last);
			$("#K_orderfirst").text(orderHTML.first);
			
			$("#dine #order #navigation a."+orderHTML.active+" span."+orderHTML.active).addClass('active');			
		
			return;
// CHANGE BEG: 20120202-0130 bsears - high priority change: dine exit confirmation panel(s)
		}	else
			if (active=='exit')	{
				
				var exitHTML = dineExitHTML();
				$("#dine #exit").html(exitHTML.exit);

				$("#dine #order #navigation a."+active+" span."+active).removeClass('active');
				$("#K_subpanel").text('exit');

				$("#dine").show();
				$("#dine #cover").show();
				$("#dine #exit a span.no").addClass('active');
				$("#dine #exit").fadeIn('fast');

				return;
// CHANGE END: 20120202-0130 bsears - high priority change: dine exit confirmation panel(s)
		}	else
			if (active=='order')	{
				
// CHANGE BEG: 20120203-0200 bsears - high priority change: implement place order confirmation panel
				
//				$("#K_order").removeClass(active);
//			
//				$("#K_subpanel").text('confirm');
//				
//				var confirmHTML = confirmorderHTML();
//				
//				$("#dine #order").html(confirmHTML.title+confirmHTML.message+confirmHTML.panel);
//				
//				$("#K_order").addClass(confirmHTML.active);
//				$("#dine #order #navigation a."+confirmHTML.active+" span."+confirmHTML.active).addClass('active');

				var placeorderHTML = placeOrderHTML();
				$("#dine #exit").html(placeorderHTML.placeorder);

				$("#dine #order #navigation a."+active+" span."+active).removeClass('active');
				$("#K_subpanel").text('placeorder');

				$("#dine").show();
				$("#dine #cover").show();
				$("#dine #exit a span.no").addClass('active');
				$("#dine #exit").fadeIn('fast');
				
// CHANGE END: 20120203-0200 bsears - high priority change: implement place order confirmation panel
			
				return;
			}
			
	$("#K_subpanel").text('stepQ');
	
	var quantityHTML = getQuantityHTML(active);
	
	$("#K_quantity").text(quantityHTML.quantity);
	$("#K_quantitymax").text(quantityHTML.quantitymax);
	$("#K_quantitynav").addClass(quantityHTML.active);

	$("#dine div#quantity").html(quantityHTML.panel);
	$("#dine div#quantity a."+quantityHTML.active+" span").addClass('active');

	$("#dine div#quantity").show();	

	return;
}

function backtodine()	{
	
	var active = $("#K_order").attr("class");
	
	$("#K_subpanel").text('stepA');
	
	$("#dine #order").html('');
	$("#K_order").removeClass(active);
	$("#K_order").text('');
	
	$("#dine").hide();
	$("#dine #order").hide();
	
	var total = checkvieworder();
	
	if (total==0)	{
		active = $("#K_groups").attr("class");
		$("#K_groups").removeClass(active);
		active = 'mainmenu';
		$("#K_groups").addClass(active);
		$("#secondary.dine #secondaryLeft #choices #choice.dim span.dim").removeClass('active');
		$("#secondary.dine #secondaryLeft #choices #choice."+active+" span."+active).addClass('active');			
	}
	
	$("#secondary.dine #secondaryLeft").show();
	$("#secondary.dine #secondaryRight").show();
	$("#secondary.dine").show();	
	
	return;
}

// CHANGE BEG: 20120203-0200 bsears - high priority change: implement place order confirmation panel

function placeorder(key)	{
	
	var yesno 	= $("#dine #exit a span.active").attr("class");
	var yesnoA  = yesno.split(" ");
	yesno = yesnoA[0];
	
	if (key=='LEFT'||key=='RIGHT')	{
		$("#dine #exit a span."+yesno).removeClass('active');
		if (yesno=='yes')
			$("#dine #exit a span.no").addClass('active');	
			else
			if (yesno=='no')
				$("#dine #exit a span.yes").addClass('active');
	}	else
		if (key=='ENTER')	{
			var active = $("#K_order").attr("class");
			$("#dine #order #navigation a."+active+" span."+active).addClass('active');	
			$("#K_subpanel").text('order');
			$("#dine #exit a span.yes").removeClass('active');
			$("#dine #exit a span.no").removeClass('active');
			$("#dine #exit").hide();
			$("#dine #cover").hide();
			if (yesno=='yes')	{
				submitORDER();
// CHANGE BEG: 20120207-1000 bsears - dine issue via Tami: display error panel if place order submission fails				
				var xmlError = $("#XMLCALL_error").text();
				if(xmlError)	{	
					var errorHTML = submitErrorHTML();
					$("#dine #exit").html(errorHTML.error);
	
					$("#dine #order #navigation a."+active+" span."+active).removeClass('active');
					$("#K_subpanel").text('error');
	
					$("#dine").show();
					$("#dine #cover").show();
					$("#dine #exit a span.close").addClass('active');
					$("#dine #exit").fadeIn('fast');
					return;
				}				
// CHANGE END: 20120207-1000 bsears - dine issue via Tami: display error panel if place order submission fails				
				$("#K_subpanel").text('confirm');
				var confirmHTML = confirmorderHTML();
				$("#dine #order").html(confirmHTML.title+confirmHTML.message+confirmHTML.panel);
				$("#K_order").addClass(confirmHTML.active);
				$("#dine #order #navigation a."+confirmHTML.active+" span."+confirmHTML.active).addClass('active');
			}
		}

	return;
}
// CHANGE END: 20120203-0200 bsears - high priority change: implement place order confirmation panel

// CHANGE BEG: 20120207-1000 bsears - dine issue via Tami: display error panel if place order submission fails
function errorexit()	{

	$("#dine #exit").html('');
	$("#dine #exit").hide();
	$("#dine #cover").hide();
				
	var activeO = $("#K_order").attr("class");
	$("#dine #order").html('');
	$("#K_order").removeClass(activeO);
	$("#K_order").text('');
	$("#dine").hide();
	$("#dine #order").hide();				

	var submenu = $("#K_submenu").attr("class");
	var active  = $("#K_groups").attr("class");
	$("#K_panel").removeClass('secondary');
	$("#K_panel").addClass('primary');
	$("#K_subpanel").removeClass('dine');
	$("#K_subpanel").text('');
	$("#K_submenu").removeClass(submenu);
	$("#submenuitem."+submenu+" span").removeClass('active');
	$("#K_groups").removeClass(active);
	$("#K_groups").text('');
	$("#K_group").removeAttr('class');
	$("#K_group").text('');
	$("#secondary").removeClass('dine');
	$("#secondary").hide();
	$("#primary").show();
	
	return;
}
// CHANGE end: 20120207-1000 bsears - dine issue via Tami: display error panel if place order submission fails

function confirmexit()	{

// CHANGE BEG: 20120203-0200 bsears - high priority change: implement place order confirmation panel
//	submitORDER();
// CHANGE END: 20120203-0200 bsears - high priority change: implement place order confirmation panel	
	
	var submenu = $("#K_submenu").attr("class");
	var active  = $("#K_order").attr("class");
	
	$("#dine #order").html('');
	$("#dine").hide();
	$("#K_order").removeClass(active);
	
	$("#K_panel").removeClass('secondary');
	$("#K_panel").addClass('primary');
	$("#K_subpanel").removeClass('dine');
	$("#K_subpanel").text('');
	$("#K_submenu").removeClass(submenu);
	$("#submenuitem."+submenu+" span").removeClass('active');	
	$("#secondary").removeClass('dine');
	$("#secondary").fadeOut('fast');
	$("#primary").show();
	
	return;
}

function navquantity(key)	{

	var active = $("#K_quantitynav").attr("class");
	
	$("#K_quantitynav").removeClass(active);
	$("#dine div#quantity a."+active+" span."+active).removeClass('active');
	if (key=='LEFT')
		var next = $("#dine div#quantity a."+active).attr("rev");
		else
		if (key=='RIGHT')
			var next = $("#dine div#quantity a."+active).attr("rel");
	$("#K_quantitynav").addClass(next);
	
	$("#dine div#quantity a."+next+" span."+next).addClass('active');
		
	return;
}

function nextquantity(key)	{

	var quantity 	= $("#K_quantity").text();
	var quantitymax = $("#K_quantitymax").text();
	
	if (key=='DOWN')	{
		quantity--;
		if (quantity<0)	
			quantity=quantitymax;
	}
		else
		if (key=='UP')	{
		quantity++;
		if (quantity>quantitymax)	
			quantity=0;
		}
		
	$("#K_quantity").text(quantity);
	$("#K_quantitymax").text(quantitymax);
	
	$("#dine div#quantity div#left p").text(quantity);
		
	return;
}

function selectquantity(key)	{
	
	var active  = $("#K_quantitynav").attr("class");
	
	if (active=='confirm')	{
		
		var recipe	 = $("#K_order").attr("class");
		var quantity = $("#K_quantity").text();
		var first	 = $("#K_orderfirst").text();
		$("#K_order").removeClass(recipe);
		$("#K_order").text('');
		$("#K_orderfirst").text('');
		
// CHANGE BEG: 20120203-0200 bsears - dine issue via Tami: view order, return to same recipe after quantity change
		var prev = $("#dine #order #navigation a."+recipe).attr('rev');
		var next = $("#dine #order #navigation a."+recipe).attr('rel');
		if (quantity==0)	{
			var recipeP = prev.substr(0,1);
			var recipeN = next.substr(0,1);				
			if (recipeP!='R'&&recipeN=='R')	
				first = next;
		}
// CHANGE BEG: 20120203-0200 bsears - dine issue via Tami: view order, return to same recipe after quantity change
		
		//msg(recipe+' '+quantity);
		
		var beg 	= 1;
		var end 	= recipe.length-1;
		//msg(beg+' '+end);
		var recipeX = recipe.substr(beg,end);
		//msg(recipeX);
		
		quantityORDER(recipeX,quantity);
		orderHTML = nextorderHTML(first);
		
		$("#dine #order #recipes").html(orderHTML.recipes);
		$("#dine #order #totals").html(orderHTML.totals);
		$("#dine #order #navigation").html(orderHTML.navigation);
		$("#dine #order #navigation.bar").html(orderHTML.navbar);
		
// CHANGE BEG: 20120203-0200 bsears - dine issue via Tami: view order, return to same recipe after quantity change
		if (quantity!=0)
			orderHTML.active = recipe;
			else	{		
				if (recipeN=='R')	
					orderHTML.active = next;
					else
					if (recipeP=='R')	
						orderHTML.active = prev;
			}
// CHANGE BEG: 20120203-0200 bsears - dine issue via Tami: view order, return to same recipe after quantity change

		$("#K_order").addClass(orderHTML.active);
		$("#K_order").text(orderHTML.last);
		$("#K_orderfirst").text(orderHTML.first);
		
		$("#dine #order #navigation a."+orderHTML.active+" span."+orderHTML.active).addClass('active');	
						
	}	else
		if (active=='cancel')	{
			
		}
		
	$("#K_quantity").text('');	
	$("#K_quantitymax").text('');	
	$("#K_quantitynav").removeClass(active);
	$("#dine div#quantity a."+active+" span."+active).removeClass('active');
	
	$("#K_subpanel").text('order');

	$("#dine div#quantity").hide();
	
	if (orderHTML.total==0)	{			
		backtodine();
	}
	
	return;
}
