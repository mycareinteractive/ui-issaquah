//
// json menu data functions
//

function getMenubar()	{

	var menubar =	{
	
		"type":	"menu",
		"menufirst": "dine",
		"menulast":	"relax",
		"menu":	[	
					{	"tag": "dine",  "label": "Dine", "image": "img_swedish_dine.png" },
					{	"tag": "learn", "label": "Learn", "image": "img_swedish_learn.png"	},
					{	"tag": "visit", "label": "Visit", "image": "img_swedish_visit.png"	},
					{	"tag": "watch", "label": "Watch", "image": "img_swedish_watch.png"	},
					{	"tag": "relax", "label": "Relax", "image": "img_swedish_relax.png"	}
				],
		"text1":  "<p class=\"text1\">Use your TV's remote control to order food, learn more about your health, find out more about Swedish/Issaquah, watch TV and movies, listen to music and more.</p><br />",
		"text2":  "<p class=\"text2\">If you need help using this system, press the large pink call button to ask your nurse for Assistance.</p>",
		"text3":  "<p class=\"text3\">Press the blue-and-white arrow keys to see your different viewing choices.<br />Press the solid blue \"SELECT\" button to<br />make a selection.</p>",
		"image1": "<img class=\"remote\" src=\"images/img_pillowspeaker-swedish.png\" />",
		"image2": "<img class=\"arrows\" src=\"images/img_pillowspeaker-swedish_arrows.png\" />"	,	
	};
	
	return menubar;
}


function getMenu(menu)	{

	var menus = new Array();

	menus['dine'] =	{
					
		"type":	"menu",
		"menufirst": "mymenu",
		"menulast":	"visitorsmenu",
		"dine":			[
						{	"tag": "mymenu", "label": "My Menu", "image": "img_swedish_dine.png"	},
						{	"tag": "visitorsmenu", "label": "Visitors&apos; Menu", "image": "img_swedish_dine.png"	}
						]	
	};
	
	menus['learn'] =	{
					
		"type":	"menu",
		"menufirst": "myprograms",
		"menulast":	"allprograms",
		"learn":		[
						{	"tag": "myprograms", "label": "My Programs", "image": "img_swedish_learn.png"	},
						{	"tag": "allprograms", "label": "All Programs", "image": "img_swedish_learn.png"	}
						]	
	};

	menus['visit'] =	{
					
		"type":	"menu",
		"menufirst": "campusmaps",
		"menulast":	"aboutswedish",
		"visit":		[
						{	"tag": "campusmaps", "label": "Campus Maps", "image": "img_swedish_visit.png" },
						{	"tag": "aboutswedish", "label": "About Swedish", "image": "img_swedish_visit.png" }
						]	
	};

	menus['watch'] =	{
					
		"type":	"menu",
		"menufirst": "watchtv",
		"menulast":	"movies",
		"watch":			[
						{	"tag": "watchtv", "label": "Watch TV", "prev": "movies", "next": "movies", "image": "img_swedish_watch.png"	},
						{	"tag": "movies", "label": "Movies", "prev": "watchtv", "next": "watchtv", "image": "img_swedish_watch.png" }
						]	
	};

	menus['relax'] =	{
		
// CHANGE BEG: 20120214-1300 bsears - change via Dennis: activate musicradio
					
//		"type":	"menu",
//		//"menufirst": "musicradio",
//		"menufirst": "mrfuture",
//		"menulast":	"scenictv",
//		"relax":			[
//					//{	"tag": "musicradio", "label": "Music & Radio", "prev": "scenictv", "next": "scenictv" , "image": "img_swedish_relax.png" },
//					//{	"tag": "scenictv", "label": "Scenic TV", "prev": "musicradio", "next": "musicradio", "image": "img_swedish_relax.png" }
//					{	"tag": "mrfuture", "label": "Music & Radio", "prev": "scenictv", "next": "scenictv" , "image": "img_swedish_relax.png" },
//					{	"tag": "scenictv", "label": "Scenic TV", "prev": "mrfuture", "next": "mrfuture", "image": "img_swedish_relax.png" }
//						]	

		"type":	"menu",
		"menufirst": "musicradio",
		"menulast":	"scenictv",
		"relax":			[
					{	"tag": "musicradio", "label": "Music & Radio", "prev": "scenictv", "next": "scenictv" , "image": "img_swedish_relax.png" },
					{	"tag": "scenictv", "label": "Scenic TV", "prev": "musicradio", "next": "musicradio", "image": "img_swedish_relax.png" }
						]
// CHANGE END: 20120214-1300 bsears - change via Dennis: activate musicradio		
		
	};	

	var submenu = menus[menu];
	
	return submenu;
}


function getChoices(submenu)	{
	
	//msg(submenu);
	
	if (submenu=='movies')	{
		var options = movieCategories();
		return options;
	}	else
		if (submenu=='allprograms')	{
			var options = allprogramsCategories();
			return options;
		}	else
			if (submenu=='myprograms')	{
				var options = myprogramsCategories();
				return options;
			}	else
				if (submenu=='mymenu'||submenu=='visitorsmenu')	{
//					var options = dineCategories();
//					return options;
					return;
				}

	var submenus = new Array();

	submenus['campusmaps'] =	{
		
		"type":	"menu",
		"background":	"",
		"campusmaps":	[	
					{	"tag": "campusmap",  "label": "Campus Map", "image": "" 								},
					{	"tag": "firstfloor", "label": "First Floor Plan", "image": ""							},		
					{	"tag": "directory", "label": "Directory", "image": ""									}
				]
	};	
	
	submenus['aboutswedish'] =	{
		
		"type":	"menu",
		"background":	"",
		"aboutswedish":	[	
					{	"tag": "visitinghours", "label": "Visiting Hours", "image": ""				},
					{	"tag": "shopping",  "label": "Shopping", "image": "" 						},
					{	"tag": "dining", "label": "Dining", "image": ""								},						
					{	"tag": "history", "label": "History", "image": ""							}
				]
	};		
	
	submenus['mymenu'] =	{
		
		"type":	"nomenu",
		"choice": "mymenu",
		"background": "",
		"message":	"Dial Extension<br />34222 on your<br /> bedside phone"
	};			
	
	submenus['visitorsmenu'] =	{
		
		"type":	"nomenu",
		"choice": "visitorsmenu",
		"background": "",
		"message":	"Dial Extension<br />34222 on your<br /> bedside phone"
	};		
	
	submenus['watchtv'] =	{
		
		"type":	"grid",
		"background": "",
		"choice": "watchtv"
	};		
	
	submenus['musicradio'] =	{
		
		"type":	"grid",
		"background": "",
		"choice": "musicradio"
	};	
	
	submenus['mrfuture'] =	{
		
		"type":	"nomenu",
		"choice": "mrfuture",
		"background": "",
		"message":	""
	};	
	
	submenus['scenictv'] =	{
		
		"type":	"grid",
		"background": "",
		"choice": "scenictv"
	};	

	var options = submenus[submenu];
	
	return options;
}


function getChoice(choice)	{
	
	//msg('choice: '+choice);
	
	var submenu = $("#K_submenu").attr("class");
	//msg('submenu: '+submenu);
	if(submenu=='movies')	{
		//msg('movies: '+choice);
		var options = movieSELECTION(choice);
		//msg('movies type: '+options.type);
		return options;		
	}	else
		if(submenu=='allprograms')	{
			//msg('allprograms: '+choice);
			var options = allprogramsSELECTION(choice);
			//msg('myprograms type: '+options.type);
			return options;		
		}	else
			if(submenu=='myprograms')	{
				//msg('myprograms: '+choice);
				var options = myprogramsSELECTION(choice);
				//msg('myprograms type: '+options.type);
				return options;		
			}		else
				if(submenu=='mymenu'||submenu=='visitorsmenu')	{
//					var options = dineSELECTION(choice);
//					return options;	
					return;
				}	
	

	var selections = new Array();
	
	selections['more'] 				=	{ "type":	"navigation" 												};	
	selections['back'] 				=	{ "type":	"navigation" 												};
	selections['mainmenu'] 			=	{ "type":	"navigation" 												};	
	
	selections['mymenu'] 			=	{ "type":	"info", "panel": "mymenu", "content": "Our on-screen menu system isn't quite ready at this time. Please order food<br />by choosing items on the paper menu<br />in your room. Then dial Extension 34222 on your room phone to place your order.","background": "imgs_orderfood.png"	 								};		
	selections['visitorsmenu'] 		=	{ "type":	"info", "panel": "visitorsmenu", "content": "Our on-screen menu system isn't quite ready at this time. Please order food<br />by choosing items on the paper menu<br />in your room. Then dial Extension 34222 on your room phone to place your order.","background": "imgs_orderfood.png"								};	
	
	selections['mrfuture'] 			=	{ "type":	"info", "panel": "mrfuture", "content": "<br /><br />Our music and radio feature is still in production and will be ready soon.","background": ""	 								};			
	
	selections['campusmap'] 		=	{ "type":	"image", "source": "branding_hosp_map_susq.png"	 			};		
	selections['firstfloor'] 		=	{ "type":	"image", "source": "branding_hosp_map_susq2.jpg"	 		};		
	selections['directory'] 		=	{ "type":	"slider", 
										"panel": "directory",
										"panels": "5",
										"instructions": "Use Right and Left Arrow Keys to View More Floors",
										"slides":	[
													 {"title": "First Floor",	"columns": "2",	"column1": "Caf&eacute; 1910<br />Cancer Institute (Oncology)<br />Concierge Desk<br />Emergency Department<br />Imaging Center","column2": "Pharmacy<br />Spiritual Care Services<br />Starbucks<br />Surgery Check-In and Waiting<br />The Shops at Swedish"},  
													 {"title": "Second Floor",	"columns": "1",	"column": "Education & Conference Center<br />OB&frasl;GYN Clinics<br />OB Labor, Delivery & Postpartum Unit<br />Pediatric Inpatient Unit"}, 
													 {"title": "Third Floor",	"columns": "1",	"column": "Intensive Care Unit<br />Medical&frasl;Surgical Unit<br />Specialty Care Clinics &#40;Heart, GI, Colorectal, etc.&#41;"},
													 {"title": "Fourth Floor",	"columns": "1",	"column": "Medical/Surgical Unit<br />Pain & Rehabilitation<br />Specialty Care Clinics &#40;Orthopedics, Neurology, <br /><span>Neurosurgery, etc.&#41;</span>"}, 
													 {"title": "Fifth Floor",	"columns": "1",	"column": "Outpatient Laboratory<br />Primary Care Clinic<br />Specialty Care Clinics &#40;ENT, Pediatric, etc.&#41;"}													 
													 ]									
										};	
										
										
	selections['visitinghours'] 		=	{ "type":	"slider", 
										"panel": "visitinghours",
										"panels": "0",
										"instructions": "",
										"slides":	[
													 {"title": "",	"columns": "",	"column": "Visiting hours at Swedish&frasl;Issaquah are from 7 a.m. to 9 p.m. daily. During these hours, patients may receive guests in their hospital room or in the unit's reception area. There may be times a nurse may ask your visitors to step out of the room for a brief period for patient care privacy.  Siblings are welcome to meet the newest addition of the family.", "image": ""	}												 
													 ]									
										};	
										
	selections['shopping'] 		=	{ "type":	"slider", 
										"panel": "shopping",
										"panels": "4",
										"instructions": "Use Right and Left Arrow Keys to View More Shopping",
										"slides":	[
													 {"title": "",	"columns": "0",	"column": "<img src=\"./images/img_swedish_logo_theshops.png\" />The <span>Shops at Swedish</span> is a unique retail destination dedicated to the health and well-being of patients, families and the greater Eastside community.<br /><br />All are located on the first floor, Monday-Friday 9 a.m.- 6 p.m.", "image": ""	},
													 {"title": "",	"columns": "0",	"column": "<img class=\"bewell\" src=\"./images/img_swedish_logo_bewell.png\" /><span>Be Well</span> - Wellness boutique designed to support healthy lifestyles.<br /><br /><span>Perfect Fit</span> - Fine-lingerie boutique offering luxurious daywear, sleepwear, fashion-forward intimate apparel and basics. We offer a large selection of <img class=\"perfect\" src=\"./images/img_swedish_logo_perfect.png\" />post-operative breast-surgery garments and accessories for cancer survivors.", "image": ""	},
													 {"title": "",	"columns": "0",	"column": "<img class=\"comfort\" src=\"./images/img_swedish_logo_comfort.png\" /><span>Comfort & Joy</span> - Warm and welcoming boutique with pregnant women, new moms and babies in mind. <br /><br /> <img class=\"lily\" src=\"./images/img_swedish_logo_lily.png\" /><span>Lily and Pearl</span> - A distinctly elegant departure from the traditional<br />hospital gift shop.<br /><br /><span>Retail pharmacy</span> available for patients and the<br />general public.  Open 9 a.m.- 5:30 p.m.", "image": ""	},
													 {"title": "",	"columns": "0",	"column": "<img class=\"cafe1910\" src=\"./images/img_swedish_logo_cafe1910.png\" /><span>Caf&eacute; 1910</span> - Named for the year Swedish Hospital was founded, featuring a diverse menu with<br />healthy eating the priority and<br />tastes good.<br /><br /><span>Adventure Kids Playcare</span> - A unique drop-in childcare and entertainment center for<br />kids ages six weeks to 12 years old.", "image": ""	}												 
													 ]									
										};
										
// CHANGE BEG: 20120209-1000 bsears - low priority change: textual changes on Visit->About Swedish->Dining pages
										
//	selections['dining'] 		=	{ "type":	"slider", 
//										"panel": "dining",
//										"panels": "2",
//										"instructions": "Use Right and Left Arrow Keys to View More Dining",
//										"slides":	[
//													 {"title": "",	"columns": "0",	"column": "We have several options to keep your appetite happy.<br/><img class=\"cafe\" src=\"./images/img_swedish_logo_cafe1910.png\" /><span>Cafe 1910</span> - Hospital cafeteria dining re-imagined. Named for the year that Swedish Hospital was founded, Cafe 1910 features a diverse menu with healthy eating the priority - there are no fryers or soda machines.<br /><br /><span>Vending Machines</span> - Located right off the entrance of Cafe 1910 you'll find vending machines that offer a quick snack.", "image": ""	},  
//													 {"title": "",	"columns": "0",	"column": "<span>Starbucks</span> - This is a full-service Starbucks coffee shop<br />ready to serve you your favorite drink or snack whether<br />it&#39;s a coffee, chai tea, chocolate milk or cookie.  Starbucks<br />is open Monday through Friday from 6 a.m.- 6 p.m.<br /><br /><span>A la Carte Dining</span> - Swedish's award-winning A la Carte<br />Dining is at Swedish/Issaquah and available via<br />room-service for all patients and visitors Monday through<br />Friday from 7 a.m.- 8 p.m.", "image": ""	}													 
//													 ]									
//										};	
										
	selections['dining'] 		=	{ "type":	"slider", 
										"panel": "dining",
										"panels": "2",
										"instructions": "Use Right and Left Arrow Keys to View More Dining",
										"slides":	[
													 {"title": "",	"columns": "0",	"column": "We have several options to keep your appetite happy.<br /><br /><img class=\"cafe\" src=\"./images/img_swedish_logo_cafe1910.png\" width=\"240\" /> <span>Caf&eacute; 1910</span> - Hospital dining re-imagined. Named for the year that Swedish Hospital was founded, Caf&eacute; 1910 features a diverse and healthy menu and is open Monday through Friday from 6:30 a.m.-2 p.m.<br /><br /><span>Vending Machines</span> - Located right off the entrance of Caf&eacute; 1910 you'll find vending machines that offer a quick snack.", "image": ""	},  
													 
													 {"title": "",	"columns": "0",	"column": "<span>Starbucks</span> - This is a full-service Starbucks coffee shop<br />ready to serve you. Starbucks is open Monday through Friday 6 a.m.-7 p.m., Saturday and Sunday from 7 a.m.-4 p.m.<br /><br /><span>Caf&eacute; 1910 Room Service</span> - Swedish's award-winning Caf&eacute; 1910 Room Service is available for patients and visitors daily from 7 a.m.-8 p.m.<br /><br /><span>Brewed coffee</span> can be requested from the nursing station after hours between 8p.m. and 7 a.m.", "image": ""	}
													 													 
													 ]									
										};
										
// CHANGE END: 20120209-1000 bsears - low priority change: textual changes on Visit->About Swedish->Dining pages										
										
	selections['history'] 		=	{ "type":	"slider", 
										"panel": "history",
										"panels": "2",
										"instructions": "Use Right and Left Arrow Keys to View More History",
										"slides":	[
													 {"title": "",	"columns": "0",	"column": "Since 1910, Swedish has been the region's hallmark for excellence in health care. Swedish is one of the largest, most comprehensive, nonprofit health-care providers in the region. We have:<br /><img class=\"li2\" src=\"./images/bullet.png\" />Three hospital locations in Seattle, one in Edmonds and the new Issaquah campus<br /><img class=\"li2\" src=\"./images/bullet.png\" />An emergency room and specialty center in Redmond and the Mill Creek area in South-Everett", "image": ""	},  
													 {"title": "",	"columns": "0",	"column": "Since 1910, Swedish has been the region's hallmark for excellence in health care. Swedish is one of the largest, most comprehensive, nonprofit health-care providers in the region. We have:<br /><img class=\"li\" src=\"./images/bullet.png\" />Swedish Home Care<br /><img class=\"li\" src=\"./images/bullet.png\" />Primary Care and Specialty Clinics<br /><br />To learn more about our history and all the services we offer visit us online at <span>swedish.org.</span>", "image": ""	}												 
													 ]									
										};										
	
	selections['watchtv'] 			=	{ "type":	"grid", "choice":	"watchtv" };
	selections['musicradio'] 		=	{ "type":	"grid", "choice":	"musicradio" };
	selections['scenictv'] 			=	{ "type":	"grid", "choice":	"scenictv" };
		
	var options = selections[choice];
	
	return options;
}


function getSelections(panel)	{
		
	var submenu = $("#K_submenu").attr("class");
	if(submenu=='movies')	{
		var options = moviePANEL(panel);
		return options;		
	}	else
		if(submenu=='allprograms')	{
			var options = allprogramsPANEL(panel);
			return options;		
		}
		
}
