//
// common JSON call functions
//

function ewfObject()	{
	// moved configurations into portalconfig.js file at the same level as index.html
	return window.portalConfig;
}

function getRating(code)	{
	
	var rating  	= '';
	var ratingA 	= Array();
	
	ratingA['0'] = 'not rated';
	ratingA['1'] = 'G';
	ratingA['2'] = 'PG';
	ratingA['3'] = 'PG-13';
	ratingA['4'] = 'R';
	ratingA['5'] = 'NC-17';
	ratingA['6'] = 'Adult';
	ratingA['7'] = 'unknown';
	ratingA['8'] = 'unknown';
	ratingA['9'] = 'unknown';
	
	rating = ratingA[code-1];
	
	return rating;
}

function getDevice() {
	
	var version = $("#K_version").text();
	if ( typeof window['EONimbus'] != 'undefined' ) {
		var device_list = Nimbus.getNetworkDeviceList();
		var username = device_list[0];
		var device = Nimbus.getNetworkDevice( username );
		var mac = device.getMAC().replace( /:/gi, '' );
	}
	
	if (!mac)	{
		var client = $("#K_client").text();
		if (client=='SWEDISH')	{		
			var mac = "0021F8033BE1"; // Diabetic
            $('#K_version').text('TEST');
			//var mac = "0021F802B7F8"; // General
			//var mac = "0021F802BAC8"; // Pediatric
		}	else
			if (client=='ACESO')	{
				var mac = "0021F801514b";	
				//var mac = "0016E87DDA62";	
				//var mac = "0021F8015156";	
				//var mac = "0021f802b822";	
			}
	}
	
	//msg(mac);
	
	return mac;
}

function cleanJSON(data)	{
	
	//data = clean3JSON(data);
	//data = clean4JSON(data);
	//data = clean1JSON(data);
	data = clean2JSON(data);
	data = clean5JSON(data);

	return data;
}

function clean1JSON(data)	{

	check1 = data.indexOf('/');
	if (check1 != -1)	{
		while (data != (data = data.replace('/','&#47;')));
	}
	
	return data;
}

function clean2JSON(data)	{

	check2 = data.indexOf('\\');
	if (check2 != -1)	{
		while (data != (data = data.replace('\\','&#47;')));
	}
	
	return data;
}

function clean3JSON(data)	{

	check3 = data.indexOf(',http');
	if (check3 != -1)	{
		while (data != (data = data.replace(',http',' http')));
	}
	
	return data;
}

function clean4JSON(data)	{

	check4 = data.indexOf('http://192.168.200.60:9090/Poster/');
	if (check4 != -1)	{
		while (data != (data = data.replace('http://192.168.200.60:9090/Poster/','')));
	}	
	check4 = data.indexOf('http://10.209.120.28:9090/PICTURE/');
	if (check4 != -1)	{
		while (data != (data = data.replace('http://10.209.120.28:9090/PICTURE/','')));
	}	
	
	return data;
}

function clean5JSON(data)	{

	while (data != (data = data.replace(/[\r\n]/g, '')));
	
	return data;
}

function ajaxJSON(url,args,ignore)	{
	
	$("#CALL_current").html(url+'?'+args);
	
	var data = $.ajax({
		url: url,
		data: args,
		async: false,
		dataType: "json"
		}).responseText;
	
	if (ignore) 						{ msg('Ignoring JSON Returned Data: '+url+'?'+args+'  resulted in: '+data); return; };
	if (!data) 							{ msg('FATAL ERROR!! JSON Data Not Found:  '+url+'?'+args+'  resulted in: NO DATA'); return; };
	if (data==''||data==' ') 			{ msg('FATAL ERROR!! JSON Data Incomplete: '+url+'?'+args+'  resulted in: '+data); return; };
	if (data.indexOf('{0}') != -1) 		{ msg('FATAL ERROR!! JSON Data Incomplete: '+url+'?'+args+'  resulted in: '+data); return; };
	if (data.indexOf('<?xml') != -1)	{ msg('FATAL ERROR!! JSON Data is XML: '+url+'?'+args+'  resulted in: '+data); return; };
	//if (data.indexOf('<html>') != -1)	{ document.write(data+url+'?'+args); return; };
	if (data.indexOf('<html>') != -1)	{ msg('FATAL ERROR!! JSON Data is HTML: '+url+'?'+args+'  resulted in: '+data); return; };	
	
	data = cleanJSON(data);

	$("#EWF_current").html(data);

	var jsondata = eval('(' + data + ')');
	
	return jsondata;
}

function loadEWF(type)	{

	var jsonString = $("#EWF_"+type).text();
	var jsonObject = JSON.parse(jsonString);

	return jsonObject;
}

function loadJSON(type)	{

	var jsonString = $("#JSON_"+type).text();
	jsonString = cleanJSON(jsonString);	
	var jsonObject = JSON.parse(jsonString);

	return jsonObject;
}

function saveJSON(type,jsonObject)	{

	var jsonString = JSON.stringify(jsonObject);
	$("#X_"+type).text(jsonString);
	
	return true;
}

function getJSON(type)	{

	var jsonString = $("#X_"+type).text();
	jsonString = cleanJSON(jsonString);		
	var jsonObject = JSON.parse(jsonString);

	return jsonObject;
}
