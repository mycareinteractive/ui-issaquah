
function validateProgramDATA()	{

	// get channels
	
	var channels = epgChannels();
	
	var channel 	= '';
	var channelfile = '';
	var	startPrev	= '';
	var	endPrev		= '';
	var c = 0;
	var x = 0;
	
	// get channels

	var channelA = Array();
	$.each(channels['Channels'], function(i,row){									
		channelA[c] = row["channelID"];
		c++;
	});	
	x = c - 1;
	
	for(c=0;c<=x;c++)	{ 
		channel = channelA[c];	
		channelfile = epgChannel(channel);

		startPrev	= '';
		endPrev		= '';

		$.each(channelfile.programs, function(i,row){
			if (row["EOF"])	{
				if (endPrev!='2400')
					msg('ERROR for channel '+channel+', '+row["programName"]+' - endTime not 2400: '+row["startTime"]);
				startPrev = '';
				endPrev   = '';										  							  
			}	else
			if (row["programName"])	{
				if (startPrev=='')	{
					if (row["startTime"]!='0000')
						msg('ERROR for channel '+channel+', '+row["programName"]+' - startTime not 0000: '+row["startTime"]);
					startPrev = row["startTime"];
					endPrev   = row["endTime"];
				}	else	{
						if (row["startTime"]!=endPrev)
							msg('ERROR for channel '+channel+', '+row["programName"]+' - startPrev: '+startPrev+' '+endPrev+' startTime: '+row["startTime"]);
						startPrev = row["startTime"];
						endPrev   = row["endTime"];						
					}
			}
		});

	}
	
	return 'success';
}
