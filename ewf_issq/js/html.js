//
// HTML build functions...
//

function initIndexHTML()	{
	
	var kookies		= '';
	var secondary	= '';
	var tertiary	= '';
	var watchtv		= '';
	var dine		= '';

	kookies = kookies+'	<!-- panel cookies -->';
	kookies = kookies+'	<div id="K_panel"></div>';
	kookies = kookies+'	<div id="K_subpanel"></div>';
//	kookies = kookies+'	<!-- primary menu cookies -->';
//	kookies = kookies+'	<div id="K_menu"></div>';
//	kookies = kookies+'	<div id="K_menufirst"></div>';
//	kookies = kookies+'	<!-- primary submenu cookies -->';
//	kookies = kookies+'	<div id="K_submenu"></div>';
//	kookies = kookies+'	<div id="K_submenufirst"></div>';
	kookies = kookies+'	<!-- secondary choice cookies -->';
	kookies = kookies+'	<div id="K_choice"></div>';
	kookies = kookies+'	<div id="K_choicefirst"></div>';
	kookies = kookies+'	<!-- secondary selection cookies -->';
	kookies = kookies+'	<div id="K_selection"></div>';
	kookies = kookies+'	<div id="K_selectionfirst"></div>';
	kookies = kookies+'	<!-- tertiary controls cookies -->';
	kookies = kookies+'	<div id="K_control"></div>';
	kookies = kookies+'	<div id="K_controlfirst"></div>';	
	kookies = kookies+'	<!-- slider cookies -->';
	kookies = kookies+'	<div id="K_slider"></div>';
	kookies = kookies+'	<div id="K_movie"></div>';
	kookies = kookies+'	<div id="K_carousel"></div>';
	kookies = kookies+'	<!-- json object cookies -->';
	kookies = kookies+'	<div id="X_choices"></div>';	
	kookies = kookies+'	<div id="X_selections"></div>';
	kookies = kookies+'	<!-- panel cookies -->';
	kookies = kookies+'	<div id="K_tomorrow"></div>';
	kookies = kookies+'	<div id="K_today"></div>';
	kookies = kookies+'	<div id="K_ymd"></div>';
	kookies = kookies+'	<div id="K_firstchannel"></div>';
	kookies = kookies+'	<div id="K_lastchannel"></div>';
	kookies = kookies+'	<div id="K_grid"></div>';
	kookies = kookies+'	<div id="K_gridactive"></div>';
	kookies = kookies+'	<div id="K_gridpos"></div>';
	kookies = kookies+'	<div id="K_handshake"></div>';
	kookies = kookies+'	<div id="K_channel1"></div>';
	kookies = kookies+'	<div id="K_channel2"></div>';
	kookies = kookies+'	<div id="K_channel3"></div>';
	kookies = kookies+'	<div id="K_channel4"></div>';			
	kookies = kookies+'	<div id="K_channel5"></div>';
	kookies = kookies+'	<div id="K_channel6"></div>';			
	kookies = kookies+'	<div id="K_channel7"></div>';			
	kookies = kookies+'	<div id="K_timebeg"></div>';
	kookies = kookies+'	<div id="K_timeend"></div>';
	kookies = kookies+'	<!-- power cookies -->';
	kookies = kookies+'	<div id="K_power"></div>';
	kookies = kookies+'	<!-- digit keys cookies -->';
	kookies = kookies+'	<div id="K_key"></div>';
	kookies = kookies+'	<div id="K_keycheck"></div>';
	kookies = kookies+'	<!-- secondary dine cookies -->';
// CHANGE BEG: 20120217-0800 bsears - dine issue via Tami: implement menu for exit dine
	kookies = kookies+'	<div id="K_dinestep"></div>';
// CHANGE END: 20120217-0800 bsears - dine issue via Tami: implement menu for exit dine
	kookies = kookies+'	<div id="K_dine"></div>';
	kookies = kookies+'	<div id="K_dinefirst"></div>';
	kookies = kookies+'	<div id="K_group"></div>';
	kookies = kookies+'	<div id="K_groups"></div>';
	kookies = kookies+'	<div id="K_menus"></div>';
	kookies = kookies+'	<div id="K_menusfirst"></div>';
	kookies = kookies+'	<div id="K_recipe"></div>';
	kookies = kookies+'	<div id="K_recipes"></div>';
	kookies = kookies+'	<div id="K_quantity"></div>';
	kookies = kookies+'	<div id="K_quantitymax"></div>';
	kookies = kookies+'	<div id="K_quantitynav"></div>';
	kookies = kookies+'	<div id="K_order"></div>';
	kookies = kookies+'	<div id="K_orderfirst"></div>';
	
	secondary = secondary+'	<div id="secondaryLeft">';
	secondary = secondary+'		<div id="secondaryTitle" class="menu"><p class="menu"></p></div>';
	secondary = secondary+'		<div id="secondaryTitle" class="submenu"><p class="submenu"></p></div>';
	secondary = secondary+'		<div id="choices"></div><!-- #choices -->';
	secondary = secondary+'	</div><!-- #secondaryLeft -->';
	secondary = secondary+'	<div id="clear"></div>';
	secondary = secondary+'	<div id="secondaryRight">';
	secondary = secondary+'	</div><!-- #secondaryRight -->';
	secondary = secondary+'	<div id="clear"></div>';
	
	tertiary = tertiary+'	<div id="tertiaryLeft">';
	tertiary = tertiary+'		<div id="tertiaryTitle" class="menu"><p class="menu"></p></div>';
	tertiary = tertiary+'		<div id="tertiaryTitle" class="submenu"><p class="submenu"></p></div>';
	tertiary = tertiary+'		<div id="controls"></div><!-- #selections -->';
	tertiary = tertiary+'	</div><!-- #tertiaryLeft -->';
	tertiary = tertiary+'	<div id="clear"></div>';
	tertiary = tertiary+'	<div id="tertiaryRight">';
	tertiary = tertiary+'		<div id="controlimage"></div>';
	tertiary = tertiary+'	</div><!-- #tertiaryRight -->';
	tertiary = tertiary+'	<div id="clear"></div>';
	
	watchtv = watchtv+'	<div id="gridinfo"></div><!-- #gridinfo -->';
	watchtv = watchtv+'	<div id="gridbg"></div><!-- #gridbg -->';
	watchtv = watchtv+'	<div id="gridcover"></div><!-- #gridcover -->';
	watchtv = watchtv+'	<div id="gridpanel" class="today"></div><!-- #gridpanel.today -->';
	watchtv = watchtv+'	<div id="gridpanel" class="channel"></div><!-- #gridpanel.channel -->';
	watchtv = watchtv+'	<div id="gridpanel" class="time"></div><!-- #gridpanel.time -->';
	watchtv = watchtv+'	<div id="gridpanel" class="program"></div><!-- #gridpanel.program -->';	
	
	dine = dine+'	<div id="cover"></div><!-- #overlay -->';
	dine = dine+'	<div id="step1"></div><!-- #step1 -->';	
	dine = dine+'	<div id="step2"></div><!-- #step2 -->';
	dine = dine+'	<div id="step3"></div><!-- #step3 -->';
	dine = dine+'	<div id="order"></div><!-- #order -->';
	dine = dine+'	<div id="quantity"></div><!-- #quantity -->';
// CHANGE BEG: 20120202-0130 bsears - high priority change: dine exit confirmation panel(s)
	dine = dine+'	<div id="exit"></div><!-- #exit -->';
// CHANGE END: 20120202-0130 bsears - high priority change: dine exit confirmation panel(s)
	
	var indexHTML = Array();

	indexHTML['kookies'] 	= kookies;
	indexHTML['secondary'] 	= secondary;
	indexHTML['tertiary'] 	= tertiary;
	indexHTML['watchtv'] 	= watchtv;
	indexHTML['dine'] 		= dine;

	return indexHTML;
}

function getPatientHTML()	{
	
	var patientDATA 	= getUserDataDATA();
	var patientstatus	= patientDATA.status;
			
	var patient = '';

	if (patientDATA.phoneNumber)	{
		var patient = patient+'<p class="name">Welcome '+patientDATA.userFullName+'</p>';
		var patient = patient+'<p class="youarein">You are on Level '+patientDATA.roomLevel+': '+patientDATA.roomWing+' Rm '+patientDATA.roomNumber+' </p>';
		var patient = patient+'<p class="youarein">Rm Phone: '+patientDATA.phoneNumber+'</p>';
	}	else	{
			var patient = patient+'<p class="youarein">&nbsp;</p>';
			var patient = patient+'<p class="name">Welcome '+patientDATA.userFullName+'</p>';
			var patient = patient+'<p class="youarein">You are on Level '+patientDATA.roomLevel+': '+patientDATA.roomWing+' Rm '+patientDATA.roomNumber+'</p>';
		}
	
	var patientHTML = Array();

	patientHTML['patient'] = patient;

	return patientHTML;
 }

function getMenuHTML()	{

	var menu = '';
	
	var menubar = getMenubar();
	var menu 	= '';
	var menuitems = '';
	var menufirst = '';
	var menulabel = '';
	var menuimage = '';
	var menucurr  = '';
	var menucurrl = '';
	var menucurrp = '';
	var menunext  = menubar.menufirst;
	var menuprev  = menubar.menulast;
	
    $.each(menubar[menubar.type], function(i,row){
	
		if (i==0)	{
			menucurr  = row["tag"];
			menucurrl = row["label"];
			menucurrp = row["image"];
		}	else	{
				menu = menu+'<a id="menuitem" class="'+menucurr+'" rev="'+menuprev+'" rel="'+row["tag"]+'" title="'+menucurrp+'" >'+menucurrl+'</a>';
				menuitems = menuitems+'<div id="submenu" class="'+menucurr+'"></div>';
				menuprev  = menucurr;
				menucurr  = row["tag"];
				menucurrl = row["label"];
				menucurrp = row["image"];
			}	
		if (menufirst=='')	{
			menufirst = row["tag"];
			menulabel = row["label"];
		}
    });
	menu = menu+'<a id="menuitem" class="'+menucurr+'" rev="'+menuprev+'" rel="'+menunext+'" title="'+menucurrp+'" >'+menucurrl+'</a>';
	menuitems = menuitems+'<div id="submenu" class="'+menucurr+'"></div>';

	var menuimage = '';
	menuimage = menuimage+'<div id="mainpanel">';
	menuimage = menuimage+menubar.text1;
	menuimage = menuimage+menubar.text2;
	menuimage = menuimage+menubar.image1;
	menuimage = menuimage+menubar.image2;
	menuimage = menuimage+menubar.text3;
	menuimage = menuimage+'</div><!-- #mainpanel -->';
	
	var menuHTML = Array();

	menuHTML['menu']  	  = menu;
	menuHTML['menuitems'] = menuitems;
	menuHTML['menufirst'] = menufirst;
	menuHTML['menulabel'] = menulabel;
	menuHTML['menuimage'] = menuimage;

	return menuHTML;	
}

function getSubmenuHTML(menu)	{

	var submenu = getMenu(menu);
	var menuitems = '';
	var submenufirst = '';
	var submenulabel = '';
	var submenuimage = '';
	var submenucurr  = '';
	var submenucurrl = '';
	var submenucurrp = '';
	var submenunext  = submenu.menufirst;
	var submenuprev  = submenu.menulast;
	
    $.each(submenu[menu], function(i,row){
	
		if (i==0)	{
			submenucurr  = row["tag"];
			submenucurrl = row["label"];
			submenucurrp = row["image"];
		}	else	{
				menuitems = menuitems+'<a id="submenuitem" class="'+submenucurr+'" rev="'+submenuprev+'" rel="'+row["tag"]+'" title="'+submenucurrp+'" ><span class="">'+submenucurrl+'</span></a>';
				submenuprev  = submenucurr;
				submenucurr  = row["tag"];
				submenucurrl = row["label"];
				submenucurrp = row["image"];
			}	
		if (submenufirst=='')	{
			submenufirst = row["tag"];
			submenulabel = row["label"];
		}
    });
	menuitems = menuitems+'<a id="submenuitem" class="'+submenucurr+'" rev="'+submenuprev+'" rel="'+submenunext+'" title="'+submenucurrp+'" ><span class="">'+submenucurrl+'</span></a>';
	
	var submenuHTML = Array();

	submenuHTML['submenu']  	= menu;
	submenuHTML['menuitems'] 	= menuitems;
	submenuHTML['submenufirst'] = submenufirst;
	submenuHTML['submenulabel'] = submenulabel;

	return submenuHTML;	
}


function getChoicesHTML(choice,choicelast)	{
	
	var submenu = $("#K_submenu").attr("class");
	
	var choicestart 	= 0;
	var choicetotal 	= 0;
	var choicecapacity 	= 6;

	if (choicelast)	{
		var options = getChoices(choice);
		//var options = getJSON('choices');
		$.each(options[choice], function(i,row){
			if(choicelast==row["tag"])	{
				choicestart = i+1;
			}
			choicetotal++;
		});
		if(choicestart==choicetotal)	{
			choicestart = 0;
		}
		if (choicetotal > choicecapacity)
			var scrolling = true;		
	}	else	{
			var options = getChoices(choice);
			// if no menu to process, run nomenu function and return
			if (options.type=='nomenu')	{
				var choicesHTML = getNOMENU(options);	
				return choicesHTML;
			}	else
			// if grid, retrieve grid and return
			if (options.type=='grid')	{
				var choicesHTML = getGRID(options);
				return choicesHTML;
			}			
			//-------
			$.each(options[choice], function(i,row){
				choicetotal++;
			});	
			if (choicetotal > choicecapacity)
				var scrolling = true;	
			//saveJSON('choices',options);
		}
	
	var choicetype 	= options.type;
	var choices 	= '';
	var choicecount = choicecapacity;
	var choicefirst = '';
	var choicelabel = '';
	var choiceimage = '';
	var choicecurr  = '';		// current tag
	var choicecurrl = '';		// current label
	var choicecurrp = '';		// current image
	var choicenext  = 'back';
	if (scrolling)
		choicenext  = 'more';
	var choiceprev  = 'mainmenu';
	var background  = options.background;
	var choiceimage = '<div id="comments"><p>'+options.comments+'</p></div>';
//	var label    = '';
//	var labelL   = 0;
//	var descrL   = 0;
	
    $.each(options[choice], function(i,row){
//here	
//		label = row["label"];
//		if (submenu=='myprograms')	{
//			labelL = row["label"].length;
//			descrL = row["description"].length;
//			if (descrL<labelL) 
//				label = row["description"];
//		}							 
//here									 
		if (i >= choicestart && choicecount > 0)	{
			if (i==choicestart)	{
				choicecurr  = row["tag"];
				choicecurrl = row["label"];
				choicecurrp = row["image"];
			}	else	{
					choices = choices+'<a id="choice" class="'+choicecurr+'" rev="'+choiceprev+'" rel="'+row["tag"]+'" title="'+choicecurrp+'" ><span class="'+choicecurr+'">'+choicecurrl+'</span></a>';
					choiceprev  = choicecurr;
					choicecurr  = row["tag"];
					choicecurrl = row["label"];	
					choicecurrp = row["image"];
				}	
			choicecount--;
			if (choicefirst=='')	{
				choicefirst = row["tag"];
				if (row["image"]>'')
					choiceimage = '<img src="images/'+row["image"]+'" />';
				choicelabel = row["label"];
			}
		}
    });
	choices = choices+'<a id="choice" class="'+choicecurr+'" rev="'+choiceprev+'" rel="'+choicenext+'" title="'+choicecurrp+'" ><span class="'+choicecurr+'">'+choicecurrl+'</span></a>';
	
	var i=0;
	for (i=1;i<=choicecount;i++)	{
		choices = choices+'<a id="choice" class="filler" rev="" rel="" title="" ><span class="filler">&nbsp;</span></a>';
	}
	
	if (scrolling)	{
		choices = choices+'<a id="choice" class="more" rev="'+choicecurr+'" rel="back" title="" ><span class="more">More &gt;</span></a>';
		choices = choices+'<a id="choice" class="back" rev="more" rel="mainmenu" title="" ><span class="back">BACK</span></a>';
	}	else	{
			choices = choices+'<a id="choice" class="back" rev="'+choicecurr+'" rel="mainmenu" title="" ><span class="back">BACK</span></a>';
		}
	choices = choices+'<a id="choice" class="mainmenu" rev="back" rel="'+choicefirst+'" title="" ><span class="mainmenu">MAIN MENU</span></a>';
	
	choices = '<div id="choicepanel">'+choices+'</div><!-- #choicepanel -->';
	
	var choicesHTML = Array();
	
	choicesHTML['choice'] 	   = choice;
	choicesHTML['choicetype']  = choicetype;
	choicesHTML['choices'] 	   = choices;
	choicesHTML['choicefirst'] = choicefirst;
	choicesHTML['choicelabel'] = choicelabel;
	choicesHTML['choiceimage'] = choiceimage;
	choicesHTML['background']  = background;
	choicesHTML['choicepanel'] = choicefirst;

	return choicesHTML;	
}

function getNOMENU(options)	{
	
	var choices 	= '';
	choices = choices+'<p>'+options.message+'</p>';
	choices = choices+'<a id="choice" class="back" rev="mainmenu" rel="mainmenu" title="" ><span class="back">BACK</span></a>';
	choices = choices+'<a id="choice" class="mainmenu" rev="back" rel="back" title="" ><span class="mainmenu">MAIN MENU</span></a>';
	
	var choicesHTML = Array();
	
	choicesHTML['choicetype']  = options.type;
	choicesHTML['choices'] 	   = choices;
	choicesHTML['choicefirst'] = 'back';
	choicesHTML['choicelabel'] = '';
	choicesHTML['background']  = options.background;
	choicesHTML['choicepanel'] = options.choice;

	return choicesHTML;		
}

function getGRID(options)	{
	
	var choices 	= '';
	choices = choices+'<p>&nbsp;</p>';
	choices = choices+'<a id="choice" class="back" rev="mainmenu" rel="mainmenu" title="" ><span class="back">BACK</span></a>';
	choices = choices+'<a id="choice" class="mainmenu" rev="back" rel="back" title="" ><span class="mainmenu">MAIN MENU</span></a>';
	
	var choicesHTML = Array();
	
	choicesHTML['choicetype']  = options.type;
	choicesHTML['choices'] 	   = choices;
	choicesHTML['choicefirst'] = 'back';
	choicesHTML['choicelabel'] = '';
	choicesHTML['background']  = options.background;
	choicesHTML['choicepanel'] = options.choice;

	return choicesHTML;		
}

function getChoiceHTML(choice)	{
	
	var selections = getSelections(choice);
	
	var bookmark = '<img class="checkmark" src="images/checkmark-swedish.png" />';
	var bm 		 = '';
//	var label    = '';
//	var labelL   = 0;
//	var descrL   = 0;
	
	var choicecount = 6;
	var choicelist = '';
	choicelist = choicelist+'	<div id="choicepanel1">';
	$.each(selections[choice], function(i,row){	
//		labelL = row["label"].length;
//		descrL = row["description"].length;
//		label = row["label"];
//		if (descrL<labelL) 
//			label = row["description"];
		if (row["bookmark"])
			bm = bookmark;
		if (choicecount > 0)	{
			choicelist = choicelist+'		<p>'+bm+row["label"]+'</p>';
			choicecount--;
		}
		bm = '';
	});	
	choicelist = choicelist+'	</div><!-- #choicelist -->';
	choicelist = choicelist+'	<div id="choicepanel2"><img src="images/more.png" width="30"/></div><!-- #choicepanel2 -->';
	choicelist = choicelist+'	<div id="choicepanel3">Press SELECT to Select a Video</div><!-- #choicepanel3 -->';
	choicelist = choicelist+'	<div id="choicepanel4"></div><!-- #choicepanel4 -->';
	
	choicelist = '<div id="choicepanel">'+choicelist+'</div><!-- #choicepanel -->';
	
	var choiceHTML = Array();

	choiceHTML['choice'] 	  = choice;
	choiceHTML['choicelist']  = choicelist;

	return choiceHTML;	
}

function getInfoHTML(options)	{
	
	var info 	= '';
	info = info+'<p class="panelinfo">'+options.content+'</p>';
	
	info = '<div id="choicepanel">'+info+'</div><!-- #choicepanel -->';
	
	var infoHTML = Array();
	
	infoHTML['background']  = options.background;
	infoHTML['choicepanel'] = info;

	return infoHTML;			
	
}

function getCommentsHTML(options)	{

	comments = '<div id="comments"><p>These videos have been specially chosen for you by your medical team<br />to let you know more about the care you&#39;ll receive here at Swedish/Issaquah.<br /><br />You may also find information you&#39;ll need to know about what to do after<br />you go home.</p></div><!-- #comments -->';
	
	var commentsHTML = Array();
	commentsHTML['choicepanel'] = comments;

	return commentsHTML;			
}

function getImageHTML(choice,options)	{
	
	var image 	= '';
	
	if (choice=='campusmap'||choice=='firstfloor')	{
		var youarein = $("#primary #header #patient p.youarein").text();
		image = image+'<p class="panelimage">'+youarein+'<br />';
		image = image+'<img src="./images/'+options.source+'" />';
		image = image+'</p>';		
	}	
	
	var imageHTML = Array();
	
	imageHTML['choicepanel'] = image;

	return imageHTML;			
	
}

function getCarouselHTML(choice)	{
	
	var ewf = ewfObject();
	
	var carouseltotal = 0;
	
	var selections = getSelections(choice);
	$.each(selections[choice], function(i,row){
		carouseltotal++;
	});	
	
	var carouselPOS = Array();
	carouselPOS[0] = 'currentb';
	carouselPOS[1] = 'currenta';
	carouselPOS[2] = 'current';
	carouselPOS[3] = 'currenta';
	carouselPOS[4] = 'currentb';
	carouselPOS[5] = '';
	
	var carouselSTYLE = Array();
	carouselSTYLE[0] = 'height: 103px; width: 75px; margin-top: 75px; margin-left: 0px; display: block; padding: 3px 3px 0px 3px; ';
	carouselSTYLE[1] = 'height: 165px; width: 120px; margin-top: 50px; margin-left: 25px; display: block; padding: 4px 4px 0px 4px; ';
	carouselSTYLE[2] = 'height: 330px; width: 240px; margin-top: 0px; margin-left: 25px; display: block; padding: 4px 5px 0px 5px; ';
	carouselSTYLE[3] = 'height: 165px; width: 120px; margin-top: 50px; margin-left: 25px; display: block; padding: 4px 4px 0px 4px; ';
	carouselSTYLE[4] = 'height: 103px; width: 75px; margin-top: 75px; margin-left: 25px; display: block; padding: 3px 3px 0px 3px; ';
	carouselSTYLE[5] = 'height: 1px; width: 1px; margin-top: 75px; margin-left: 0px; display: none;';

	//var selections = getSelections(choice);
	var carousellimit = 5;
	var ibump	      = 0;
	var id			  = 0;
	var x 			  = 0;
	var carousel = '';
    carousel = carousel+'	<div id="carousel">';
	carousel = carousel+'		<div id="carouselTitle"><p>title goes here</p></div>';
	carousel = carousel+'		<div id="carouselOverlay"></div>';	
	carousel = carousel+'		<div id="carouselPosters">';
	carousel = carousel+'			<img id="'+id+'" class="blank" style="'+carouselSTYLE[5]+'" title="" src="images/mov_poster_BLANK.png" />';
	if (carouseltotal > 3)	{ 
		id++
		carousel = carousel+'			<img id="'+id+'" class="blank" style="'+carouselSTYLE[5]+'" title="" src="images/mov_poster_BLANK.png" />';
	}
	
	if (carouseltotal==1||carouseltotal==2||carouseltotal==3)	{
		id++
		carousel = carousel+'			<img id="'+id+'" class="blank" style="'+carouselSTYLE[0]+'" title="" src="images/mov_poster_BLANK.png" />';
		ibump++
	}
	if (carouseltotal == 1)	{
		id++
		carousel = carousel+'			<img id="'+id+'" class="blank" style="'+carouselSTYLE[1]+'" title="" src="images/mov_poster_BLANK.png" />';
		ibump++
	}	
	
	$.each(selections[choice], function(i,row){
		id++
		if (i > carousellimit)
			x = carousellimit;
			else
				x = i + ibump;
		carousel = carousel+'			<img id="'+id+'" class="'+carouselPOS[x]+'" style="'+carouselSTYLE[x]+'" title="'+row["label"]+'" alt="'+row["tag"]+'" src="'+ewf.Poster+row["poster"]+'" />';
	});
	
	if (carouseltotal==1)	{
		id++
		carousel = carousel+'			<img id="'+id+'" class="blank" style="'+carouselSTYLE[3]+'" title="" src="images/mov_poster_BLANK.png" />';	
		id++
		carousel = carousel+'			<img id="'+id+'" class="blank" style="'+carouselSTYLE[4]+'" title="" src="images/mov_poster_BLANK.png" />';		
	}
	
	id++
	carousel = carousel+'			<img id="'+id+'" class="blank" style="'+carouselSTYLE[5]+'" title="" src="images/mov_poster_BLANK.png" />';
	id++
	carousel = carousel+'			<img id="'+id+'" class="blank last" style="'+carouselSTYLE[5]+'" title="" src="images/mov_poster_BLANK.png" />';
	carousel = carousel+'		</div><!-- #carouselPosters -->';
	carousel = carousel+'		<div id="carouselMessage"><p>Use Left & Right arrow keys to move the Movie Carousel</p></div>';
	carousel = carousel+'	</div><!-- #carousel -->';
	
	carousel = '<div id="choicepanel">'+carousel+'</div><!-- #choicepanel -->';
	
	var carouselHTML = Array();

	carouselHTML['choice']	 		= choice;
	carouselHTML['carousel'] 		= carousel;

	return carouselHTML;	
}

function getSlidesHTML(options)	{
	
	var count   = options.panels;
	var active  = 1;
	var sliders = '';
	var image   = '';
	var columns = 0;
	var slide   = 0;
	var panelD  = 'block';
	
	sliders = sliders+'<div id="slides">';
	
	if (count>0)	{
		
		if (options.panel == 'directory')	{
			var youarein = $("#primary #header #patient p.youarein").text();
			sliders = sliders+'	<div id="youarein">'+youarein+'</div>';
		}

		$.each(options.slides, function(i,row){
			slide = i+1;
			columns = row["columns"];

			sliders = sliders+'	<div id="slide" class="slide'+slide+'" style="display: '+panelD+'";>';
			if (row["title"]>'')
				sliders = sliders+'		<p class="title">'+row["title"]+'</p>';
			if (columns==2)	{
				sliders = sliders+'		<p class="column1">'+row["column1"]+'</p>';
				sliders = sliders+'		<p class="column2">'+row["column2"]+'</p>';
			}	else
			if (columns==1)	{
					sliders = sliders+'		<p class="column">'+row["column"]+'</p>';
			}	else
			if (columns==0)	{
				image   = '';
				if (row["image"]>'')
					image   = '<img src="./images/'+row["image"]+'" />';				
				sliders = sliders+'		<p class="shops">'+image+row["column"]+'</p>';
			}
			sliders = sliders+'	</div>';
			panelD = 'none';
		});	
		if (options.instructions > '')
			sliders = sliders+'	<div id="instructions">'+options.instructions+'</div>';
		
	}	else
	if (count==0)	{
		
		$.each(options.slides, function(i,row){
			sliders = sliders+'	<div id="slide">';
			if (row["title"]>'')
				sliders = sliders+'		<p class="title">'+row["title"]+'</p>';
			sliders = sliders+'		<p class="panel">'+row["column"]+'</p>';
			sliders = sliders+'	</div>';	
		});	

	}	else	{
	


	}
	
	sliders = sliders+'</div>';
	
	var slidesHTML = Array();

	slidesHTML['count']   = count;
	slidesHTML['active']  = active;
	slidesHTML['sliders'] = sliders;

	return slidesHTML;
}

function getVideoHTML(options)	{
	
	var submenu		 = $("#K_submenu").attr("class");
	var menulabel 	 = '<p class="menu">'+$("#K_menu").text()+'</p>';
	var submenulabel = '<p class="submenu">'+$("#K_submenu").text()+'</p>';
	var choicelabel  = $("#K_choice").text();
	var controls 	 = '';
	var controlfirst = 'play';
	var controllabel = 'PLAY VIDEO';
	if (submenu=='allprograms')	{
		var controlfirst = 'bookmark';
		var controllabel = 'BOOKMARK';
		var duration     = options.duration;
	}	else
	if (submenu=='myprograms')	{
		var selection = allprogramsSELECTION(options.tag);
		var duration  = selection.duration;
		var controlfirst = 'play';
		var controllabel = 'PLAY';		
	}		
	
	var controlimage = '<div id="comments"><p>These videos have been specially chosen<br />for you by your medical team to let you know more about the care you&#39;ll receive here at Swedish/Issaquah.<br /><br />You may also find information you&#39;ll need<br />to know about what to do after you go home.</p></div>';	
	
	controls = controls+'<p id="control" class="title">'+options.title+'</p>';
	controls = controls+'<p id="control" class="duration">'+duration+' minutes</p>';
	controls = controls+'<p id="control" >&nbsp;</p>';
	if (submenu=='allprograms')	{
		controls = controls+'<a id="control" class="bookmark" rev="mainmenu" rel="play" title="" ><span class="bookmark">BOOKMARK</span></a>';
		controls = controls+'<a id="control" class="play" rev="bookmark" rel="back" title="" ><span class="play">PLAY VIDEO</span></a>';
		controls = controls+'<a id="control" class="back" rev="play" rel="mainmenu" title="" ><span class="back">BACK</span></a>';
		controls = controls+'<a id="control" class="mainmenu" rev="back" rel="bookmark" title="" ><span class="mainmenu">MAIN MENU</span></a>';		
	}	else
	if (submenu=='myprograms')	{
		controls = controls+'<p id="control" >&nbsp;</p>';
		controls = controls+'<a id="control" class="play" rev="remove" rel="back" title="" ><span class="play">PLAY VIDEO</span></a>';
		controls = controls+'<a id="control" class="back" rev="play" rel="remove" title="" ><span class="back">BACK</span></a>';
		controls = controls+'<a id="control" class="remove" rev="back" rel="play" title="" ><span class="mainmenu">REMOVE</span></a>';		
	}	
	
	var videoHTML = Array();

	videoHTML['source']			= options.source;
	videoHTML['menulabel'] 		= menulabel;	
	videoHTML['submenulabel'] 	= submenulabel;	
	videoHTML['controlfirst'] 	= controlfirst;	
	videoHTML['controllabel'] 	= controllabel;		
	videoHTML['controls'] 		= controls;
	videoHTML['controlimage'] 	= controlimage;

	return videoHTML;		
}

function getMovieHTML()	{
	
	var ewf = ewfObject();
	
	var choice 		= $("#K_choice").attr("class");
	var choicelabel = $("#K_choice").text();
	
	var selection 	= $("#K_selection").attr("class");
	
	var controls	 = '';
	var controlfirst = 'play';
	var controllabel = 'WATCH NOW';
	
	controls = controls+'<p id="control" class="choice">'+choicelabel+'</p>';
	controls = controls+'<p id="control" >&nbsp;</p>';
	controls = controls+'<p id="control" >&nbsp;</p>';
	controls = controls+'<p id="control" >&nbsp;</p>';
	controls = controls+'<a id="control" class="play" rev="mainmenu" rel="back" title="panel.png" ><span class="play">WATCH NOW</span></a>';
	controls = controls+'<a id="control" class="back" rev="play" rel="mainmenu" title="panel.png" ><span class="back">BACK</span></a>';
	controls = controls+'<a id="control" class="mainmenu" rev="back" rel="play" title="panel.png" ><span class="mainmenu">MAIN MENU</span></a>';

	var count  = 0;					//number of movies
	var movies = Array();
		
	var selections = getSelections(choice);
	$.each(selections[choice], function(i,row){
		movies[i] = row["tag"];
		count++;
	});	
	count--;
	
	var movieprev = '';		// first movie
	var movienext  = '';	// last movie
	
	var movie   = '';
	var shrink  = 'display: none;';
	var normal  = 'display: block;';
	var style   = shrink;
	var y = 0;						// loop
	for (y=0; y<=count; y++)	{

		var options  = getChoice(movies[y]);
		
		if (movies[y]==selection)	{
			style = normal;
		}
			
		// determine position of prev movie 
		p = y; p--;
		if (p < 0)
			p = 0;
		// determine position of next movie
		n = y; n++;
		if (n > count)
			n = y;
			
		movie = movie+'	<div id="movie" class="'+movies[y]+'" style="'+style+'">';
		movie = movie+'	<p id="prev" class="'+movies[y]+'" title="'+movies[p]+'" style="display: none; ">';
		movie = movie+'	<p id="next" class="'+movies[y]+'" title="'+movies[n]+'" style="display: none; ">';
		movie = movie+'	<p class="poster"><img alt="'+options.title+'" src="'+ewf.Poster+options.poster+'" /></p>';
		movie = movie+'	<p class="title">'+options.title+'</p>';
		movie = movie+'	<p class="rating">Rated '+options.rating+'</p>';
		movie = movie+'	<p class="duration">'+options.duration+' minutes</p>';
		movie = movie+'	<p class="synop">'+options.synopsis+'</p>';
		movie = movie+'	</div><!-- #movie -->';	
		
		style = shrink;
	}

	movie = movie+'	<div id="message"><p>Use left and right arrows keys to move the carousel</p></div><!-- #message -->';

	var movieHTML = Array();

	movieHTML['source']			= options.source;	
	movieHTML['controlfirst'] 	= controlfirst;	
	movieHTML['controllabel'] 	= controllabel;		
	movieHTML['controls'] 		= controls;
	movieHTML['selection'] 		= selection;
	movieHTML['movie'] 			= movie;

	return movieHTML;	
}

function getSelectionsHTML(selection,selectionlast)	{

	var selectionstart = 0;
	var selectiontotal = 0;
	
	var panels = getSelections(selection);

	if (selectionlast)	{
		//var panels = getSelections(selection);
		//saveJSON('selections',panels);	
		//var panels = getJSON('selections');
		$.each(panels[selection], function(i,row){
			if(selectionlast==row["tag"])	{
				selectionstart = i+1;
			}
			selectiontotal++;
		});	
		//msg('selectionstart: '+selectionstart+' selectiontotal: '+selectiontotal);
		if(selectionstart==selectiontotal)	{
			selectionstart = 0;
		}		
	}
	//else	{
		//	var panels = getSelections(selection);
	//		//saveJSON('selections',panels);
	//}
	
	var choicelist 	   = '';
	var selections 	   = '';
	var selectioncount = 6;
	var selectionfirst = '';
	var selectionlabel = '';
	var selectioncurr  = '';
	var selectioncurrl = '';
	var selectioncurrb = '';
	var selectionnext  = 'more';
	var selectionprev  = 'back';
	var bookmark 	   = '<img class="checkmark" src="images/checkmark-swedish.png" />';
	var bm 		 	   = '';
	
// CHANGE BEG: 20120130-1030 bsears - removed code to eliminate video list jump
//	choicelist = choicelist+'	<div id="choicepanel1">';
// CHANGE END: 20120130-1030 bsears - removed code to eliminate video list jump
	
	$.each(panels[selection], function(i,row){
		if (row["bookmark"])
			bm = bookmark;									   
		if (i >= selectionstart && selectioncount > 0)	{
			if (i==selectionstart)	{
				selectioncurr  = row["tag"];
				selectioncurrl = row["label"];
				selectioncurrb = bm;
			}	else	{
					choicelist = choicelist+'		<p>'+selectioncurrb+selectioncurrl+'</p>';
					selections = selections+'<a id="selection" class="'+selectioncurr+'" rev="'+selectionprev+'" rel="'+row["tag"]+'" ><span class="'+selectioncurr+'">&nbsp;</span></a>';
					selectionprev  = selectioncurr;
					selectioncurr  = row["tag"];
					selectioncurrl = row["label"];
					selectioncurrb = bm;

				}
			selectioncount--;
			if (selectionfirst=='')	{
				selectionfirst = row["tag"];
				selectionlabel = row["label"];
			}
		}
		bm = '';
	});	
	choicelist = choicelist+'		<p>'+selectioncurrl+'</p>';
	selections = selections+'<a id="selection" class="'+selectioncurr+'" rev="'+selectionprev+'" rel="'+selectionnext+'" ><span class="'+selectioncurr+'">&nbsp;</span></a>'
	
	var i=0;
	for (i=1;i<=selectioncount;i++)	{
		selections = selections+'<a id="selection" class="filler" rev="" rel="" ><span class="filler">&nbsp;</span></a>';
	}	
	
	selections = selections+'<a id="selection" class="more" rev="'+selectioncurr+'" rel="back" ><span class="more">More &gt;</span></a>';
	selections = selections+'<a id="selection" class="back" rev="more" rel="'+selectionfirst+'" ><span class="back">BACK</span></a>';
	
// CHANGE BEG: 20120130-1030 bsears - removed code to eliminate video list jump (PENDING)
//	choicelist = choicelist+'	</div><!-- #choicelist -->';
// CHANGE END: 20120130-1030 bsears - removed code to eliminate video list jump
		
	var selectionsHTML = Array();
	
	selectionsHTML['choicelist'] 	 = choicelist;
	selectionsHTML['selection'] 	 = selection;
	selectionsHTML['selections'] 	 = selections;
	selectionsHTML['selectionfirst'] = selectionfirst;
	selectionsHTML['selectionlabel'] = selectionlabel;

	return selectionsHTML;
}

function getDineHTML()	{
	
	var step 		 = $("#K_subpanel").text();
	var submenu 	 = $("#K_submenu").attr("class");
	var submenulabel = $("#K_submenu").text();
	
	if (step=='step1')	{
		
		var patient 	 = loadJSON('patient');
		var name		 = patient.userFullName;
		if (!name)	name = 'NO NAME';
		
		var dine 		 = '';
		
		dine = dine+'	<div id="title"><p>'+submenulabel+' - Welcome to Caf&#233; 1910 Room Service</p></div>';
		dine = dine+'	<div id="hello">';
		dine = dine+'		<p class="greeting">Hello, '+name+'</p>';
		dine = dine+'		<p>Please select who you are ordering for:</p>';
		dine = dine+'	</div>';
		dine = dine+'	<div id="navigation">';
		dine = dine+'		<a class="mymenu" rev="mainmenu" rel="visitorsmenu" title="" ><span class="mymenu">'+name+'</span></a>';
		dine = dine+'		<a class="visitorsmenu" rev="mymenu" rel="neither" title="" ><span class="visitorsmenu">A visitor of '+name+'</span></a>';
		dine = dine+'		<a class="neither" rev="visitorsmenu" rel="back" title="" ><span class="neither">Neither</span></a>';
		dine = dine+'		<a class="back" rev="neither" rel="mainmenu" title="" ><span class="back">BACK</span></a>';
		dine = dine+'		<a class="mainmenu" rev="back" rel="mymenu" title="" ><span class="mainmenu">MAIN MENU</span></a>';
		dine = dine+'	</div>';
		
		return dine;
		
	}	else
	if (step=='step2')	{
		
		var meal 		= '';
		var menutype 	= '';
		var menuname 	= '';
		var description = 'Menu type and description are not available.';		
		
		var mealDATA = getXML('meal');
			
		$(mealDATA).find('MEAL').each(function(){
			meal 		= $(this).attr('meal');
			menutype 	= $(this).attr('menutype');
			menuname 	= $(this).attr('menuname');
			description = $(this).attr('description');
		});
		
		var dine 		 = '';
		
		dine = dine+'	<div id="title"><p>'+submenulabel+' - Welcome to Caf&#233; 1910 Room Service</p></div>';
		dine = dine+'	<div id="message">';
		dine = dine+'		<p>Your menu is based on the diet order that your healthcare provider has prescribed for you. Please note there may be certain items not available based on your current dietary restrictions.</p>';
		dine = dine+'		<p class="menu">Your '+meal+' Menu: '+menuname+'</p>';
		dine = dine+'		<p class="description">'+description+'</p></div>';
		dine = dine+'	</div>';
		dine = dine+'	<div id="navigation">';
		dine = dine+'		<a class="next" rev="back" rel="back" title="" ><span class="next">NEXT</span></a>';
		dine = dine+'		<a class="back" rev="next" rel="next" title="" ><span class="back">BACK</span></a>';
		dine = dine+'	</div>';
		
		return dine;
		
	}	else
	if (step=='step3')	{
		
		var dine 		 = '';
		
		dine = dine+'	<div id="title"><p>'+submenulabel+' - Welcome to Caf&#233; 1910 Room Service</p></div>';
		dine = dine+'	<div id="message">';
		dine = dine+'		<p class="bump">To Place Your Order:</p>';
		dine = dine+'		<p class="indent">1) Make your selections</p>';
		dine = dine+'		<p class="indent">2) Select View order and place your order.</p>';
		if (submenu=='mymenu')
			dine = dine+'		<p class="bump">If you need assistance, have any special requests, or would like to place your order now for a future meal, please call Extension 34222.</p>';
			else
			if (submenu=='visitorsmenu')	{
				dine = dine+'		<p class="indent">3) Your total amount will show and is to be paid at delivery. We accept Cash and Credit Cards. All pricing includes tax. Gratuity not accepted.</p>';
				dine = dine+'		<p class="bump">If you need assistance, have special dietary considerations, or would like to place your order now for a future meal, please call Ext. 34222.</p>';	
			}
		dine = dine+'	</div>';
		dine = dine+'	<div id="navigation">';
		dine = dine+'		<a class="next" rev="back" rel="back" title="" ><span class="next">NEXT</span></a>';
		dine = dine+'		<a class="back" rev="next" rel="next" title="" ><span class="back">BACK</span></a>';
		dine = dine+'	</div>';
		
		return dine;
		
	}	else
	if (step=='stepX')	{
		
		var dine 		 = '';
		
		dine = dine+'	<div id="title"><p>'+submenulabel+' - Welcome to Caf&#233; 1910 Room Service</p></div>';
		dine = dine+'	<div id="neither">';
		dine = dine+'		<p>We&#39;re sorry. On-screen ordering is unavailable at this time.</p>';
		dine = dine+'		<p>&nbsp;</p>';
		dine = dine+'		<p>Please call the Diet Clerk for assistance or to place your order between 7am and 8pm - Extension 34222.</p>';
		dine = dine+'	</div>';
		dine = dine+'	<div id="navigation" class="neither">';
		dine = dine+'		<a class="close" rev="close" rel="close" title="" ><span class="close">CLOSE</span></a>';
		dine = dine+'	</div>';
		
		return dine;
		
	}		

}

// CHANGE BEG: 20120206-1000 bsears - implement previous/next scrolling for publishing groups
//function getGroupsHTML(last)	{
function getGroupsHTML(active)	{
// CHANGE END: 20120206-1000 bsears - implement previous/next scrolling for publishing groups
	
// CHANGE BEG: 20120206-1000 bsears - implement previous/next scrolling for publishing groups
	var last = $("#K_groups").text();
// CHANGE END: 20120206-1000 bsears - implement previous/next scrolling for publishing groups
	
	var groupsDATA = getXML('groups');
	
	var start 	  = 1;
	var finis 	  = 5;
	var total 	  = 0;
	var capacity  = 5;
// CHANGE BEG: 20120206-1000 bsears - implement previous/next scrolling for publishing groups
	var olines 	  = 0;
	var overflow  = 0;
	var remain	  = 0;
// CHANGE END: 20120206-1000 bsears - implement previous/next scrolling for publishing groups
	var count     = 0;
	var countP    = 0;
	var countN    = 0;
	var panel	  = '';
	var panelP	  = 'mainmenu';
	var panelN	  = 'prev';
	var panelF	  = '';
	var panelL	  = '';
	var id		  = '';
	var group	  = '';
	var groupL	  = 0;
	
	var idA    	  = Array();
	var groupA    = Array();
	
	$(groupsDATA).find('group').each(function(){									   													 
		id		= $(this).attr('id');											 
		group 	= $(this).attr('group');
		
		groupL = group.length;
		if(groupL>20) group = group.substr(0,20);		
		
		total++;
		idA[total] 	  = id;
		groupA[total] = group;
		
// CHANGE BEG: 20120206-1000 bsears - implement previous/next scrolling for publishing groups
		if (last==id)	start = total+1;
		if (active=='more'&&last==id)	start = total+1;
		if (active=='prev'&&last==id)	{
			if (total==10||total==15||total==20||total==25)	{
				start = total-9;
			}	else	{
					remain = total;
					for(i=0;remain>5;i++)	{
						remain = remain - 5;
					}
					start = total - (4+remain);
				}
		}
		
// CHANGE END: 20120206-1000 bsears - implement previous/next scrolling for publishing groups
		
		//msg(' last: '+last+' id: '+id+' start: '+start);
	});	
	
	if(start>total)	start = 1;
// CHANGE BEG: 20120206-1000 bsears - implement previous/next scrolling for publishing groups
	if(start<1)		start = 1;
// CHANGE BEG: 20120206-1000 bsears - implement previous/next scrolling for publishing groups
	finis = start+(capacity-1);
// CHANGE BEG: 20120206-1000 bsears - implement previous/next scrolling for publishing groups
	overflow = total-capacity;
	olines   = countORDER();
	
	//msg(' start:'+start+' finis:'+finis+' total:'+total+' overflow:'+overflow+' order lines'+olines);
// CHANGE END: 20120206-1000 bsears - implement previous/next scrolling for publishing groups
	
	for(count=start;count<=finis;count++)	{

		countP = count-1;
		countN = count+1;
		
// CHANGE BEG: 20120206-1000 bsears - implement previous/next scrolling for publishing groups
		panelP = 'mainmenu';
//		panelN = 'more';
		if(overflow>0)
// CHANGE BEG: 20120223-0730 bsears - dine issue via Dennis: swap Previous/Next button positions
//			panelN = 'prev';
			panelN = 'more';
// CHANGE END: 20120223-0730 bsears - dine issue via Dennis: swap Previous/Next button positions
			else
			if(olines>0)
				panelN = 'vieworder';
				else
					panelN = 'mainmenu';
// CHANGE BEG: 20120206-1000 bsears - implement previous/next scrolling for publishing groups

		if(count>start)	
			panelP = idA[countP];
		if(count<finis&&count<total)
			panelN = idA[countN];
		
		panel = panel+'<a id="choice" class="'+idA[count]+'" rev="'+panelP+'" rel="'+panelN+'" title="" ><span class="'+idA[count]+'">'+groupA[count]+'</span></a>';	
		
		if (panelF=='') panelF = idA[count];
		panelL = idA[count];
		
		capacity--;
		if(capacity<0) 		break;
		if(count==total)	break;
	}
	
	for (count=0;capacity>count;capacity--)	{
		panel = panel+'<a id="choice" class="filler" rev="" rel="" title="" ><span class="filler">&nbsp;</span></a>';	
	}
	

// CHANGE BEG: 20120206-1000 bsears - implement previous/next scrolling for publishing groups
//	var total = countORDER();
//	if(total==0)	{
////		panel = panel+'<a id="choice" class="more" rev="'+panelL+'" rel="back" title="" ><span class="more">More &gt;</span></a>';
////		panel = panel+'<a id="choice" class="dim" rev="" rel="" title="" ><span class="dim">View Order</span></a>';		
////		panel = panel+'<a id="choice" class="back" rev="more" rel="mainmenu" title="" ><span class="back">BACK</span></a>';
////		panel = panel+'<a id="choice" class="mainmenu" rev="back" rel="'+panelF+'" title="" ><span class="mainmenu">Exit Dine</span></a>';
//		panel = panel+'<a id="choice" class="more" rev="'+panelL+'" rel="mainmenu" title="" ><span class="more">More &gt;</span></a>';
//		panel = panel+'<a id="choice" class="dim" rev="" rel="" title="" ><span class="dim">View Order</span></a>';		
//		panel = panel+'<a id="choice" class="mainmenu" rev="more" rel="'+panelF+'" title="" ><span class="mainmenu">Exit Dine</span></a>';
//	}	else	{
////			panel = panel+'<a id="choice" class="more" rev="'+panelL+'" rel="vieworder" title="" ><span class="more">More &gt;</span></a>';
////			panel = panel+'<a id="choice" class="vieworder" rev="more" rel="back" title="" ><span class="vieworder">View Order</span></a>';		
////			panel = panel+'<a id="choice" class="back" rev="vieworder" rel="mainmenu" title="" ><span class="back">BACK</span></a>';
////			panel = panel+'<a id="choice" class="mainmenu" rev="back" rel="'+panelF+'" title="" ><span class="mainmenu">Exit Dine</span></a>';
//			panel = panel+'<a id="choice" class="more" rev="'+panelL+'" rel="vieworder" title="" ><span class="more">More &gt;</span></a>';
//			panel = panel+'<a id="choice" class="vieworder" rev="more" rel="mainmenu" title="" ><span class="vieworder">View Order</span></a>';		
//			panel = panel+'<a id="choice" class="mainmenu" rev="vieworder" rel="'+panelF+'" title="" ><span class="mainmenu">Exit Dine</span></a>';
//		}
	if(olines>0)	{
		if (overflow>0)	{
// CHANGE BEG: 20120223-0730 bsears - dine issue via Dennis: swap Previous/Next button positions
//			panel = panel+'<a id="choice" class="prev" rev="'+panelL+'" rel="more" title="" ><span class="prev">Previous</span></a>';
//			panel = panel+'<a id="choice" class="more" rev="prev" rel="vieworder" title="" ><span class="more">Next</span></a>';
//			panel = panel+'<a id="choice" class="vieworder" rev="more" rel="mainmenu" title="" ><span class="vieworder">View Order</span></a>';		
//			panel = panel+'<a id="choice" class="mainmenu" rev="vieworder" rel="'+panelF+'" title="" ><span class="mainmenu">Exit Dine</span></a>';
			panel = panel+'<a id="choice" class="more" rev="'+panelL+'" rel="prev" title="" ><span class="more">Next</span></a>';
			panel = panel+'<a id="choice" class="prev" rev="more" rel="vieworder" title="" ><span class="prev">Previous</span></a>';
			panel = panel+'<a id="choice" class="vieworder" rev="prev" rel="mainmenu" title="" ><span class="vieworder">View Order</span></a>';		
			panel = panel+'<a id="choice" class="mainmenu" rev="vieworder" rel="'+panelF+'" title="" ><span class="mainmenu">Exit Dine</span></a>';
// CHANGE END: 20120223-0730 bsears - dine issue via Dennis: swap Previous/Next button positions
		}	else	{
// CHANGE BEG: 20120215-1700 bsears - dine issue via Tami: prev/next goes green with vieworder
//				panel = panel+'<a id="choice" class="dim" rev="" rel="" title="" ><span class="dim">Previous</span></a>';
//				panel = panel+'<a id="choice" class="dim" rev="" rel="" title="" ><span class="dim">Next</span></a>';
// CHANGE BEG: 20120223-0730 bsears - dine issue via Dennis: swap Previous/Next button positions
//				panel = panel+'<a id="choice" class="dimm" rev="" rel="" title="" ><span class="dimm">Previous</span></a>';
//				panel = panel+'<a id="choice" class="dimm" rev="" rel="" title="" ><span class="dimm">Next</span></a>';
				panel = panel+'<a id="choice" class="dimm" rev="" rel="" title="" ><span class="dimm">Next</span></a>';
				panel = panel+'<a id="choice" class="dimm" rev="" rel="" title="" ><span class="dimm">Previous</span></a>';
// CHANGE END: 20120223-0730 bsears - dine issue via Dennis: swap Previous/Next button positions
// CHANGE END: 20120215-1700 bsears - dine issue via Tami: prev/next goes green with vieworder
				panel = panel+'<a id="choice" class="vieworder" rev="'+panelL+'" rel="mainmenu" title="" ><span class="vieworder">View Order</span></a>';		
				panel = panel+'<a id="choice" class="mainmenu" rev="vieworder" rel="'+panelF+'" title="" ><span class="mainmenu">Exit Dine</span></a>';
			}
	}	else	{
		if (overflow>0)	{
// CHANGE BEG: 20120223-0730 bsears - dine issue via Dennis: swap Previous/Next button positions
//			panel = panel+'<a id="choice" class="prev" rev="'+panelL+'" rel="more" title="" ><span class="prev">Previous</span></a>';
//			panel = panel+'<a id="choice" class="more" rev="prev" rel="mainmenu" title="" ><span class="more">Next</span></a>';
//			panel = panel+'<a id="choice" class="dim" rev="" rel="" title="" ><span class="dim">View Order</span></a>';		
//			panel = panel+'<a id="choice" class="mainmenu" rev="more" rel="'+panelF+'" title="" ><span class="mainmenu">Exit Dine</span></a>';
			panel = panel+'<a id="choice" class="more" rev="'+panelL+'" rel="prev" title="" ><span class="more">Next</span></a>';
			panel = panel+'<a id="choice" class="prev" rev="more" rel="mainmenu" title="" ><span class="prev">Previous</span></a>';
			panel = panel+'<a id="choice" class="dim" rev="" rel="" title="" ><span class="dim">View Order</span></a>';		
			panel = panel+'<a id="choice" class="mainmenu" rev="prev" rel="'+panelF+'" title="" ><span class="mainmenu">Exit Dine</span></a>';
// CHANGE END: 20120223-0730 bsears - dine issue via Dennis: swap Previous/Next button positions
		}	else	{
// CHANGE BEG: 20120215-1700 bsears - dine issue via Tami: prev/next goes green with vieworder			
//				panel = panel+'<a id="choice" class="dim" rev="" rel="" title="" ><span class="dim">Previous</span></a>';
//				panel = panel+'<a id="choice" class="dim" rev="" rel="" title="" ><span class="dim">Next</span></a>';
// CHANGE BEG: 20120223-0730 bsears - dine issue via Dennis: swap Previous/Next button positions
//				panel = panel+'<a id="choice" class="dimm" rev="" rel="" title="" ><span class="dimm">Previous</span></a>';
//				panel = panel+'<a id="choice" class="dimm" rev="" rel="" title="" ><span class="dimm">Next</span></a>';
				panel = panel+'<a id="choice" class="dimm" rev="" rel="" title="" ><span class="dimm">Next</span></a>';
				panel = panel+'<a id="choice" class="dimm" rev="" rel="" title="" ><span class="dimm">Previous</span></a>';
// CHANGE END: 20120223-0730 bsears - dine issue via Dennis: swap Previous/Next button positions
// CHANGE END: 20120215-1700 bsears - dine issue via Tami: prev/next goes green with vieworder
				panel = panel+'<a id="choice" class="dim" rev="" rel="" title="" ><span class="dim">View Order</span></a>';		
				panel = panel+'<a id="choice" class="mainmenu" rev="'+panelL+'" rel="'+panelF+'" title="" ><span class="mainmenu">Exit Dine</span></a>';
			}
		}
// CHANGE END: 20120206-1000 bsears - implement previous/next scrolling for publishing groups
	
	//panel = '<div id="choicepanel">'+panel+'</div><!-- #choicepanel -->';
	
	var groupsHTML = Array();
	
	groupsHTML['groups'] = panel;
	groupsHTML['active'] = panelF;
	groupsHTML['first']  = panelF;
	groupsHTML['last'] 	 = panelL;

	return groupsHTML;	
}

// CHANGE BEG: 20120206-1000 bsears - implement previous/next scrolling for publishing groups
function groupsOverflow()	{
	
	var groupsDATA = getXML('groups');
	var total	   = 0;
	var capacity   = 5;
	
	$(groupsDATA).find('group').each(function(){									   													 
		total++;
	});	
	
	var overflow = total-capacity;

	return overflow;	
}
// CHANGE END: 20120206-1000 bsears - implement previous/next scrolling for publishing groups

function dinePanelHTML()	{
	
	var dine  = '';
	var error = '';
	var exit  = '';
	
	error = error+'<p>We&#39;re sorry</p>';
	error = error+'<p>You have reached the allowable</p>';
	error = error+'<p>limit for this category.</p>';
	error = error+'<p>&nbsp;</p>';
	error = error+'<p>Please call x34222</p>';
	error = error+'<p>for assistance</p>';
	error = error+'<a class="close" rev="close" rel="close" title="" ><span class="close">CLOSE</span></a>';
	
	exit = exit+'<p>&nbsp;</p>';
	exit = exit+'<p>If you leave this area, your order selections will be lost.</p>';
	exit = exit+'<p>&nbsp;</p>';
	exit = exit+'<p>&nbsp;</p>';
	exit = exit+'<p>Are you sure you want to exit?</p>';
	exit = exit+'<p>&nbsp;</p>';
	exit = exit+'<a class="yes" rev="no" rel="no" title="" ><span class="yes">Yes</span></a>';
	exit = exit+'<a class="no" rev="yes" rel="yes" title="" ><span class="no">No</span></a>';

	dine = dine+'	<div id="choicepanel">';
	dine = dine+'		<div id="choicepanel0"></div><!-- #choicepanel0 -->';
	dine = dine+'		<div id="choicepanel5"></div><!-- #choicepanel5 -->';
	dine = dine+'		<div id="choicepanel6"></div><!-- #choicepanel6 -->';
	dine = dine+'		<div id="choicepanel7"></div><!-- #choicepanel7 -->';
// CHANGE BEG: 20120202-0130 bsears - high priority change: dine exit confirmation panel(s)
//	dine = dine+'		<div id="choicepanel8">'+exit+'</div><!-- #choicepanel8 -->';
// CHANGE END: 20120202-0130 bsears - high priority change: dine exit confirmation panel(s)
	dine = dine+'		<div id="choicepanel9">'+error+'</div><!-- #choicepanel9 -->';
	dine = dine+'	</div><!-- #choicepanel -->';
	
	var panelHTML = Array();

	panelHTML['dine']	= dine;

	return panelHTML;	
}

function getRecipesHTML(group,bkwd)	{
	
	var last = $("#K_recipes").text();
	
	getRECIPES(group);
		
	var recipesDATA = getXML('recipes');
	
	var start 	  = 1;
	var finis 	  = 7;
	var total 	  = 0;
	var capacity  = 7;
	var remain 	  = 0;
	var overflow  = 0;
	var count     = 0;
	var countP    = 0;
	var countN    = 0;
	var panel	  = '';
	var panelP	  = 'back';
	var panelN	  = 'more';
	var panelF	  = '';
	var panelL	  = '';
	var id		  = '';
	var recipe	  = '';
	var recipes	  = '';
	var recipeL	  = 0;
	
	var idA    	     = Array();
	var recipeA      = Array();
	var descriptionA = Array();
	var orderedA 	 = Array();
	
	$(recipesDATA).find('RECIPE').each(function(){									   													 
		id			= $(this).attr('id');											 
		recipe 		= $(this).attr('recipe');
		description = $(this).attr('description');
		
		recipeL = description.length;
		if(recipeL>30) description = description.substr(0,30);				
		
		total++;
		idA[total] 	  		= id;
		recipeA[total]      = recipe;
		descriptionA[total] = description;
		orderedA[total] 	= '';
		
		ordered = checkORDER(id);
		if (ordered)
			orderedA[total] = 'ordered';
			
		if (last==id)	{
			if (bkwd)	{
				if (total==14||total==21||total==28||total==35)	{
					start = total-13;
				}	else	{
						remain = total;
						for(i=0;remain>7;i++)	{
							remain = remain - 7;
						}
						start = total - (6+remain);
					}
			}	else	{
					start = total+1;
				}
		}

	});	
	
	if(start>total)	start = 1;
	if(start<1)	start = 1;
	finis = start+(capacity-1);
	overflow = total-capacity;
	
	for(count=start;count<=finis;count++)	{

		countP = count-1;
		countN = count+1;
		
		if(overflow>0)	{
			panelP = 'back';
			panelN = 'more';
		}	else	{
				panelP = 'back';
				panelN = 'back';			
			}

		if(count>start)	
			panelP = idA[countP];
		if(count<finis&&count<total)
			panelN = idA[countN];
		
		recipes = recipes+'		<p class="'+idA[count]+'"><span class="'+orderedA[count]+'">&nbsp;</span>'+descriptionA[count]+'</p>';
		panel = panel+'		<a id="selection" class="'+idA[count]+'" rev="'+panelP+'" rel="'+panelN+'" title="'+descriptionA[count]+'" ><span class="'+idA[count]+'">&nbsp;</span></a>';
		
		if (panelF=='') panelF = idA[count];
		panelL = idA[count];
		
		capacity--;
		if(capacity<0) 		break;
		if(count==total)	break;
	}
	
	for (count=0;capacity>count;capacity--)	{
		recipes = recipes+'		<p>&nbsp;</p>';
		panel = panel+'	<a id="selection" class="filler" rev="" rel="" title="" ><span class="filler">&nbsp;</span></a>';	
	}
	
	recipes = recipes+'		<p class="overflow">&nbsp;</p>';
	
	if(overflow>0)	{
		panel = panel+'		<a id="selection" class="more" rev="'+panelL+'" rel="back" ><span class="more">More &gt;</span></a>';
		panel = panel+'		<a id="selection" class="back" rev="more" rel="'+panelF+'" ><span class="back">Menu</span></a>';
	}	else	{
			panel = panel+'		<a id="selection" class="more" rev="" rel="" ><span class="dim">More &gt;</span></a>';
			panel = panel+'		<a id="selection" class="back" rev="'+panelL+'" rel="'+panelF+'" ><span class="back">Menu</span></a>';		
		}
	
	var nutrients = getNutrientsHTML(panelF);
	
	var recipesHTML = Array();
	
	recipesHTML['recipes']   	= recipes;
	recipesHTML['panel']     	= panel;
	recipesHTML['nutrients'] 	= nutrients;
	recipesHTML['active']    	= panelF;
	recipesHTML['last']      	= panelL;
	recipesHTML['start']     	= start;
	recipesHTML['overflow']  	= overflow;
	//recipesHTML['instruction'] 	= '<p>Use Right/Left to scroll pages of items</p>';
	recipesHTML['instruction'] 	= '<p>Use right/left arrow to page forward/back through items</p>';

	return recipesHTML;
}

function getNutrientsHTML(recipe)	{
	
	var submenu = $("#K_submenu").attr("class");
	
	var recipesDATA   = getXML('recipes');
	var nutrientsDATA = getXML('nutrients');
	
	var description 	= '';
	var longdescription = '';
	var price		    = 0.00;
	var nutrients   	= '';
	var nutrient    	= '';
	var title 			= '';
	var measure   		= '';
	var panel 			= '';
	var count 			= 0;
	
	$(recipesDATA).find('RECIPE').each(function(){									   													 
		id				= $(this).attr('id');
		if(recipe==id)	{
			description 	= $(this).attr('description');
			longdescription = $(this).attr('longdescription');
			price			= $(this).attr('price');
			nutrients 		= $(this).attr('nutrients');
			nutrient    	= nutrients.split("|");
		}
	});
	
	if (longdescription!='')	
		description = longdescription;
			
	price = roundNumber(price,2);

	panel = panel+'		<p class="description">'+description+'</p>';
	if (submenu=='visitorsmenu')
		panel = panel+'		<p class="price">$'+price+'</p>';
		else
			panel = panel+'		<p class="price">&nbsp;</p>';
	panel = panel+'		<p class="nutrients">';
	
	$(nutrientsDATA).find('NUTRIENT').each(function(){									   													 
		title 	= $(this).attr('title');
		measure = $(this).attr('measure');
		if(title=='Calories') measure='&nbsp;';
		panel = panel+'			<span class="left">'+title+'</span><span class="farright">'+measure+'</span><span class="right">'+nutrient[count]+'</span>';
		count++;
	});	
	
	panel = panel+'		</p>';
	
	return panel;		
}

function vieworderHTML()	{
	
	var submenu = $("#K_submenu").attr("class");
	
	var nutrientsDATA = getXML('nutrients');

	var panel 	= '';
	var headX 	= '';
	var headC   = '';	


	panel = panel+'		<div id="title"><p>View Order</p></div>';
	
	panel = panel+'		<div id="heading">';
	panel = panel+'			<p>';
	panel = panel+'			<span>Qty&nbsp;Item</span>';
	
	if (submenu=='mymenu')
		panel = panel+'			<span class="price">&nbsp;</span>';
		else
		if (submenu=='visitorsmenu')
			panel = panel+'			<span class="price">Price</span>';
	
	$(nutrientsDATA).find('NUTRIENT').each(function(){
		titleX = $(this).attr('title');
		titleC = 'nutrient';
		if(titleX=='Calories')	{
			titleX = 'Cal.';
			titleC = 'calories';
		}
		if(titleX=='Cholestrol')	{
			titleX = 'Chol.';
		}
		panel = panel+'			<span class="'+titleC+'">'+titleX+'</span>';
	});	
	
	panel = panel+'			</p>';
	panel = panel+'		</div>';
	
	panel = panel+'		<div id="recipes"></div>';
	panel = panel+'		<div id="totals"></div>';
	panel = panel+'		<div id="navigation"></div>';
	panel = panel+'		<div id="navigation" class="bar"></div>';

	var orderHTML = Array();

	orderHTML['panel']		= panel;
	
	return orderHTML;
			
}

// CHANGE BEG: 20120206-0900 bsears - high priority change: implement previous/next scrolling for view order
//function nextorderHTML(first)	{
function nextorderHTML(active)	{
// CHANGE END: 20120206-0900 bsears - high priority change: implement previous/next scrolling for view order
	
	var submenu = $("#K_submenu").attr("class");
	
	var orderDATA  	= getXML('order');
	var last 		= $("#K_order").text();
	
	var recipes 	= '';
	var totals 		= '';
	var navigation	= '';
	var navbar		= ''
	
	var start 	  = 0;
	var finis 	  = 5;
	var total 	  = 0;
	var remain 	  = 0;
	var capacity  = 5;
	var overflow  = 0;
	var count     = 0;
	var countP    = 0;
	var countN    = 0;
	var panel	  = '';
// CHANGE BEG: 20120203-1100 bsears - high priority change: view order button additions and changes
//	var panelP	  = 'more';
//	var panelN	  = 'order';
	var panelP	  = 'order';
	var panelN	  = 'prev';
// CHANGE END: 20120203-1100 bsears - high priority change: view order button additions and changes
	var panelF	  = '';
	var panelL	  = '';

	var recipe 		= '';
	var quantity   	= '';
	var description = '';
	var nutrients	= '';
	var nutrient	= '';
	var nutrientC	= '';
	var nutrientV	= 0;
	var nutrientL	= 0;
	var recipeL		= 0;
	var n			= 0;
	
	var recipeA      = Array();
	var quantityA    = Array();
	var descriptionA = Array();
	var nutrientsA 	 = Array();
	var priceA 	 	 = Array();
	var nutrientT    = Array();
	var priceT 	 	 = 0.00;
	
	for(count=0;count<10;count++)	{
		nutrientT[count] = 0;
	}
	
	$(orderDATA).find('orderitem').each(function(){						   													 
		recipe 		= $(this).attr('recipe');
		quantity 	= $(this).attr('quantity');
		description	= $(this).attr('description');
		nutrients	= $(this).attr('nutrients');
		price		= $(this).attr('price');
		
//		recipeL = description.length;
//		if(recipeL>30) description = description.substr(0,30);		
		
		total++;
		
		recipeA[total]      = 'R'+recipe;
		quantityA[total] 	= quantity;
		descriptionA[total] = description;
		nutrientsA[total] 	= nutrients;
		
		price = price * quantity;
		priceT = priceT + price;
		price = roundNumber(price,2);
		priceA[total] = price;
		
		//msg('price: '+description+' '+quantity+' '+price+' '+priceT);
		
		nutrient    = nutrients.split("|");
		nutrientL   = nutrient.length-1;
		
		for(count=0;count<nutrientL;count++)	{
			nutrientV = nutrient[count]*quantity;
//			nutrientV = roundNumber(nutrientV,1);
			nutrientT[count] = nutrientT[count] + nutrientV;
//			nutrientT[count] = roundNumber(nutrientT[count],1);
			
			//msg('price: '+description+' '+quantity+' '+nutrient[count]+' '+nutrientT[count]);
		}	
		
// CHANGE BEG: 20120206-0900 bsears - high priority change: implement previous/next scrolling for view order
//		if (start==0&&first==recipeA[total])
		if (start==0&&active==recipeA[total])		// think `first`
// CHANGE END: 20120206-0900 bsears - high priority change: implement previous/next scrolling for view order
			start = total;
// CHANGE BEG: 20120206-0900 bsears - high priority change: implement previous/next scrolling for view order			
		if (active=='prev'&&start==0&&last==recipeA[total])	{
			if (total==10||total==15||total==20||total==25||total==30||total==35||total==40||total==45||total==50)	{
				start = total-9;
			}	else	{
					remain = total;
					for(i=0;remain>5;i++)	{
						remain = remain - 5;
					}
					start = total - (4+remain);
				}			
		}
// CHANGE END: 20120206-0900 bsears - high priority change: implement previous/next scrolling for view order			
		if (start==0&&last==recipeA[total])	
			start = total+1;

		
		//msg('start: '+start+' last:'+last+' recipe:'+recipe);
			
	});
	
	if(start>total)	start = 1;
	if(start==0)	start = 1;
// CHANGE BEG: 20120206-0900 bsears - high priority change: implement previous/next scrolling for view order
	if(start<0)	start = 1;
// CHANGE END: 20120206-0900 bsears - high priority change: implement previous/next scrolling for view order
	if(total==capacity) start = 1;
	finis = start+(capacity-1);
	overflow = total-capacity;	
	
	for(count=start;count<=finis;count++)	{
		
		if(total==0) break;

		countP = count-1;
		countN = count+1;
		
// CHANGE BEG: 20120203-1100 bsears - high priority change: view order button additions and changes
		
//		if (overflow>0)	{
//			panelP = 'more';
//			panelN = 'order';
//		}	else	{
//				panelP = 'back';
//				panelN = 'order';			
//			}	

		if (overflow>0)	{
			panelP = 'order';
			panelN = 'prev';
		}	else	{
				panelP = 'order';
				panelN = 'back';			
			}
			
// CHANGE END: 20120203-1100 bsears - high priority change: view order button additions and changes

		if(count>start)	
			panelP = recipeA[countP];
		if(count<finis&&count<total)
			panelN = recipeA[countN];
			
		recipes = recipes+'			<p class="'+recipeA[count]+'">';
		recipes = recipes+'			<span class="quantity">'+quantityA[count]+'</span>';
		recipes = recipes+'			<span class="description">'+descriptionA[count]+'</span>';
		
		if (submenu=='mymenu')
			recipes = recipes+'			<span class="price">&nbsp;</span>';
			else
			if (submenu=='visitorsmenu')
				recipes = recipes+'			<span class="price">$'+priceA[count]+'</span>';
		
		nutrients = nutrientsA[count];

		nutrient    = nutrients.split("|");
		nutrientL   = nutrient.length-1;
		
		for(n=0;n<nutrientL;n++)	{
			nutrientV = nutrient[n]*quantityA[count];
			nutrientV = roundNumber(nutrientV,1);
			nutrientC = 'nutrient';
			if(n==0)
				nutrientC = 'calories';
			recipes = recipes+'			<span class="'+nutrientC+'">'+nutrientV+'</span>';
		}
		
		recipes = recipes+'			</p>';				
			
		navigation = navigation+'			<a id="selection" class="'+recipeA[count]+'" rev="'+panelP+'"   rel="'+panelN+'" ><span class="'+recipeA[count]+'">&nbsp;</span></a>';	
		
		if (panelF=='') panelF = recipeA[count];
		panelL = recipeA[count];
		
		capacity--;
		if(capacity<0) 		break;
		if(count==total)	break;
	}
	
	for (count=0;capacity>count;capacity--)	{
		recipes = recipes+'		<p>&nbsp;</p>';
		navigation = navigation+'	<a id="selection" class="filler" rev=""   rel="" ><span class="filler">&nbsp;</span></a>';	
	}
	
// CHANGE BEG: 20120203-1100 bsears - high priority change: view order button additions and changes
	
//	if (total==0)	{
//		navbar = navbar+'			<a id="selection" class="dim" rev="" rel="" ><span class="dim">PLACE ORDER</span></a>';
//		navbar = navbar+'			<a id="selection" class="back" rev="back" rel="back" ><span class="back">BACK</span></a>';
//		navbar = navbar+'			<a id="selection" class="dim" rev="" rel="" ><span class="dim">More &gt;</span></a>';		
//	}	else
//		if (overflow>0)	{
//			navbar = navbar+'			<a id="selection" class="order" rev="'+panelL+'" rel="back" ><span class="order">PLACE ORDER</span></a>';
//			navbar = navbar+'			<a id="selection" class="back" rev="order" rel="more" ><span class="back">BACK</span></a>';
//			navbar = navbar+'			<a id="selection" class="more" rev="back" rel="'+panelF+'" ><span class="more">More &gt;</span></a>';
//		}	else	{
//				navbar = navbar+'			<a id="selection" class="order" rev="'+panelL+'" rel="back" ><span class="order">PLACE ORDER</span></a>';
//				navbar = navbar+'			<a id="selection" class="back" rev="order" rel="'+panelF+'" ><span class="back">BACK</span></a>';
//				navbar = navbar+'			<a id="selection" class="dim" rev="" rel="" ><span class="dim">More &gt;</span></a>';		
//			}
			
	if (total==0)	{
		navbar = navbar+'			<a id="selection" class="dim" rev="" rel="" ><span class="dim">Previous</span></a>';
		navbar = navbar+'			<a id="selection" class="dim" rev="" rel="" ><span class="dim">Next</span></a>';
		navbar = navbar+'			<a id="selection" class="back" rev="exit" rel="exit" ><span class="back">Order More</span></a>';
		navbar = navbar+'			<a id="selection" class="exit" rev="back" rel="back" ><span class="exit">Exit Dine</span></a>';
		navbar = navbar+'			<a id="selection" class="dim" rev="" rel="" ><span class="dim">PLACE ORDER</span></a>';	
	}	else
		if (overflow>0)	{
			navbar = navbar+'			<a id="selection" class="prev" rev="'+panelL+'" rel="more" ><span class="prev">Previous</span></a>';
			navbar = navbar+'			<a id="selection" class="more" rev="prev" rel="back" ><span class="more">Next</span></a>';
			navbar = navbar+'			<a id="selection" class="back" rev="more" rel="exit" ><span class="back">Order More</span></a>';
			navbar = navbar+'			<a id="selection" class="exit" rev="back" rel="order" ><span class="exit">Exit Dine</span></a>';
			navbar = navbar+'			<a id="selection" class="order" rev="exit" rel="'+panelF+'" ><span class="order">PLACE ORDER</span></a>';
		}	else	{
				navbar = navbar+'			<a id="selection" class="dim" rev="" rel="" ><span class="dim">Previous</span></a>';
				navbar = navbar+'			<a id="selection" class="dim" rev="" rel="" ><span class="dim">Next</span></a>';
				navbar = navbar+'			<a id="selection" class="back" rev="'+panelL+'" rel="exit" ><span class="back">Order More</span></a>';
				navbar = navbar+'			<a id="selection" class="exit" rev="back" rel="order" ><span class="exit">Exit Dine</span></a>';
				navbar = navbar+'			<a id="selection" class="order" rev="exit" rel="'+panelF+'" ><span class="order">PLACE ORDER</span></a>';	
			}
			
// CHANGE END: 20120203-1100 bsears - high priority change: view order button additions and changes
	
	priceT = roundNumber(priceT,2);
	
	totals = totals+'			<p>';
	totals = totals+'			<span class="underline">&nbsp;</span>';
	totals = totals+'			<span>TOTALS</span>';
	
	if (submenu=='mymenu')
		totals = totals+'			<span class="price">&nbsp;</span>';
		else
		if (submenu=='visitorsmenu')
			totals = totals+'			<span class="price">$'+priceT+'</span>';

	for(count=0;count<nutrientL;count++)	{
		nutrientV = nutrientT[count];
		nutrientV = roundNumber(nutrientV,1);
		nutrientC = 'nutrient';
		if(count==0)
			nutrientC = 'calories';
		totals = totals+'			<span class="'+nutrientC+'">'+nutrientV+'</span>';
	}
	
	totals = totals+'			</p>';
	totals = totals+'			<p>';
	totals = totals+'			<span class="instruction">To remove an item or change quantities, please Select an Item on the list</span>';
	totals = totals+'			</p>';

	var orderHTML = Array();

	orderHTML['recipes']	= recipes;	
	orderHTML['totals']		= totals;	
	orderHTML['navigation']	= navigation;
	orderHTML['navbar']		= navbar;
// CHANGE BEG: 20120203-1100 bsears - high priority change: view order button additions and changes
//	orderHTML['active']		= 'order';
	orderHTML['active']		= panelF;
// CHANGE END: 20120203-1100 bsears - high priority change: view order button additions and changes
	orderHTML['first']		= panelF;
	orderHTML['last']		= panelL;
	orderHTML['total']		= total;
	if (total==0)
		orderHTML['active']	= 'back';

	return orderHTML;	
	

}

function getQuantityHTML(recipe)	{
	
	var	r = (recipe.length - 1);
	recipe = recipe.substr(1,r);
	
	var orderDATA  	= getXML('order');
	
	var quantity 	= 1;
	var quantitymax	= 1;
	var group		= '';
	var panel 		= '';
	var active   	= 'confirm';
	
	$(orderDATA).find('orderitem').each(function(){									   													 
		if(recipe==$(this).attr('recipe'))	{
			group 	 = $(this).attr('group');
			quantity = $(this).attr('quantity');
		}
	});
	
	if (group=='Condiments')	{
		quantitymax = 9;
	}	else	{
		var submenu = $("#K_submenu").attr("class");
		if (submenu=='visitorsmenu')
			quantitymax	= 9;
		}
	
	panel = panel+'		<div id="left">';
	panel = panel+'			<p>'+quantity+'</p>';
	panel = panel+'		</div>'; 
	panel = panel+'		<div id="right">';
	panel = panel+'			<p>Use Up/Down keys to change quantity.</p>';
	panel = panel+'			<p class="zero">"0" removes item from order</p>';
	panel = panel+'			<p class="bump">Please call x34222 for assistance with additional quantities.</p>';
	panel = panel+'		</div>'; 
	panel = panel+'		<a class="confirm" rev="cancel" rel="cancel" ><span class="confirm">Confirm</span></a>';
	panel = panel+'		<a class="cancel" rev="confirm" rel="confirm" ><span class="cancel">Cancel</span></a>';

	var quantityHTML = Array();

	quantityHTML['quantity']	= quantity;
	quantityHTML['quantitymax']	= quantitymax;
	quantityHTML['panel']		= panel;
	quantityHTML['active']		= active;

	return quantityHTML;		
}


// CHANGE BEG: 20120202-0130 bsears - high priority change: dine exit confirmation panel(s)
function dineExitHTML()	{
	
	var exit  = '';
	
	exit = exit+'<div id="title">';
	exit = exit+'	<p>Exit Dine</p>';
	exit = exit+'</div>';
	exit = exit+'<div id="message">';
	exit = exit+'	<p>&nbsp;</p>';
	exit = exit+'	<p>Are you sure you would like to Exit Dine?</p>';
	exit = exit+'	<p>&nbsp;</p>';
	exit = exit+'	<p>Answering NO returns to your Order.</p>';
	exit = exit+'	<p>Answering YES cancels selected items.</p>';
	exit = exit+'</div>';
	exit = exit+'<div id="navigation">';
	exit = exit+'	<a class="no" rev="yes" rel="yes" title="" ><span class="no">NO</span></a>';
	exit = exit+'	<a class="yes" rev="no" rel="no" title="" ><span class="yes">YES</span></a>';
	exit = exit+'</div>';
	
	var exitHTML = Array();

	exitHTML['exit'] = exit;

	return exitHTML;	
}
// CHANGE END: 20120202-0130 bsears - high priority change: dine exit confirmation panel(s)

// CHANGE BEG: 20120207-1000 bsears - dine issue via Tami: display error panel if place order submission fails
function submitErrorHTML()	{
	
	var submenulabel = $("#K_submenu").text();
	
	var error  = '';
	
	error = error+'	<div id="title">';
	error = error+'		<p>'+submenulabel+' - Welcome to Caf&#233; 1910 Room Service</p>';
	error = error+'	</div>';
	error = error+'	<div id="message">';
	error = error+'		<p class="error">We&#39;re sorry. On-screen ordering<br />is unavailable at this time.</p><br /><br />';
	error = error+'		<p class="error">Please call the Diet Clerk for assistance<br />or to place your order between<br />7am and 8pm - Extension 34222.</p>';
	error = error+'	</div>';
	error = error+'	<div id="navigation">';
	error = error+'		<a class="close" rev="close" rel="close" title="" ><span class="close">CLOSE</span></a>';
	error = error+'	</div>';
	
	var errorHTML = Array();

	errorHTML['error'] = error;

	return errorHTML;	
}
// CHANGE END: 20120207-1000 bsears - dine issue via Tami: display error panel if place order submission fails

// CHANGE BEG: 20120203-0200 bsears - high priority change: implement place order confirmation panel
function placeOrderHTML()	{
	
	var placeorder  = '';
	
	placeorder = placeorder+'<div id="title">';
	placeorder = placeorder+'	<p>Place Order</p>';
	placeorder = placeorder+'</div>';
	placeorder = placeorder+'<div id="message">';
	placeorder = placeorder+'	<p>&nbsp;</p>';
	placeorder = placeorder+'	<p>Are you sure you would like to<br />Place this Order?</p><br /><br />';
	placeorder = placeorder+'	<p>&nbsp;</p>';
	placeorder = placeorder+'	<p>Answering NO returns to your Order.</p>';
	placeorder = placeorder+'	<p>Answering YES places the Order.</p>';
	placeorder = placeorder+'</div>';
	placeorder = placeorder+'<div id="navigation">';
	placeorder = placeorder+'	<a class="no" rev="yes" rel="yes" title="" ><span class="no">NO</span></a>';
	placeorder = placeorder+'	<a class="yes" rev="no" rel="no" title="" ><span class="yes">YES</span></a>';
	placeorder = placeorder+'</div>';
	
	var placeorderHTML = Array();

	placeorderHTML['placeorder'] = placeorder;

	return placeorderHTML;	
}
// CHANGE END: 20120203-0200 bsears - high priority change: implement place order confirmation panel

function confirmorderHTML()	{
	
	var submenu = $("#K_submenu").attr("class");

	var title 	= '';
	var message = '';
	var panel 	= '';
	
	title = title+'		<div id="title"><p>View Order</p></div>';
	
	if (submenu=='mymenu')	{
		
		var patient 	 = loadJSON('patient');
		var name		 = patient.userFullName;
		if (!name)	name = 'NO NAME';		
		
		message = message+'		<div id="message">';
		message = message+'			<p>Thank you, '+name+', for your order.<br />It will be confirmed by our Diet Clerk.<br />Please call Ext. 34222 if you have any questions.<br /><span>Approximate delivery time is 45 minutes.</span></p>';
		message = message+'			<p class="bump">If you have diabetes, please let your nurse know<br />you have placed an order.</p>';
		message = message+'		</div>';
	
	}	else
		if (submenu=='visitorsmenu')	{
			
			var orderDATA  	= getXML('order');
			var recipe		= '';
			var price		= 0.00;
			var priceT		= 0.00;
			var quantity 	= 0;
			
			$(orderDATA).find('orderitem').each(function(){	
//				recipe 	= $(this).attr('id');	
//				if(recipe)	{								
					quantity 	= $(this).attr('quantity');
					price		= $(this).attr('price');
			
//					price = price * quantity;
					priceT = priceT + (price * quantity);
//				}
			});
			
			priceT = roundNumber(priceT,2);

			message = message+'		<div id="message">';
			message = message+'			<p>Thank you for your order.<br />Your total amount due upon delivery is <span>$'+priceT+'</span>.<br />Please call Ext. 34222 if you have any questions.<br />&nbsp;</p>';
			message = message+'			<p><span>Approximate delivery time is 45 minutes.</span></p>';
			message = message+'		</div>';
			
		}

	panel = panel+'		<div id="navigation" class="close">';
	panel = panel+'			<a id="selection" class="close" rev="close" rel="close" ><span class="close">CLOSE</span></a>';
	panel = panel+'		</div>';

	var confirmHTML = Array();

	confirmHTML['title']	= title;
	confirmHTML['message']	= message;	
	confirmHTML['panel']	= panel;
	confirmHTML['active']	= 'close';

	return confirmHTML;			
}

//function getMealsCount(meals)	{
//	
//	var mealstotal = 0;
//
///  loop through xml and count number of meals (breakfast, lunch, etc.
//	
//	return mealstotal;
//}

//function getMealsPN(meals)	{
//	
//	var mealsPN = Array();
//	var count	= 0;
//	var countF  = 0;
//	var countL  = 0;
//	
////  loop through xml, build array for prev/next...
//	
//	return mealsPN;
//}

//function getMealsCount(meals,mealslast,mealstotal)	{
//	
//	var mealsstart = 0;
//
////  loop through xml and determine staring position.
//
//	if(mealsstart==mealstotal)	{
//		mealsstart = 0;
//	}
//	
//	return mealsstart;
//}
