//
// navigation functions
//

function reloadapp()	{
	var version = $("#K_version").text();
	if (version=='PROD')	{
		//Nimbus.reload(true);
	}
	window.location = 'index.html';		
}

function keypressed(keyCode)	{
	
	var keys = Array();
	
	keys[13] = 'ENTER';
	keys[36] = 'HOME';
//	keys[37] = 'LEFT';
//	keys[38] = 'UP';
//	keys[39] = 'RIGHT';
//	keys[40] = 'DOWN';
	keys[33] = 'CHDN';
	keys[34] = 'CHUP';
	
//	keys[38] = 'CHUP';
//	keys[40] = 'CHDN';
	
	keys[48] = '0';
	keys[49] = '1';
	keys[50] = '2';
	keys[51] = '3';
	keys[52] = '4';
	keys[53] = '5';
	keys[54] = '6';
	keys[55] = '7';
	keys[56] = '8';
	keys[57] = '9';
	
	keys[61446] = 'ENTER';
//	keys[61444] = 'LEFT';
//	keys[61442] = 'UP';
//	keys[61445] = 'RIGHT';
//	keys[61443] = 'DOWN';
	
	keys[61447] = 'MENU';
	
	keys[61441] = 'POWR';	// power
	keys[61507] = 'POWR';	// power
	keys[61508] = 'POWR';	// power
	keys[61521] = 'CC';	// closed caption
	
	keys[61483] = 'CHUP';	// channel+
	keys[61484] = 'CHDN';	// channel-
	//keys[61449] = 'VOLU';	// volume+
	//keys[61448] = 'VOLD';	// volume-
	
//	keys[61464] = 'PLAY';	// pause
//	keys[61465] = 'STOP';	// stop
//	keys[61464] = 'PAUS';	// pause
//	keys[61467] = 'RWND';	// rewind
//	keys[61468] = 'FFWD';	// fast forward		
	
	var key = keys[keyCode];
	
	if (!key)	{
		msg('keypress not processed... Actual Value: ' + keyCode + ' Translated Value: ' + key);
		return false;
	}
	
	var panel   = $("#K_panel").attr("class");
	var version = $("#K_version").text();
	var client  = $("#K_client").text();
	
	if (key=='POWR')	{
////		var onoff = $("#K_power").text();
//		msg('in discharge panel ' + panel);
//		if (version=='PROD')	{	
//			var TVController = Nimbus.getTVController();
//			if (TVController != null) {
//				if (TVController.getPower()) {
//				//if (onoff=='ON')	{
//					if (panel=='video')	stopVideo();
//					if (panel=='tv'||panel=='scenictv'||panel=='discharged') stopTV();
////					reloadapp();	// remove menu reload
//				}
//				//var power = togglePower();
//				//reloadapp();
//			}
//		}
		return false;	
	}
		
	if (key=='CC')	{
		setCC();
		return true;	
	}	
	
	if (key=='MENU'||key=='HOME')	{

		if (panel=='discharged')	stopTV();
		$("#discharged").show();

//		window.location = 'discharged.html';
//		reloadapp();	// remove menu reload
		return true;
	}	
		
	if (panel=='discharged')	{
		if(key=='ENTER'||key=='CHUP'||key=='CHDN')	{
			gotochannel(key);
			return true;
		}
		
		if (key=='0'||key=='1'||key=='2'||key=='3'||key=='4'||key=='5'||key=='6'||key=='7'||key=='8'||key=='9')	{
			$("#discharged").hide();
			keyChannel(key);
			return true;
		}			
	}
		
	msg('keypress not caught/processed... Actual Value: ' + keyCode + ' Translated Value: ' + key);
	return false;
}

function welcome()	{
	
	$("#K_panel").addClass('discharged');
	
	var channelsDATA = getChannelsDATA();
	
	var roomDATA = getRoomDATA();
	var welcome  = 'Welcome to Level '+roomDATA.roomLevel+': '+roomDATA.roomWing+' Room '+roomDATA.roomNumber;
		
	$("#discharged p.discharged").html(welcome);
	
	$("#K_versionoverlay").text('');
	var pathname = window.location;
	if (pathname=='http://10.209.120.28:9090/ewf_test_test/discharged.html')	$("#K_versionoverlay").text('TEST');
	if (pathname=='http://10.209.120.28:9090/ewf_test/discharged.html')			$("#K_versionoverlay").text('TEST');
	
	var versionoverlay = $("#K_versionoverlay").text();
	if (versionoverlay)	{
		$("#testoverlay p").text(versionoverlay);
		$("#testoverlay").show();
	}	
	
	setInterval( "getRoomDATA()", 60000 );	
	
	return;
}

function gotochannel(key)	{
	
	$("#discharged").hide();
	
	var nowplaying  = $("#VIDEO_nowplaying").text();
	if (nowplaying&&nowplaying>' ')	{
		updnChannel(key); 
		return;
	}
	
	var channels 	 = epgChannels();
	var channel		 = '';

	$.each(channels['Channels'], function(i,row){	
		if (channel=='') channel=row["channelID"];
	});	
	
	var url = playTV(channel);

	return;
}

