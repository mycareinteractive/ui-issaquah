//
// JSON call functions
//
 
function getUserDataDATA()	{
	
	var client  = $("#K_client").text();
	var version = $("#K_version").text();
	if (version=='DEMO')	{
		var userdataDATA = Array();
		userdataDATA['patientName'] = 'Joseph Smith';
		userdataDATA['roomWing'] 	= 'Cascade';
		userdataDATA['roomLevel'] 	= '2';
		userdataDATA['roomNumber'] 	= '2105';
		userdataDATA['phoneNumber'] = '206-386-0124';
		return userdataDATA;		
	}	
		 
	var ewf 	= ewfObject();
	var mac		= getDevice();
	
	var arg1	= 'attribute=json_libs_oss_get_user_data';
	var arg2	= 'device_id='+mac;
	
	var url		= ewf.host+ewf.method;
	var args    = arg1+'&'+arg2;
	//$("#CALL_userdata").html(url+'?'+args);
	
	var userdata = ajaxJSON(url,args);

	var userdataString = JSON.stringify(userdata);
	//$("#EWF_userdata").html(userdataString);
	
	var comma = '';
	var room  = '';
	var level = '';
	var wing  = '';
	var bed   = '';

	patientString = '{';
	deviceString  = '{';
	
	$.each(userdata, function(index, DataArea) {									
		$.each(DataArea, function(index, Home) {								
			$.each(Home.tagAttribute, function(tagName, tagAttribute) {	
				if (tagName=='status'&&tagAttribute=='0')							   
					discharged();
				if (tagName=='homeID')	{
					if (client=='ACESO')	{
						room  = tagAttribute.substr(3,3);
						level = tagAttribute.substr(3,1);
						bed   = tagAttribute.substr(7,1);
						if(room>='300'&&room<='399')	{ wing = 'Cascade'; }
					}	else
					if (client=='SWEDISH')	{
						// -- swedish only --
						room  = tagAttribute.substr(3,4);
						level = tagAttribute.substr(3,1);
						bed   = tagAttribute.substr(8,1);
						if(room>='2104'&&room<='2108')	{ wing = 'Cascade North'; }
						if(room>='2112'&&room<='2293')	{ wing = 'Cascade South'; }
						if(room>='2301'&&room<='2422')	{ wing = 'Olympic'; }
						if(room>='3103'&&room<='3211')	{ wing = 'Cascade'; }
						if(room>='3301'&&room<='3422')	{ wing = 'Olympic'; }
						if(room>='4101'&&room<='4223')	{ wing = 'Cascade'; }					
						// -- swedish only --
					}
					patientString = patientString+comma+' "roomWing":"'+wing+'"';
					comma = ',';
					patientString = patientString+comma+' "roomLevel":"'+level+'"';
					patientString = patientString+comma+' "roomNumber":"'+room+'"';	
					patientString = patientString+comma+' "roomBed":"'+bed+'"';
				}
				patientString = patientString+comma+' "'+tagName+'":"'+tagAttribute+'"';
				comma = ',';
			});	
			$.each(Home.ListOfSTBUser, function(index, STBUser) {								
				$.each(STBUser.tagAttribute, function(tagName, tagAttribute) {		
					patientString = patientString+comma+' "'+tagName+'":"'+tagAttribute+'"';
				});	
			});			
			comma = '';
			$.each(Home.ListOfDevice, function(index, Device) {								
				$.each(Device.tagAttribute, function(tagName, tagAttribute) {		
					deviceString = deviceString+comma+' "'+tagName+'":"'+tagAttribute+'"';
					comma = ',';
				});	
			});				
		
		});	
	});	
	
	patientString = patientString+' }';
	$("#JSON_patient").html(patientString);
	deviceString = deviceString+' }';
	$("#JSON_device").html(deviceString);
	
	var patientDATA = JSON.parse(patientString);
	
	return patientDATA;
}

function checkDischarge()	{
	
	var version = $("#K_version").text();
	if (version=='DEMO')	
		return;		
		 
	var ewf 	= ewfObject();
	var mac		= getDevice();
	
	var arg1	= 'attribute=json_libs_oss_get_user_data';
	var arg2	= 'device_id='+mac;
	
	var url		= ewf.host+ewf.method;
	var args    = arg1+'&'+arg2;
	
	var userdata = ajaxJSON(url,args);
		
	$.each(userdata, function(index, DataArea) {									
		$.each(DataArea, function(index, Home) {								
			$.each(Home.tagAttribute, function(tagName, tagAttribute) {	
				if (tagName=='status'&&tagAttribute=='0')							   
					discharged();
			});	
			$.each(Home.ListOfSTBUser, function(index, STBUser) {								
				$.each(STBUser.tagAttribute, function(tagName, tagAttribute) {		
				});	
			});			
			$.each(Home.ListOfDevice, function(index, Device) {								
				$.each(Device.tagAttribute, function(tagName, tagAttribute) {		
				});	
			});				
		
		});	
	});	
	
	return 'active';
}

function getRoomDATA()	{

	//msg('arrived getRoomDATA');
	
	var client  = $("#K_client").text();
	var version = $("#K_version").text();
		 
	var ewf 	= ewfObject();
	var mac		= getDevice();
	
	var arg1	= 'attribute=json_libs_oss_get_user_data';
	var arg2	= 'device_id='+mac;
	
	var url		= ewf.host+ewf.method;
	var args    = arg1+'&'+arg2;
	
	var userdata = ajaxJSON(url,args);

	var userdataString = JSON.stringify(userdata);
	
	//msg('json result: '+userdataString);
	
	var comma = '';
	var room  = '';
	var level = '';
	var wing  = '';

	var roomString  = '{';
	
	
 //   { DataArea: [ 
 //        
 //{ tagName: "Home", tagAttribute: {
//  homeID:"1002314_p",siteID:"100",status:"0",type:"0",portalID:"test",phoneNumber:"425-837-4274"}
//  
// }
// 
 //      ] }	
	
	$.each(userdata.DataArea[0].tagAttribute, function(tagName, tagAttribute) {	
		//msg(tagName+': '+tagAttribute);
		if (tagName=='status'&&tagAttribute=='1')							   
			reloadapp();
		if (tagName=='homeID')	{
//			room  = tagAttribute.substr(3,4);
//			level = tagAttribute.substr(3,1);
//			if (room>='2104'&&room<='2108')	{ wing = 'Cascade North'; }
//			if (room>='2112'&&room<='2293')	{ wing = 'Cascade South'; }
//			if (room>='2301'&&room<='2422')	{ wing = 'Olympic'; }
//			if (room>='3103'&&room<='3211')	{ wing = 'Cascade'; }
//			if (room>='3301'&&room<='3422')	{ wing = 'Olympic'; }
//			if (room>='4101'&&room<='4223')	{ wing = 'Cascade'; }
	
			if (client=='ACESO')	{
				room  = tagAttribute.substr(3,3);
				level = tagAttribute.substr(3,1);
				if(room>='300'&&room<='399')	{ wing = 'Cascade'; }
			}	else
			if (client=='SWEDISH')	{
				// -- swedish only --
				room  = tagAttribute.substr(3,4);
				level = tagAttribute.substr(3,1);
				if(room>='2104'&&room<='2108')	{ wing = 'Cascade North'; }
				if(room>='2112'&&room<='2293')	{ wing = 'Cascade South'; }
				if(room>='2301'&&room<='2422')	{ wing = 'Olympic'; }
				if(room>='3103'&&room<='3211')	{ wing = 'Cascade'; }
				if(room>='3301'&&room<='3422')	{ wing = 'Olympic'; }
				if(room>='4101'&&room<='4223')	{ wing = 'Cascade'; }					
				// -- swedish only --
			}
			roomString = roomString+comma+' "roomWing":"'+wing+'"';
			comma = ',';
			roomString = roomString+comma+' "roomLevel":"'+level+'"';
			roomString = roomString+comma+' "roomNumber":"'+room+'"';	
		}													 							 
		roomString = roomString+comma+' "'+tagName+'":"'+tagAttribute+'"';
		comma = ',';
	});	
	
	roomString = roomString+' }';
	//msg('device object: '+roomString);
	//$("#JSON_device").html(roomString);
	
	var roomDATA = JSON.parse(roomString);
	
	return roomDATA;
}

//function getRoomDATA()	{
//		 
//	var ewf 	= ewfObject();
//	var patient = loadJSON('patient');
//	
//	var arg1	= 'attribute=json_libs_oss_check_home_status';
//	var arg2	= 'home_id='+patient.homeID;
//	
//	var url		= ewf.host+ewf.method;
//	var args    = arg1+'&'+arg2;
//	//$("#CALL_homestatus").html(url+'?'+args);
//	
//	var homestatus = ajaxJSON(url,args);
//
//	var homestatusString = JSON.stringify(homestatus);
//	//$("#EWF_homestatus").html(homestatusString);
//	
//	var comma		= '';
//
//	var homestatusString 	= '{';
//						 
//	$.each(homestatus.DataArea.tagAttribute, function(tagName, tagAttribute) {	
//		if (tagName=='homeID')	{
//			room  = tagAttribute.substr(3,4);
//			level = tagAttribute.substr(3,1);
//			if (room>='2104'&&room<='2108')	{ wing = 'Cascade North'; }
//			if (room>='2112'&&room<='2293')	{ wing = 'Cascade South'; }
//			if (room>='2301'&&room<='2422')	{ wing = 'Olympic'; }
//			if (room>='3103'&&room<='3211')	{ wing = 'Cascade'; }
//			if (room>='3301'&&room<='3422')	{ wing = 'Olympic'; }
//			if (room>='4101'&&room<='4223')	{ wing = 'Cascade'; }
//			homestatusString = homestatusString+comma+' "roomWing":"'+wing+'"';
//			comma = ',';
//			homestatusString = homestatusString+comma+' "roomLevel":"'+level+'"';
//			homestatusString = homestatusString+comma+' "roomNumber":"'+room+'"';						
//		}	
//		homestatusString = homestatusString+comma+' "'+tagName+'":"'+tagAttribute+'"';
//		comma = ',';
//	});		
//	homestatusString = homestatusString+' }';
//	//$("#JSON_homestatus").html(homestatusString);
//		
//	var homestatusDATA = JSON.parse(homestatusString);
//	
//	return homestatusDATA;	
//	
//}

function getHomeStatusDATA()	{
		 
	var ewf 	= ewfObject();
	var patient = loadJSON('patient');
	
	var arg1	= 'attribute=json_libs_oss_check_home_status';
	var arg2	= 'home_id='+patient.homeID;
	
	var url		= ewf.host+ewf.method;
	var args    = arg1+'&'+arg2;
	//$("#CALL_homestatus").html(url+'?'+args);
	
	var homestatus = ajaxJSON(url,args);

	var homestatusString = JSON.stringify(homestatus);
	$("#EWF_homestatus").html(homestatusString);
	
	var comma		= '';

	var homestatusString 	= '{';
						 
	$.each(homestatus.DataArea.tagAttribute, function(tagName, tagAttribute) {								  
			homestatusString = homestatusString+comma+' "'+tagName+'":"'+tagAttribute+'"';
			comma = ',';
	});		
	homestatusString = homestatusString+' }';
	$("#JSON_homestatus").html(homestatusString);
		
	var homestatusDATA = JSON.parse(homestatusString);
	
	return homestatusDATA;	
	
}

function getProductOfferingDATA()	{
		 
	var ewf 	 = ewfObject();
	var patient = loadJSON('patient');
	
	var arg1	= 'attribute=json_libs_stb_list_entry';
	var arg2	= 'parent_HUID='+ewf.VODRootHUID;
	var arg3	= 'depth=-1';
	var arg4	= 'home_id='+patient.homeID;
	
	var url		= ewf.host+ewf.method;
	var args    = arg1+'&'+arg2+'&'+arg3+'&'+arg4;
	//$("#CALL_productoffering").html(url+'?'+args);
	
	var productoffering = ajaxJSON(url,args);

	var productofferingString = JSON.stringify(productoffering);
	//$("#EWF_productoffering").html(productofferingString);
	
	productofferingString = '{ "ProductOffering": [  ';
	
	var comma  = '';
	var comma2 = '';
	
	$.each(productoffering, function(index, DataArea) {  
		$.each(DataArea.ListOfEntry, function(index, ProductOffering) {								  
			productofferingString = productofferingString+comma2+' {';
			comma2 = ',';
			comma  = '';
			$.each(ProductOffering.tagAttribute, function(tagName, tagAttribute) {										  
				tagAttribute = cleanJSON(tagAttribute);			
				productofferingString = productofferingString+comma+' "'+tagName+'":"'+tagAttribute+'"';
				comma = ',';
			});
			productofferingString = productofferingString+' }';
		});			
	});	
	
	productofferingString = productofferingString+' ] } ';
	$("#JSON_productoffering").html(productofferingString);
		
	var productofferingDATA = JSON.parse(productofferingString);
	
	return productofferingDATA;	
	
}

// don't think checkBookmark is used....

function checkBookmark(video)	{
	
	var bookmarksDATA 	= getBookmarksDATA();
	var bookmarks 		= loadJSON('bookmarks');
	
	var comma = '';
	var bookmarkString = '{ ';
	
	// loop through and copy existing bookmark data if available...
		bookmarkString = bookmarkString+'"bookmarkFound": ""';
	
	bookmarkString = bookmarkString+' }';
	$("#JSON_bookmark").html(bookmarkString);
		
	var bookmarkDATA = JSON.parse(bookmarkString);
	
	return bookmarkDATA;		
}

//
// My Programs
//

function getMyProgramsDATA()	{
	
	var ewf 	= ewfObject();
	var patient = loadJSON('patient');
	
	var arg1	= 'attribute=json_libs_ote_list_ticket';
	var arg2	= 'product_type=0';
	var arg3	= 'home_id='+patient.homeID;
	
	var url		= ewf.host+ewf.method;
	var args    = arg1+'&'+arg2+'&'+arg3;
	//$("#CALL_myprograms").html(url+'?'+args);
	
	var bookmarks = ajaxJSON(url,args);

	var bookmarksString = JSON.stringify(bookmarks);
	//$("#EWF_myprograms").html(bookmarksString);
	
	var tags = Array();
	tags['ticketID'] 			= 'ticketID';
	tags['homeID'] 				= 'homeID';
	tags['deviceID'] 			= 'deviceID';
	tags['assetID'] 			= 'assetID';
	tags['localEntryUID'] 		= 'tag';
	tags['productOfferingID'] 	= 'poUID';
	tags['Title'] 				= 'title';
	tags['TitleBrief'] 			= 'label';
	tags['YearOfRelease'] 		= 'year';
	tags['Length'] 				= 'duration';		// not available
	tags['Rating'] 				= 'rating';	
	tags['Description'] 		= 'description';
	tags['LongDescription'] 	= 'synopsis';
	tags['PosterBoard'] 		= 'poster';
	tags['suspendPosition'] 	= 'position';
	
	// build myprograms
	
	myprogramsString = '{ ';
	myprogramsString = myprogramsString+'"type":"menu", ';
	myprogramsString = myprogramsString+'"background":"myprograms", ';
	myprogramsString = myprogramsString+'"comments":"", ';
	myprogramsString = myprogramsString+'"myprograms": [  ';
															
	var count  		= 0;
	var comma  		= '';
	var comma2 		= '';
	var titlelength = 0;
	var descrlen 	= 0;
	var descrpos	= 0;
	var descrval  	= '';
	
	$.each(bookmarks, function(index, DataArea) {
		$.each(DataArea, function(index, ListOfTicket) {			  
			$.each(ListOfTicket, function(index1, Ticket) {
				count++;
				myprogramsString = myprogramsString+comma2+' { ';
				comma2 = ',';
				comma  = '';
				$.each(Ticket.tagAttribute, function(tagName, tagAttribute) {	
					if (tagName=='ticketID'||tagName=='homeID'||tagName=='deviceID'||tagName=='assetID'||tagName=='localEntryUID'||tagName=='suspendPosition')	{
						tagAttribute = cleanJSON(tagAttribute);	
						myprogramsString = myprogramsString+comma+' "'+tags[tagName]+'":"'+tagAttribute+'"';
						comma = ',';
					};
				});	
				$.each(ListOfTicket[index1].ListOfSubEntry, function(index2, SubEntry) {												 
					$.each(SubEntry.tagAttribute, function(tagName, tagAttribute) {	
						if (tagName=='productOfferingID'||tagName=='suspendPosition')	{
							tagAttribute = cleanJSON(tagAttribute);							
							myprogramsString = myprogramsString+comma+' "'+tags[tagName]+'":"'+tagAttribute+'"';
							myprogramsString = myprogramsString+comma+' "image":""';
							myprogramsString = myprogramsString+' }';
						};							
					});	
					if (ListOfTicket[index1].ListOfSubEntry[index2].ListOfMetaData)	{
						$.each(ListOfTicket[index1].ListOfSubEntry[index2].ListOfMetaData, function(tagName, tagAttribute) {
							if (tagName=='Title'||tagName=='TitleBrief'||tagName=='YearOfRelease'||tagName=='Rating'||tagName=='Description'||tagName=='LongDescription'||tagName=='PosterBoard'||tagName=='suspendPosition')	{
								tagAttribute = cleanJSON(tagAttribute);	
//								if(tagName=='Title')	{
//									titlelength = tagAttribute.length;
//									if(titlelength>30) tagAttribute = tagAttribute.substr(0,29);
//								}		
//								if(tagName=='Description')	{
//									descrlen = tagAttribute.length;
//									descrpos = descrlen - 6;
//									descrval = tagAttribute.substr(descrpos,6);
//									if(descrval==' title') tagAttribute = tagAttribute.substr(0,descrpos);
//								}									
								myprogramsString = myprogramsString+comma+' "'+tags[tagName]+'":"'+tagAttribute+'"';
							};
						});
					}
				});
			});
		});
	});
	
	if(count==0)
		myprogramsString = myprogramsString+comma2+'{ "tag":"notfound", "label":"no bookmarked videos", "image":"", "duration":"10" } ';
	myprogramsString = myprogramsString+' ] }';
	$("#JSON_bookmarks").html(myprogramsString);		// contains all bookmarks...healthcare and movies
	
	//
	// filter out the movies
	//
	
	var myprogramsObject = JSON.parse(myprogramsString);

	comma='';
	var atleastone = 'N';
//	var assetID	   = '';
//	var asset	   = '';
//	var duration   = '';
	var position   = '';
	
	var filteredString = '{ "type":"menu", "background":"myprograms", "comments":"", "myprograms": [';
	$.each(myprogramsObject['myprograms'], function(i,row){											
		if (row["poUID"]=='5')	{
//			assetID = row["tag"];
//			asset = getAsset(assetID);
//			duration = ', "duration":"'+asset['duration']+'"';	
			position = '';
			if (row["position"])	{
				position = ', "position":"'+row["position"]+'"';	
			}
						filteredString = filteredString+comma+' { "ticketID":"'+row["ticketID"]+'", "homeID":"'+row["homeID"]+'", "deviceID":"'+row["deviceID"]+'", "assetID":"'+row["assetID"]+'", "tag":"'+row["tag"]+'", "label":"'+row["label"]+'", "year":"'+row["year"]+'", "rating":"'+row["rating"]+'", "synopsis":"'+row["synopsis"]+'", "description":"'+row["description"]+'", "poUID":"'+row["poUID"]+'", "image":"'+row["image"]+'"'+position+'}';
			comma = ',';
			atleastone = 'Y';
		}
	});	
	if (atleastone=='N')	{
		filteredString = filteredString+comma+' { "ticketID":"", "homeID":"", "deviceID":"", "assetID":"", "tag":"EMPTY1", "label":"no bookmarks", "year":"", "rating":"", "synopsis":"", "poUID":"", "image":"" }';		
	}
	filteredString = filteredString+' ] }';
	
	$("#JSON_myprograms").html(filteredString);		// contains just healthcare bookmarks
	//
	// build individual programs
	//
	var myprogramsObject = JSON.parse(filteredString);
	
	var videoString = '';
	var checkexists = '';
	comma='';
	
	$.each(myprogramsObject['myprograms'], function(i,row){									
		videoString = ' { ';								 
		videoString = videoString+'"type":"video", "source":"", "tag":"'+row["tag"]+'", "title":"'+row["label"]+'", "poUID":"'+row["poUID"]+'" ';
		videoString = videoString+' }';
		
		checkexists = $("#BOOKMARK_"+row["tag"]).text();
		if (!checkexists)
			$("#json").append('<div id="BOOKMARK_'+row["tag"]+'"></div>');
		$("#BOOKMARK_"+row["tag"]).html(videoString);
		comma = ',';
	});										 
	
	return 'success';	
}

function myprogramsCategories()	{
	
	var myprogramsString = $("#JSON_myprograms").text();
	var myprograms = JSON.parse(myprogramsString);
	
	return myprograms;
}

function myprogramsSELECTION(choice)	{
	
	var selectionString = $("#BOOKMARK_"+choice).text();
	var selection = JSON.parse(selectionString);
	
	return selection;
}

//
// allprograms BEGIN
//

function getAllProgramsDATA()	{
		
	var ewf 			= ewfObject();
	var patient 		= loadJSON('patient');
	
	var arg1	= 'attribute=json_libs_stb_list_entry';
	var arg2	= 'parent_HUID='+ewf.allprograms;
	var arg3	= 'depth=-1';
	var arg4	= 'home_id='+patient.homeID;
	
	var url		= ewf.host+ewf.method;
	var args    = arg1+'&'+arg2+'&'+arg3+'&'+arg4;	
	//$("#CALL_allprograms").html(url+'?'+args);
	
	var allprograms = ajaxJSON(url,args);

	var allprogramsString = JSON.stringify(allprograms);
	//$("#EWF_allprograms").html(allprogramsString);
	
	var tags = Array();
	tags['localEntryUID'] 		= 'tag';
	tags['entryName'] 			= 'label';
	tags['productOfferingUID'] 	= 'poUID';
	
	// build allprograms group
	
	allprogramsString = '{ ';
	allprogramsString = allprogramsString+' "type":"menu", ';
	allprogramsString = allprogramsString+' "background":"allprograms", ';
	allprogramsString = allprogramsString+' "comments":"", ';
	allprogramsString = allprogramsString+'"allprograms": [  ';											  
	
	var comma  = '';
	var comma2 = '';
	var atleastone = 'N';
	
	$.each(allprograms, function(index, DataArea) {  
		if(DataArea.ListOfEntry)	{
			$.each(DataArea.ListOfEntry, function(index, Folder) {	
				allprogramsString = allprogramsString+comma2+' {';
				comma2 = ',';
				comma  = '';
				if(Folder.tagAttribute)	{
					$.each(Folder.tagAttribute, function(tagName, tagAttribute) {	
						if (tagName=='localEntryUID'||tagName=='entryName'||tagName=='productOfferingUID')	{
							tagAttribute = cleanJSON(tagAttribute);	
							allprogramsString = allprogramsString+comma+' "'+tags[tagName]+'":"'+tagAttribute+'"';
							comma = ',';
							atleastone = 'Y';
						}
					});
				}
				allprogramsString = allprogramsString+comma+' "image":""';
				allprogramsString = allprogramsString+' }';
			});
		}
	});	
	if (atleastone=='N')	{
		allprogramsString = allprogramsString+comma+'{ "tag":"EMPTY2", "label":"no programs", "poUID":"", "image":"" }';
	}
	allprogramsString = allprogramsString+' ] } ';
	$("#JSON_allprograms").html(allprogramsString);
	
	var allprogramsDATA = buildAllProgramsDATA();
	
	
	//
	// build individual programs
	//
	
	var panelString = '';
	var checkexists = '';
	
	$.each(allprograms, function(index, DataArea) {  
		$.each(DataArea.ListOfEntry, function(index, Folder) {								  
			$.each(Folder.tagAttribute, function(tagName, tagAttribute) {	
				if (tagName=='localEntryUID')	{																	
					tagAttribute = cleanJSON(tagAttribute);	
					panelString = '{ "type":"panel", "panel":"'+tagAttribute+'" }';
					
					checkexists = $("#SELECTION_"+tagAttribute).text();
					if (!checkexists)
						$("#json").append('<div id="SELECTION_'+tagAttribute+'"></div>');
					$("#SELECTION_"+tagAttribute).html(panelString);					
				}
			});
		});			
	});	
	
	return 'success';	
}

function buildAllProgramsDATA()	{
	
	var allprogramsString = $("#JSON_allprograms").text();
	var allprograms = JSON.parse(allprogramsString);
		
	var program = '';
	var checkexists   = '';
	
	$.each(allprograms, function(tagName, tagAttribute) { 
		if (tagName=='allprograms')	{
			$.each(tagAttribute, function(index, allprograms) {
				$.each(allprograms, function(tagName, tagAttribute) {						  
					if (tagName=='tag')	{
						program = getProgramDATA(tagAttribute);
						checkexists = $("#PANEL_"+tagAttribute).text();
						if (!checkexists)
							$("#json").append('<div id="PANEL_'+tagAttribute+'"></div>');
						$("#PANEL_"+tagAttribute).html(program);						
					}
				});
			});
		}
	});		
	
	return;
}

function getProgramDATA(theprogram)	{
		
	if (theprogram=='EMPTY2')	{
		var empty = '{ "EMPTY2": [   { "hUID":"EMPTY2", "tag":"EMPTY2", "poUID":"", "label":"no videos", "duration":"", "rating":"", "synopsis":"" } ] }';
		return empty;
	}
	
	var ewf 	= ewfObject();
	var patient = loadJSON('patient');
	
	var arg1	= 'attribute=json_libs_stb_list_entry';
	var arg2	= 'parent_HUID='+theprogram;
	var arg3	= 'depth=-1';
	var arg4	= 'query_mode=1';
	var arg5	= 'home_id='+patient.homeID;
	
	var url		= ewf.host+ewf.method;
	var args    = arg1+'&'+arg2+'&'+arg3+'&'+arg4+'&'+arg5;	
	//$("#CALL_program").html(url+'?'+args);
	
	var program = ajaxJSON(url,args);

	var programString = JSON.stringify(program);
	//$("#EWF_program").html(programString);
	
	var tags = Array();
	tags['hierarchyUID'] 		= 'hUID';
	tags['localEntryUID'] 		= 'tag';
	tags['productOfferingUID'] 	= 'poUID';
	tags['Title'] 				= 'title';
	tags['TitleBrief'] 			= 'label';
	tags['Length'] 				= 'duration';
	tags['Rating'] 				= 'rating';
	tags['Description'] 		= 'description';
	tags['LongDescription'] 	= 'synopsis';
	tags['PosterBoard'] 		= 'poster';
	
	//
	// build movie group
	//
	
	programString = '{ ';
	programString = programString+'"'+theprogram+'": [  ';	
													  										  
	var comma  		= '';
	var comma2 		= '';
	var titlelength = 0;
	var descrlen 	= 0;
	var descrpos	= 0;
	var descrval  	= '';
	
	$.each(program, function(index, DataArea) { 
		$.each(DataArea, function(index, ListOfEntry) {		  
			$.each(ListOfEntry, function(index, Asset) {
				if	(index>=0&&index<=999)	{						 					 
					programString = programString+comma2+' {';
					comma  = '';
					comma2 = ',';
					$.each(Asset.tagAttribute, function(tagName, tagAttribute) {
						if(tagName=='hierarchyUID'||tagName=='localEntryUID'||tagName=='productOfferingUID')	{
							tagAttribute = cleanJSON(tagAttribute);			
							programString = programString+comma+' "'+tags[tagName]+'":"'+tagAttribute+'"';
							comma = ',';
							if (tagName=='localEntryUID')	{
								var bookmark = checkBOOKMARK(tagAttribute);
								if (bookmark==tagAttribute)
									programString = programString+comma+' "bookmark":"Y"';	
							}
						}
					});	
					$.each(Asset.ListOfMetaData, function(tagName, tagAttribute) {	
						if(tagName=='Title'||tagName=='TitleBrief'||tagName=='Length'||tagName=='Rating'||tagName=='Description'||tagName=='LongDescription'||tagName=='PosterBoard')	{								  
							tagAttribute = cleanJSON(tagAttribute);	
//							if(tagName=='Title')	{
//								titlelength = tagAttribute.length;
//								if(titlelength>30) tagAttribute = tagAttribute.substr(0,29);
//							}		
//							if(tagName=='Description')	{
//								descrlen = tagAttribute.length;
//								descrpos = descrlen - 6;
//								descrval = tagAttribute.substr(descrpos,6);
//								if(descrval==' title') tagAttribute = tagAttribute.substr(0,descrpos);
//							}								
							if(tagName=='PosterBoard')	{
								tagAttribute = parsePoster(tagAttribute);
							}
							programString = programString+comma+' "'+tags[tagName]+'":"'+tagAttribute+'"';
							comma = ',';
						}
					});				
					programString = programString+' }';
					
				}
			});	
		});
	});
	
	programString = programString+' ] } ';
	$("#JSON_program").html(programString);

	//
	// build individual videos
	//
	var programObject = JSON.parse(programString);
	
	var videoString = '';
	var checkexists = '';
	comma='';
		
	$.each(programObject[theprogram], function(i,row){	
		videoString = '{ "type":"video", "hUID":"'+row["hUID"]+'", "tag":"'+row["tag"]+'", "poUID":"'+row["poUID"]+'", "title":"'+row["label"]+'", "duration":"'+row["duration"]+'", "rating":"'+row["rating"]+'", "synopsis":"'+row["synopsis"]+'", "description":"'+row["description"]+'", "poster":"'+row["poster"]+'" }';
		
		checkexists = $("#SELECTION_"+row["tag"]).text();
		if (!checkexists)
			$("#json").append('<div id="SELECTION_'+row["tag"]+'"></div>');
		$("#SELECTION_"+row["tag"]).html(videoString);					
	});	
	
	return programString;
}

//function allprogramsOriginal()	{
	
//	var allprogramsString = $("#EWF_allprograms").text();
//	var allprograms = JSON.parse(allprogramsString);
	
//	return allprograms;
//}

function allprogramsCategories()	{
	
	var allprogramsString = $("#JSON_allprograms").text();
	var allprograms = JSON.parse(allprogramsString);
	
	return allprograms;
}

function allprogramsPANEL(panel)	{
	
	var panelString = $("#PANEL_"+panel).text();
 
	var panel = JSON.parse(panelString);
	
	return panel;
}

function allprogramsSELECTION(choice)	{
	
	var selectionString = $("#SELECTION_"+choice).text();

	var selection = JSON.parse(selectionString);
	
	return selection;
}

function checkBOOKMARK(selection)	{
	
	var bookmarks = myprogramsCategories();
	var bookmark    = '';
	
	$.each(bookmarks['myprograms'], function(i,row){									
		if (selection==row["tag"])	{
			bookmark = row["tag"];
		}
	});
	
	return bookmark;
}


// allprograms END

function getMovieCategoryDATA()	{
		
	var ewf 			= ewfObject();
	var patient 		= loadJSON('patient');
	var productoffering = loadJSON('productoffering');
	
	var arg1	= 'attribute=json_libs_stb_list_entry';
	var arg2	= 'parent_HUID='+ewf.movies;
	var arg3	= 'depth=-1';
	var arg4	= 'home_id='+patient.homeID;
	
	var url		= ewf.host+ewf.method;
	var args    = arg1+'&'+arg2+'&'+arg3+'&'+arg4;	
	//$("#CALL_moviecategory").html(url+'?'+args);
	
	var moviecategory = ajaxJSON(url,args);

	var moviecategoryString = JSON.stringify(moviecategory);
	//$("#EWF_moviecategory").html(moviecategoryString);
	
	var tags = Array();
	tags['localEntryUID'] 		= 'tag';
	tags['entryName'] 			= 'label';
	tags['productOfferingUID'] 	= 'poUID';
	
	// build movie categories group
	
	moviecategoryString = '{ ';
	moviecategoryString = moviecategoryString+' "type":"menu", ';
	moviecategoryString = moviecategoryString+' "background":"movies", ';
	moviecategoryString = moviecategoryString+' "comments":"", ';
	moviecategoryString = moviecategoryString+'"movies": [  ';											  
	
	var comma  = '';
	var comma2 = '';
	var atleastone  = 'N';
	var checkexists = '';
	
	$.each(moviecategory, function(index, DataArea) {  
		if(DataArea.ListOfEntry)	{						   
			$.each(DataArea.ListOfEntry, function(index, Folder) {								  
				moviecategoryString = moviecategoryString+comma2+' {';
				comma2 = ',';
				comma  = '';
				if(Folder.tagAttribute)	{
					$.each(Folder.tagAttribute, function(tagName, tagAttribute) {	
						if (tagName=='localEntryUID'||tagName=='entryName'||tagName=='productOfferingUID')	{																	
							tagAttribute = cleanJSON(tagAttribute);	
							moviecategoryString = moviecategoryString+comma+' "'+tags[tagName]+'":"'+tagAttribute+'"';
							comma = ',';				
						}
					});
				}
				moviecategoryString = moviecategoryString+comma+' "image":""';
				moviecategoryString = moviecategoryString+' }';
				atleastone = 'Y';
			});
		}
	});	
	if (atleastone=='N')	{
		moviecategoryString = moviecategoryString+comma+'{ "tag":"EMPTY3", "label":"no categories", "poUID":"", "image":"" }';
	}
	moviecategoryString = moviecategoryString+' ] } ';
	$("#JSON_moviecategory").html(moviecategoryString);
	
	if (atleastone=='N')	{
		var moviesString = '{ "EMPTY3": [ { "hUID":"EMPTY3", "tag":"EMPTY3", "poUID":"", "label":"no categories", "duration":"", "rating":"", "synopsis":"", "poster":"" } ] }'; 
		checkexists = $("#PANEL_EMPTY3").text();
		if (!checkexists)
			$("#json").append('<div id="PANEL_EMPTY3"></div>');
		$("#PANEL_EMPTY3").html(moviesString);				
	}	else		
			var moviesDATA = buildMoviesDATA(moviecategoryString);
	
	//
	// build individual categories
	//
	
	var moviecarouselString = '';
	checkexists = '';
	
	$.each(moviecategory, function(index, DataArea) {  
		$.each(DataArea.ListOfEntry, function(index, Folder) {								  
			$.each(Folder.tagAttribute, function(tagName, tagAttribute) {	
				if (tagName=='localEntryUID')	{																	
					tagAttribute = cleanJSON(tagAttribute);	
					moviecarouselString = '{ "type":"carousel", "panel":"'+tagAttribute+'" }';
					checkexists = $("#SELECTION_"+tagAttribute).text();
					if (!checkexists)
						$("#json").append('<div id="SELECTION_'+tagAttribute+'"></div>');
					$("#SELECTION_"+tagAttribute).html(moviecarouselString);						
				}
			});
		});			
	});	
	if (atleastone=='N')	{
		moviecarouselString = '{ "type":"carousel", "panel":"EMPTY3" }';
		checkexists = $("#SELECTION_EMPTY3").text();
		if (!checkexists)
			$("#json").append('<div id="SELECTION_EMPTY3"></div>');
		$("#SELECTION_EMPTY3").html(moviecarouselString);			
	}
	
	return 'success';	
}

function buildMoviesDATA(moviecategoryString)	{
	
	var moviecategory = JSON.parse(moviecategoryString);
	
	var checkexists = '';
	
	$.each(moviecategory, function(tagName, tagAttribute) { 
		if (tagName=='movies')	{
			$.each(tagAttribute, function(index, movies) {
				$.each(movies, function(tagName, tagAttribute) {						  
					if (tagName=='tag')	{
						if (tagAttribute!='EMPTY3')	{
							var moviesString = getMoviesDATA(tagAttribute);							
							checkexists = $("#PANEL_"+tagAttribute).text();
							if (!checkexists)
								$("#json").append('<div id="PANEL_'+tagAttribute+'"></div>');
							$("#PANEL_"+tagAttribute).html(moviesString);	
						}
					}
				});
			});
		}
	});		
		
	return 'success';
}

function getMoviesDATA(category)	{
		
	var ewf 			= ewfObject();
	var patient 		= loadJSON('patient');
	
	var arg1	= 'attribute=json_libs_stb_list_entry';
	var arg2	= 'parent_HUID='+category;
	var arg3	= 'depth=-1';
	var arg4	= 'query_mode=1';
	var arg5	= 'home_id='+patient.homeID;
	
	var url		= ewf.host+ewf.method;
	var args    = arg1+'&'+arg2+'&'+arg3+'&'+arg4+'&'+arg5;	
	//$("#CALL_movies").html(url+'?'+args);
	
	var movies = ajaxJSON(url,args);

	var moviesString = JSON.stringify(movies);
	//$("#EWF_movies").html(moviesString);
	
	var tags = Array();
	tags['hierarchyUID'] 		= 'hUID';
	tags['localEntryUID'] 		= 'tag';
	tags['productOfferingUID'] 	= 'poUID';	
	tags['Title'] 				= 'label';
	tags['Length'] 				= 'duration';
	tags['Rating'] 				= 'rating';		
	tags['LongDescription'] 	= 'synopsis';
	tags['PosterBoard'] 		= 'poster';
	
	//
	// build movie group
	//
	
	moviesString = '{ ';
	moviesString = moviesString+'"'+category+'": [  ';	
	
	var comma  = '';
	var comma2 = '';
	
	$.each(movies, function(index, DataArea) { 
		$.each(DataArea, function(index, ListOfEntry) {		  
			$.each(ListOfEntry, function(index, Asset) {
				if	(index>=0&&index<=999)	{						 					 
					moviesString = moviesString+comma2+' {';
					comma  = '';
					comma2 = ',';
					$.each(Asset.tagAttribute, function(tagName, tagAttribute) {
						if(tagName=='hierarchyUID'||tagName=='localEntryUID'||tagName=='productOfferingUID')	{
							tagAttribute = cleanJSON(tagAttribute);			
							moviesString = moviesString+comma+' "'+tags[tagName]+'":"'+tagAttribute+'"';
							comma = ',';
						}
					});	
					$.each(Asset.ListOfMetaData, function(tagName, tagAttribute) {	
						if(tagName=='Title'||tagName=='Length'||tagName=='Rating'||tagName=='LongDescription'||tagName=='PosterBoard')	{								  
							tagAttribute = cleanJSON(tagAttribute);	
							if(tagName=='PosterBoard')	{
								tagAttribute = parsePoster(tagAttribute);
							}
							moviesString = moviesString+comma+' "'+tags[tagName]+'":"'+tagAttribute+'"';
							comma = ',';
						}
					});				
					moviesString = moviesString+' }';
					
				}
			});	
		});
	});
	
	moviesString = moviesString+' ] } ';
	
	//
	// filter out the movies
	//
	
	var moviesObject = JSON.parse(moviesString);
	
	comma='';
	var atleastone = 'N';
	
	var filteredString = '{ "'+category+'": [';
	$.each(moviesObject[category], function(i,row){									
		if (row["poUID"]=='3')	{
			
			rating  = '';
			if (row["rating"])
				rating = getRating(row["rating"]);
				
			filteredString = filteredString+comma+' { "hUID":"'+row["hUID"]+'", "tag":"'+row["tag"]+'", "poUID":"'+row["poUID"]+'", "label":"'+row["label"]+'", "duration":"'+row["duration"]+'", "rating":"'+rating+'", "synopsis":"'+row["synopsis"]+'", "poster":"'+row["poster"]+'" }';
			comma = ',';
			atleastone = 'Y';
		}
	});	
	if (atleastone=='N')	{
		filteredString = filteredString+comma+' { "hUID":"", "tag":"EMPTY3", "poUID":"3", "label":"no movies", "duration":"", "rating":"", "synopsis":"", "poster":"" }';		
	}
	filteredString = filteredString+' ] }';
		
	$("#JSON_movies").html(filteredString);		// contains just healthcare bookmarks
	
	//
	// build individual movies
	//
	
// CHANGE BEG: 20120228-1100 bsears - change via Dennis: load/reload movie data every time movies are accessed
	$("#json #MOVIEDATA").text('');
// CHANGE END: 20120228-1100 bsears - change via Dennis: load/reload movie data every time movies are accessed	
	
	var filteredObject = JSON.parse(filteredString);
	
	var videoString = '';
	var checkexists = '';
	comma='';
		
	$.each(filteredObject[category], function(i,row){
											  
		videoString = '{ "type":"movie", "hUID":"'+row["hUID"]+'", "tag":"'+row["tag"]+'", "poUID":"'+row["poUID"]+'", "title":"'+row["label"]+'", "duration":"'+row["duration"]+'", "rating":"'+row["rating"]+'", "synopsis":"'+row["synopsis"]+'", "poster":"'+row["poster"]+'" }';
		
		checkexists = $("#SELECTION_"+row["tag"]).text();
		if (!checkexists)
// CHANGE BEG: 20120228-1100 bsears - change via Dennis: load/reload movie data every time movies are accessed
//			$("#json").append('<div id="SELECTION_'+row["tag"]+'"></div>');
			$("#json #MOVIEDATA").append('<div id="SELECTION_'+row["tag"]+'"></div>');
// CHANGE END: 20120228-1100 bsears - change via Dennis: load/reload movie data every time movies are accessed
		$("#SELECTION_"+row["tag"]).html(videoString);					

	});						

	return filteredString;
}

function movieCategories()	{
	
	var moviecategoryString = $("#JSON_moviecategory").text();
	var moviecategory = JSON.parse(moviecategoryString);
	
	return moviecategory;
}

function moviePANEL(panel)	{
	
	var panelString = $("#PANEL_"+panel).text();
	var panel = JSON.parse(panelString);
	
	return panel;
}

function movieSELECTION(choice)	{
	
	var selectionString = $("#SELECTION_"+choice).text();
	var selection = JSON.parse(selectionString);
	
	return selection;
}

function parsePoster(poster)	{
	
	var postername = poster.substring(0,45);
	postername = clean4JSON(postername);
	return postername;
}

// play the video

function getAsset(assetID)	{			// not used
	
	var ewf 	= ewfObject();
	var patient = loadJSON('patient');
	
	var arg1	= 'attribute=json_libs_stb_get_asset';
	var arg2	= 'local_entry_UID='+assetID;
	var arg3	= 'home_id='+patient.homeID;
	
	var url		= ewf.host+ewf.method;
	var args    = arg1+'&'+arg2+'&'+arg3;	
	//$("#CALL_asset").html(url+'?'+args);
	
	var asset = ajaxJSON(url,args);

	var assetString = JSON.stringify(asset);
	//$("#EWF_asset").html(assetString);
	
	var asset = Array();
	asset['duration'] = 0;
	
	//assetString = '{ "Asset":  {';
	
	//var comma  = '';
	
	$.each(asset, function(index, DataArea) { 	   
		comma  = '';
		$.each(DataArea.tagAttribute, function(tagName, tagAttribute) {								
			//tagAttribute = cleanJSON(tagAttribute);			
			//assetString = assetString+comma+' "'+tagName+'":"'+tagAttribute+'"';
			//comma = ',';
		});	
		$.each(DataArea.ListOfMetaData, function(tagName, tagAttribute) {								  			  
			//tagAttribute = cleanJSON(tagAttribute);
			if (tagName=='Length')	{
				asset['duration'] = tagAttribute;	
			}
			//assetString = assetString+comma+' "'+tagName+'":"'+tagAttribute+'"';
			//comma = ',';
		});				
	});
	
	//assetString = assetString+' } }';
	//$("#JSON_asset").html(assetString);
		
	//var assetDATA = JSON.parse(assetString);
	
	return asset;
	
}

function purchaseAsset(poUID,localEntryUID)	{	
	
	var ewf 			= ewfObject();
	var patient 		= loadJSON('patient');
	var device  		= loadJSON('device');
	
	var arg1	= 'attribute=json_libs_ote_purchase_product';
	var arg2	= 'local_entry_id='+localEntryUID;
	var arg3	= 'product_offering_id='+poUID;
	var arg4	= 'home_id='+patient.homeID;
	var arg5	= 'device_id='+device.deviceID;
	var arg6	= 'entry_type=6';
	var arg7	= 'ticket_id=1';

	var url		= ewf.host+ewf.method;
	var args    = arg1+'&'+arg2+'&'+arg3+'&'+arg4+'&'+arg5+'&'+arg6+'&'+arg7;
	//$("#CALL_purchase").html(url+'?'+args);
	
	var purchase = ajaxJSON(url,args);

	var purchaseString = JSON.stringify(purchase);
	//$("#EWF_purchase").html(purchaseString);
	
	return 'success';	
	
}

function positionAsset(homeID,ticketID,suspendPosition)	{	
	
	var ewf 			= ewfObject();
//	var patient 		= loadJSON('patient');
//	var device  		= loadJSON('device');
	
	var arg1	= 'attribute=json_libs_ote_update_ticket_suspend_position';
	//var arg2	= 'device_id='+device.deviceID;
	//var arg3	= 'home_id='+patient.homeID;
	//var arg2	= 'device_id='+deviceID;
	var arg3	= 'home_id='+homeID;
	var arg4	= 'ticket_id='+ticketID;
	var arg5	= 'suspend_position='+suspendPosition;
	//var arg6	= 'product_offering_type='+poUID;
	//var arg7	= 'user_id='+patient.userID;

	var url		= ewf.host+ewf.method;
	//var args    = arg1+'&'+arg2+'&'+arg3+'&'+arg4+'&'+arg5+'&'+arg6+'&'+arg7;
	var args    = arg1+'&'+arg3+'&'+arg4+'&'+arg5;
	var ignore  = true;
	//$("#CALL_position").html(url+'?'+args);
	//msg('CALL: '+url+'?'+args+', '+ignore);
	
	var purchase = ajaxJSON(url,args,ignore);
	
	if (!purchase) return 'ignore';

	var purchaseString = JSON.stringify(purchase);
	//$("#EWF_position").html(purchaseString);
	//msg('RESULT: '+purchaseString);
	
	return 'success';
}

function getServerLoadDATA()	{
		 
	var ewf = ewfObject();
	
	var arg1	= 'attribute=json_libs_ote_get_server_load_info';
	var arg2	= 'application_uid='+ewf.applicationUID;
	var arg3	= 'node_group='+ewf.nodeGroup;
	
	var url		= ewf.host+ewf.method;
	var args    = arg1+'&'+arg2+'&'+arg3;
	//$("#CALL_serverload").html(url+'?'+args);
	
	var serverload = ajaxJSON(url,args);

	var serverloadString = JSON.stringify(serverload);
	//$("#EWF_serverload").html(serverloadString);
	
	var comma		= '';

	var serverloadString 	= '{';
						 
	$.each(serverload, function(index, DataArea) {  
		$.each(DataArea.ListOfServerList, function(index, CMGroup) {
			$.each(CMGroup.tagAttribute, function(tagName, tagAttribute) {								  
				serverloadString = serverloadString+comma+' "'+tagName+'":"'+tagAttribute+'"';
				comma = ',';
			});	
		});			
	});	
	
	serverloadString = serverloadString+' }';
	$("#JSON_serverload").html(serverloadString);
		
	var serverloadDATA = JSON.parse(serverloadString);
	
	return serverloadDATA;
}

function cancelAsset(ticketID)	{	
	
	var ewf 	= ewfObject();
	var patient = loadJSON('patient');
	var device  = loadJSON('device');
	
	var arg1	= 'attribute=json_libs_ote_delete_ticket';
	var arg2	= 'home_id='+patient.homeID;
	var arg3	= 'device_id='+device.deviceID;
	var arg4	= 'user_id='+patient.userID;
	var arg5	= 'ticket_id='+ticketID;
	
	var url		= ewf.host+ewf.method;
	var args    = arg1+'&'+arg2+'&'+arg3+'&'+arg4+'&'+arg5;
	//$("#CALL_purchase").html(url+'?'+args);
	
	var cancel = ajaxJSON(url,args);

	var cancelString = JSON.stringify(cancel);
	//$("#EWF_purchase").html(purchaseString);
	
	return 'success';
}

function getChannelsDATA()	{
	
	var ewf 	= ewfObject();
	
	var arg1	= 'attribute=json_libs_clu_get_region_channel_group';
	var arg2	= 'region_id='+ewf.regionChannelGroup;
	
	var url		= ewf.host+ewf.method;
	var args    = arg1+'&'+arg2;
	//$("#CALL_channelgroup").html(url+'?'+args);
	
	var channelgroup = ajaxJSON(url,args);

	var channelgroupString = JSON.stringify(channelgroup);
	//$("#EWF_channelgroup").html(channelgroupString);
	
	var comma		= '';
	var comma2		= '';
// CHANGE BEG: 20120215-1130 bsears - change via Dennis: implement use of channel lineup
	var channellineup = '';
// CHANGE END: 20120215-1130 bsears - change via Dennis: implement use of channel lineup
	
	var channelsString 		= '{ "Channels": [ ';
	  
	$.each(channelgroup, function(index, DataArea) {  
		$.each(DataArea.tagAttribute, function(tagName, tagAttribute) {			  
		});		
		$.each(DataArea.ListOfChannel, function(index, Channel) {
			channelsString = channelsString+comma2+' {';
			comma2 = ',';
			comma  = '';
			$.each(Channel.tagAttribute, function(tagName, tagAttribute) {								  
				channelsString = channelsString+comma+' "'+tagName+'":"'+tagAttribute+'"';
				comma = ',';
			});	
			channelsString = channelsString+' }';
		});	
		$.each(DataArea.ListOfChannelLineUp, function(index, ChannelLineUp) {								
			$.each(ChannelLineUp.tagAttribute, function(tagName, tagAttribute) {
// CHANGE BEG: 20120215-1130 bsears - change via Dennis: implement use of channel lineup														
				if (tagName=='channels')
					channellineup=tagAttribute;	
// CHANGE END: 20120215-1130 bsears - change via Dennis: implement use of channel lineup													
			});	
		});				
	});	
	
	
	channelsString = channelsString+' ] }';
	$("#JSON_channels").html(channelsString);
	var channelsDATA = JSON.parse(channelsString);
// CHANGE BEG: 20120215-1130 bsears - change via Dennis: implement use of channel lineup
	$("#JSON_channellineup").text(channellineup);
// CHANGE END: 20120215-1130 bsears - change via Dennis: implement use of channel lineup
	
	
	comma = '';
	var frequency = '';
	var pgmnumber = '';
	var scenictv  = '';
// CHANGE BEG: 20120215-1130 bsears - change via Dennis: implement use of channel lineup
	var channelID = '';
	var inlineup  = '';
// CHANGE END: 20120215-1130 bsears - change via Dennis: implement use of channel lineup

// CHANGE BEG: 20120223-0900 bsears - change via Dennis: load/reload channel group/lineup on access to watchtv, musicradio and scenictv (PENDING)
	$("#json #EPG_PGMDATA").text('');
// CHANGE END: 20120223-0900 bsears - change via Dennis: load/reload channel group/lineup on access to watchtv, musicradio and scenictv (PENDING)
	
	channelsString = '{ "Channels": [ ';
	$.each(channelsDATA['Channels'], function(i,row){
											  
// CHANGE BEG: 20120215-1130 bsears - change via Dennis: implement use of channel lineup
		channelID = row["channelID"];
		inlineup = lineupFILTER(channelID);
		inlineup = 'Y';
		
		if (inlineup=='Y')	{
// CHANGE END: 20120215-1130 bsears - change via Dennis: implement use of channel lineup
											  				
		if (row["frequency"])		
			frequency=', "frequency":"'+row["frequency"]+'"';
			else
				frequency=', "frequency":"'+ewf.EPGChannelFrequency+'"';
			
		if (row["programNumber"]) 	
			pgmnumber=', "programNumber":"'+row["programNumber"]+'"';
			else
				pgmnumber=', "programNumber":"'+ewf.EPGChannelProgramNumber+'"';
			
		channelsString = channelsString+comma+'{ "channelID":"C'+row["channelID"]+'", "channelNumber":"'+row["channelNumber"]+'", "channelName":"'+row["channelShortName"]+'"'+frequency+pgmnumber+' }';
		comma = ',';
		// build channel div for later use
// CHANGE BEG: 20120223-0900 bsears - change via Dennis: load/reload channel group/lineup on access to watchtv, musicradio and scenictv (PENDING)
//		$("#json").append('<div id="CHANNEL_C'+row["channelID"]+'"></div>');
		$("#json #EPG_PGMDATA").append('<div id="CHANNEL_C'+row["channelID"]+'"></div>');
// CHANGE END: 20120223-0900 bsears - change via Dennis: load/reload channel group/lineup on access to watchtv, musicradio and scenictv (PENDING)
		
// CHANGE BEG: 20120215-1130 bsears - change via Dennis: implement use of channel lineup		
		}
// CHANGE END: 20120215-1130 bsears - change via Dennis: implement use of channel lineup
		
	});	
	
// CHANGE BEG: 20120216-1300 bsears - change via Dennis: implement use of music channels 108,109,110 (music only logic) TEMP
//	channelsString = channelsString+comma+'{ "channelID":"C108", "channelNumber":"108", "channelName":"Local NPR", "frequency":"621000", "programNumber":"2", "channelUsage":"AudioOnly"  }';
//	$("#json").append('<div id="CHANNEL_C108"></div>');
//	channelsString = channelsString+comma+'{ "channelID":"C109", "channelNumber":"109", "channelName":"Classic King FM", "frequency":"621000", "programNumber":"2", "channelUsage":"AudioOnly"  }';
//	$("#json").append('<div id="CHANNEL_C109"></div>');
//	channelsString = channelsString+comma+'{ "channelID":"C110", "channelNumber":"110", "channelName":"Local Sports", "frequency":"621000", "programNumber":"2", "channelUsage":"AudioOnly"  }';
//	$("#json").append('<div id="CHANNEL_C110"></div>');
// CHANGE BEG: 20120216-1300 bsears - change via Dennis: implement use of music channels 108,109,110 (music only logic) TEMP
	
// mumbo jumbo
	var client = $("#K_client").text();
	if (client=='ACESO')	{
		channelsString = channelsString+comma+'{ "channelID":"C999", "channelNumber":"999", "channelName":"Channel 999"'+frequency+pgmnumber+' }';
		comma = ',';
// CHANGE BEG: 20120223-0900 bsears - change via Dennis: load/reload channel group/lineup on access to watchtv, musicradio and scenictv (PENDING)		
//		$("#json").append('<div id="CHANNEL_C999"></div>');	
		$("#json #EPG_PGMDATA").append('<div id="CHANNEL_C999"></div>');
// CHANGE END: 20120223-0900 bsears - change via Dennis: load/reload channel group/lineup on access to watchtv, musicradio and scenictv (PENDING)
	}
// mumbo jumbo	
	
	channelsString = channelsString+' ] }';
	$("#EPG_channels").html(channelsString);
	var channelsDATA = JSON.parse(channelsString);	
	
	var client = $("#K_client").text();
	if (client=='SWEDISH')	{
		var watchtv    = watchtvFILTER(channelsDATA);
		var musicradio = musicradioFILTER(channelsDATA);
	}
	
	return channelsDATA;
}

// CHANGE BEG: 20120215-1130 bsears - change via Dennis: implement use of channel lineup
function lineupFILTER(channel)	{
	
	var inlineup = 'N';	
	var channellineup = $("#JSON_channellineup").text();
	var lineup = channellineup.split(",");
	var l = lineup.length;
	var i = 0;
	
	for(i=0;i<l;i++)	{
		if (channel==lineup[i])	{
			inlineup='Y';			
			break;
		}
	}
	
	return inlineup;
}
// CHANGE END: 20120215-1130 bsears - change via Dennis: implement use of channel lineup

function watchtvFILTER(channels)	{

	var comma = '';
// CHANGE BEG: 20120215-0900 bsears - change via Dennis: correct problem that excludes channel 10
	var channelNumber  = 0;
// CHANGE END: 20120215-0900 bsears - change via Dennis: correct problem that excludes channel 10
	var channelsString = '{ "Channels": [ ';

	$.each(channels['Channels'], function(i,row){									  
// CHANGE BEG: 20120215-0900 bsears - change via Dennis: correct problem that excludes channel 10										  
//		if (row["channelNumber"]<'90'&&row["channelNumber"]>'107')	{
		channelNumber = row["channelNumber"]*1;
		if (channelNumber<90)	{
// CHANGE END: 20120215-0900 bsears - change via Dennis: correct problem that excludes channel 10
			channelsString = channelsString+comma+'{ "channelID":"'+row["channelID"]+'", "channelNumber":"'+row["channelNumber"]+'", "channelName":"'+row["channelName"]+'", "frequency":"'+row["frequency"]+'", "programNumber":"'+row["programNumber"]+'" }';	
			comma = ',';
		}
	});	
	channelsString = channelsString+' ] }';
	$("#EPG_watchtv").html(channelsString);
	
	return 'success';
}

function musicradioFILTER(channels)	{

	var comma = '';
// CHANGE BEG: 20120215-0900 bsears - change via Dennis: correct problem that excludes channel 10
	var channelNumber  = 0;
// CHANGE END: 20120215-0900 bsears - change via Dennis: correct problem that excludes channel 10
// CHANGE BEG: 20120214-1300 bsears - change via Dennis: implement use of music channels 108,109,110 (music only logic)
	var channelUsage   = '';
// CHANGE END: 20120214-1300 bsears - change via Dennis: implement use of music channels 108,109,110 (music only logic)

	var channelsString = '{ "Channels": [ ';

	$.each(channels['Channels'], function(i,row){									  
// CHANGE BEG: 20120215-0900 bsears - change via Dennis: correct problem that excludes channel 10
//		if (row["channelNumber"]>='90'||(row["channelNumber"]>='100'&&row["channelNumber"]<='107'))	{	
		channelNumber = row["channelNumber"]*1;
// CHANGE BEG: 20120214-1300 bsears - change via Dennis: implement use of music channels 108,109,110 (music only logic)
		channelUsage   = '';
		if (channelNumber==108||channelNumber==109||channelNumber==110)
			channelUsage = ', "channelUsage":"AudioOnly"';
// CHANGE END: 20120214-1300 bsears - change via Dennis: implement use of music channels 108,109,110 (music only logic)
		if (channelNumber>=90)	{
// CHANGE END: 20120215-0900 bsears - change via Dennis: correct problem that excludes channel 10
// CHANGE BEG: 20120214-1300 bsears - change via Dennis: implement use of music channels 108,109,110 (music only logic)
//			channelsString = channelsString+comma+'{ "channelID":"'+row["channelID"]+'", "channelNumber":"'+row["channelNumber"]+'", "channelName":"'+row["channelName"]+'", "frequency":"'+row["frequency"]+'", "programNumber":"'+row["programNumber"]+'" }';	
			channelsString = channelsString+comma+'{ "channelID":"'+row["channelID"]+'", "channelNumber":"'+row["channelNumber"]+'", "channelName":"'+row["channelName"]+'", "frequency":"'+row["frequency"]+'", "programNumber":"'+row["programNumber"]+'"'+channelUsage+' }';	
// CHANGE END: 20120214-1300 bsears - change via Dennis: implement use of music channels 108,109,110 (music only logic)
			comma = ',';
		}
	});	
	channelsString = channelsString+' ] }';
	$("#EPG_musicradio").html(channelsString);
	
	return 'success';
}

function epgChannels()	{
	
	var client = $("#K_client").text();
	if (client=='SWEDISH')	{
// CHANGE BEG: 20120216-1500 bsears - change via Dennis: correct use of panel/grid for epg data access
//		var panel = $("#K_panel").attr("class");
//		if (panel=='watchtv')
//			var channelsString = $("#EPG_watchtv").text();
//			else
//			if (panel=='musicradio')
//				var channelsString = $("#EPG_musicradio").text();
//				else
//					var channelsString = $("#EPG_channels").text();	
		var grid = $("#K_grid").attr("class");
		if (grid=='watchtv')
			var channelsString = $("#EPG_watchtv").text();
			else
			if (grid=='musicradio')
				var channelsString = $("#EPG_musicradio").text();
				else
					var channelsString = $("#EPG_channels").text();	
// CHANGE END: 20120216-1500 bsears - change via Dennis: correct use of panel/grid for epg data access
	}	else	{
			var channelsString = $("#EPG_channels").text();
		}
				
	var channels = JSON.parse(channelsString);
	
	return channels;
}

function epgChannel(channel)	{
		
	var channelString = $("#CHANNEL_"+channel).text();
	var channel = JSON.parse(channelString);
	
	return channel;
}


