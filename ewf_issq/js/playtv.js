//
// Play TV
//

function playTV(channel)	{
	
// CHANGE BEG: 20120214-1300 bsears - change via Dennis: activate musicradio	
//	var panel = $("#K_panel").attr("class");
	var grid = $("#K_grid").attr("class");
// CHANGE END: 20120214-1300 bsears - change via Dennis: activate musicradio
		
	var ewf = ewfObject();
	var url = '';
	
	var channels 	  = epgChannels();
	var programNumber = '';
	var frequency 	  = '';
	var channelNumber = '';
	var channelName   = '';
	var scenictvF     = '57000';
// CHANGE BEG: 20120214-1300 bsears - change via Dennis: implement use of music channels 108,109,110 (music only logic)
	var channelUsage  = '';
// CHANGE END: 20120214-1300 bsears - change via Dennis: implement use of music channels 108,109,110 (music only logic)
	
	$.each(channels['Channels'], function(i,row){	
										  
		if (channel==row["channelID"])	{
			
			$("#VIDEO_nowplaying").text(channel);
			
			programNumber = row["programNumber"];
			frequency	  = row["frequency"];
			channelNumber = row["channelNumber"];
			channelName   = row["channelName"];
			
// CHANGE BEG: 20120214-1300 bsears - change via Dennis: implement use of music channels 108,109,110 (music only logic)
			if (row["channelUsage"])
				channelUsage = ' ChannelUsage="'+row["channelUsage"]+'"';
// CHANGE END: 20120214-1300 bsears - change via Dennis: implement use of music channels 108,109,110 (music only logic)			
			
			var overlay = '<p>'+channelNumber+' '+channelName+'</p>';
			$("#overlay #channelinfo").html(overlay);
			$("#overlay").fadeIn('slow');
			$("#overlay #channelinfo").fadeIn('slow');
			
		}
		
	});	
		
	var version = $("#K_version").text();
	
	var parm1 = 'PhysicalChannelIDType=Freq';
	var parm2 = 'PhysicalChannelID="'+frequency+'"';
	var parm3 = 'DemodMode="QAM'+ewf.EPGChannelQAMMode+'"';
	var parm4 = 'ProgramSelectionMode="'+ewf.EPGProgramSelectionMode+'"';
	var parm5 = 'ProgramID="'+programNumber+'"';

// CHANGE BEG: 20120214-1300 bsears - change via Dennis: activate musicradio
//	if (panel=='musicradio')	{
	if (grid=='musicradio')	{
// CHANGE END: 20120214-1300 bsears - change via Dennis: activate musicradio
		var parms = ' '+parm1+' '+parm2+' ';
// CHANGE BEG: 20120214-1300 bsears - change via Dennis: implement use of music channels 108,109,110 (music only logic)
//		url = '<ChannelParams ChannelType="Analog"><AnalogChannelParams'+parms+'></AnalogChannelParams></ChannelParams>';	
		url = '<ChannelParams ChannelType="Analog"'+channelUsage+'><AnalogChannelParams'+parms+'></AnalogChannelParams></ChannelParams>';
// CHANGE END: 20120214-1300 bsears - change via Dennis: implement use of music channels 108,109,110 (music only logic)
	}	else	{
			var parms = ' '+parm1+' '+parm2+' '+parm3+' '+parm4+' '+parm5+' ';
			url = '<ChannelParams ChannelType="Digital"  AuthRequired="TRUE"><DigitalChannelParams'+parms+'></DigitalChannelParams></ChannelParams>';
		}
		
	var sessionID = getSessionID();
		
	if (version=='PROD')	{
		
		var player = Nimbus.getPlayer( url, null, sessionID );
		
		if (player)	{
				
			player.setChromaKeyColor(0x00000000);
        	//player.setVideoLayerBlendingMode("uniform_alpha");
			player.setVideoLayerBlendingMode("colorkey");
			player.setVideoLayerTransparency(1);
			//player.raiseVideoLayerToTop();
			player.setPictureFormat("Widescreen");
			player.setVideoLayerRect(0, 0, 1920, 1080);
			player.setVideoLayerEnable(true);
		
			player.play();
		}
				
	}	
	
	$("#overlay #channelinfo").delay(3000).fadeOut('slow');
	
	return url;
}

function getSessionID()	{

	var mac	= getDevice();
	var sessionID = 'SEC'+mac.substr(7,5);

	var d = new Date();
	var day		= d.getDate();
	var month	= d.getMonth() + 1;	
	var hours 	= d.getHours();
	var minutes	= d.getMinutes();
	var seconds	= d.getSeconds();

	var year 	= d.getFullYear() - 2000;
	
	day 	= fixZero(day);
	month 	= fixZero(month);
	hours 	= fixZero(hours);
	minutes = fixZero(minutes);
	seconds = fixZero(seconds);
	
	var sessionDate = day+month+year+hours+minutes+seconds;
	
	sessionID = sessionID+sessionDate;
	
	return sessionID;	
}

function donothing()	{
	return;
}

function fixZero(val)	{
	val = ''+val;
	if (val.length==1) 
		val = '0'+val;
	
	return val;
}

function scenicTV()	{
	
	var ewf = ewfObject();
	
	var url = playTV('C'+ewf.scenictvChannel);
		
	return url;
}

function updnChannel(key)	{
	
	var channels = epgChannels();
	var channel  = $("#VIDEO_nowplaying").text();
	var curr     = '';
	var prev     = '';
	var next     = '';
	var first	 = '';
	var last	 = '';
	
	$.each(channels['Channels'], function(i,row){
		if (first=='') 
			first = row["channelID"];
		if (curr!=''&&next=='')   	
			next = row["channelID"];
		last = row["channelID"];
		if (channel==row["channelID"])	{
			curr = row["channelID"];
		}
		if (curr=='')
			prev = row["channelID"];
	});
	if (prev=='') prev = last;
	if (next=='') next = first;
	
	if (key=='CHUP') channel = next;
		else
		if (key=='CHDN') channel = prev;
		
	var stoptv = stopTV();
	var playtv = playTV(channel);	
	
	return playtv;
}

function keyChannel(key)	{
	
	//msg('keyChannel');
	
	var cLength = 1;
	
	var keycheck = $("#K_keycheck").text();
	if (keycheck)	{
		clearInterval(keycheck);
		$("#K_keycheck").text('');
	}
	
	var pKey = $("#K_key").text();
	
	if (pKey)	{
		$("#K_key").text('');
		pKey = pKey+key;
		cLength = pKey.length;
		if 	(cLength == 3)	{
			key3Channel(pKey);
			return;
		}
		key = pKey;
	}
	
	if (cLength==1&&key=='0')	
		return;
	
	$("#K_key").text(key);
	var keycheck = setInterval( "key2Channel()", 2000 );
	$("#K_keycheck").text(keycheck);
	
	//msg('interval set');
	
	// CHANGE BEG: 20130824 gsullins - displaying numbers as they are being pressed to change the channel
	var overlay = '<p>'+key+'</p>';
	$("#overlay #channelinfo").html(overlay);
	$("#overlay").fadeIn('slow');
	$("#overlay #channelinfo").fadeIn('slow');
	// CHANGE END: 20130824 gsullins - displaying numbers as they are being pressed to change the channel
	
	return;
}

function key2Channel()	{
	
	//msg('key2Channel');
	
	var keycheck = $("#K_keycheck").text();
	clearInterval(keycheck);
	$("#K_keycheck").text('');
	
	var pKey = $("#K_key").text();	
	$("#K_key").text('');
	
	key3Channel(pKey);	
}

function key3Channel(pKey)	{
	
	//msg('key3Channel');

	var channels 	  = epgChannels();
	var channel		  = '';
	
	$.each(channels['Channels'], function(i,row){	
										  
		if (pKey==row["channelNumber"])	{
			channel = row["channelID"];
			//msg('channel: '+pKey+' '+channel);
		}
		
	});	
	
//	var panel = $("#K_panel").attr("class");
//	if (panel!='scenictv')	{
//		$("#K_panel").removeClass(panel);
//		$("#"+panel).hide();
//		panel = 'tv';
//		$("#K_panel").addClass(panel);
//		$("#overlay").fadeIn('slow');
//	}
	
	if (channel)	{
		stopTV();
		playTV(channel);
	}	else	{
			var overlay = '<p>'+pKey+' not available</p>';
			$("#overlay #channelinfo").html(overlay);
			$("#overlay").fadeIn('slow');
			$("#overlay #channelinfo").fadeIn('slow').delay(3000).fadeOut('slow');	
			//msg('Channel '+pKey+' is not valid');
		}
	
	return;
}

function stopTV()	{
	
	$("#overlay #channelinfo").hide();
	$("#overlay").hide();
	
	var version = $("#K_version").text();
	if (version=='PROD')	{
		var player = Nimbus.getPlayer();
		if (player)	{
			player.setVideoLayerEnable(false);
			//player.setVideoLayerTransparency(1);
			//player.lowerVideoLayerToBottom();
			player.stop();
			player.close();
		}	
	}
	
	return;	
}

function setCC()	{
	
	var version = $("#K_version").text();
	if (version=='PROD')	{
		var cc = Nimbus.getCCMode();
		if (cc=='Off')	{
			Nimbus.setCCMode('On');
			msg('Closed Captioning has been turned ON');
		}	else
			if (cc=='On')	{
				Nimbus.setCCMode('Off');
				msg('Closed Captioning has been turned OFF');
			}
	}
	
	return;	
}

//function handshakeTV()	{
//	
//	var version = $("#K_version").text();
//	var client  = $("#K_client").text();
//	if (version=='PROD')	{
//		
//		var player = Nimbus.getPlayer();
//		if (player)	{
//	
//			var pStatus = Array();
//			
//			pStatus[0]	= 'Playback opened successfully or last player command successful.'.
//			pStatus[1] 	= 'Normally should not occur.'.
//			pStatus[2]	= 'Obsolete.  Replaced with more specific error status codes.'.
//			pStatus[3]	= 'Playback failed.  R/F channel type specified but no R/F front end hardware.'.
//			pStatus[4]	= 'Playback failed.  Board power state is OFF.'.
//			pStatus[5]	= 'Playback failed.  Cannot lock to signal.  Open retries will occur.'.
//			pStatus[6]	= 'Enseo Proidiom authorization in progress.'.
//			pStatus[7]	= 'Not used.'.
//			pStatus[8]	= 'Playback is in the process of opening a new channel.'.
//			pStatus[9]	= 'Playback is closed.'.
//			pStatus[10]	= 'Playback reached the end of the stream (RTSP only).'.
//			pStatus[11]	= 'Playback failed.  Required resource is busy (Tuner/demod for RF channel).'.
//			pStatus[12]	= 'Playback failed.  The content is protected but output protection is not enabled.'.
//			pStatus[20]	= 'RTSP Playback failure: TCP open failure.'.
//			pStatus[21]	= 'RTSP Playback failure: Options request failed.'.
//			pStatus[22]	= 'RTSP Playback failure: Describe request failed.'.
//			pStatus[23]	= 'RTSP Playback failure: Setup request failed.'.
//			pStatus[24]	= 'RTSP Playback failure: Play request failed.'.
//			pStatus[25]	= 'RTSP Playback failure: Pause request failed.'.
//			pStatus[50]	= 'Playback failed.  Analog decoder cannot lock to signal.  Open retries will occur.'.
//			pStatus[51]	= 'Playback failed.  Analog capture failed.  Open retries will occur.'.
//			pStatus[60]	= 'Playback failed.  MPEG TS/PS stream player failed to open.  Open retries will occur.'.
//			pStatus[61]	= 'Playback failed.  MPEG TS/PS stream player failed to play.  Open retries will occur.'.
//			pStatus[62]	= 'Playback failed.  MPEG TS stream player failed to find a PAT in the stream.  Open retries will occur.'.
//			pStatus[63]	= 'Playback failed.  MPEG TS stream player failed to find a PMT in the stream.  Open retries will occur.'.
//			pStatus[64]	= 'Playback failed.  MPEG TS stream player failed to find a VCT in the stream.  Open retries will occur.'.
//			pStatus[65]	= 'Playback failed.  MPEG TS stream player failed to find the specified MPEG program in the stream.  Open retries will occur.'.
//			pStatus[66]	= 'Playback failed.  MPEG TS/PS stream player encountered a decoding error.  Open retries will occur.'.
//			pStatus[80]	= 'Playback failed.  RTP/UDP player failed to receive stream data.  Open retries will occur.'.	
//
//			//var status = player.getErrorStatus();
//			Nimbus.logMessage('|'+client+'| Player Status: '+pStatus[player] );
//		}	else	{		
//				Nimbus.logMessage('|'+client+'| Player Status: PLAYER NOT DEFINED' );
//			}
//			
//	}
//	
//	return;
//}
