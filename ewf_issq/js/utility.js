//
// utility functions
//

function datetime()	{

	var d 		= new Date();
	var dow 	= d.getDay();
	var hour 	= d.getHours();
	var minute	= d.getMinutes();
	var month	= d.getMonth();
	var day		= d.getDate();
	var year 	= d.getFullYear();
	var ampm	= 'AM';
	
	if (hour>12)	{
		hour = hour-12;
		ampm  = 'PM';
	}
	if (minute<10)	{
		minute = '0'+minute;
	}	
	

	var weekday = ["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"];
	var months  = ["01","02","03","04","05","06","07","08","09","10","11","12"];
			
	var datetime = '<p>'+hour+':'+minute+' '+ampm+' '+weekday[dow]+' '+months[month]+'.'+day+'.'+year+'</p>';
	
	$("#datetime").html(datetime);
	
	return;
}


function msg(message)	{
	
	var version = $("#K_version").text();
	var client  = $("#K_client").text();
	
	if (version=='TEST' && console && console.log)	{
		console.log(message);
	}	else
	if (version=='PROD')	{
		Nimbus.logMessage('|'+client+'| '+message );
	}	
	
}

function dec2hex(id){

	var returns = '';
	var cha = ['0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F'];
	var temp = '';
	while (id > 0){
		i = id % 16;
		id = Math.floor(id / 16);
		temp = cha[i] + temp;
	}
	returns = temp.toLowerCase();
	
	return returns;
}

function getHour()	{

	var d 		= new Date();
	var hour 	= d.getHours();

	return hour;
}

function checkforReload()	{
	
	var reloadtime = $("#K_reload").text();
	msg('checkforReload...reloadtime: '+reloadtime);
	
	if (reloadtime=='OFF')
		return;
		
	var trigger = reloadtime*1;
	var hour = getHour();
	msg('checkforReload...hour/trigger: '+hour+'/'+trigger);
	
	if (hour!=trigger)
		return;
	
	var power = checkPower();
	msg('checkforReload...power: '+power);
	
	if (power=='OFF')	{
		
		msg('checkforReload...RELOAD');
		
		var panel = $("#K_panel").attr("class");

		if (panel=='video')	stopVideo();
		if (panel=='tv'||panel=='scenictv')	stopTV();
		
		reloadapp();
	}
}

function roundNumber(num,dec) {
	
	var result = num;
	
	if (num!=0)
		result = Math.round(num*Math.pow(10,dec))/Math.pow(10,dec);
	
	if (dec==2)	{
		var resultS = result.toString();
		var resultL = resultS.length;
		var dp 		= resultS.indexOf('.');
		if(dp==-1)	{
			result = resultS+'.00';	
		}	else	{
				var diff = (resultL-1) - dp;
				if (diff==1)
					result = resultS+'0';
			}
	}
	
	return result;
}

//function roundNumber(number,decimal_points) {
//	if(!decimal_points) return Math.round(number);
//	if(number == 0) {
//		var decimals = "";
//		for(var i=0;i<decimal_points;i++) decimals += "0";
//		return "0."+decimals;
//	}
//
//	var exponent = Math.pow(10,decimal_points);
//	var num = Math.round((number * exponent)).toString();
//	return num.slice(0,-1*decimal_points) + "." + num.slice(-1*decimal_points)
//}
