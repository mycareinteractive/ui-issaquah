//
// XML call, parse functions
//
function ajaxXML(type,args)	{
	
	$("#XMLCALL_error").text('');
	
	var dineversion = $("#K_dineversion").text();
	
// CHANGE BEG: 20120201-0200 bsears - add place order xml call
//	if (type=='PATRONINFO'||type=='PATRONMENU')	{
// CHANGE BEG: 20120216-0900 bsears - dine issue via Tami: implement unlock menu
//	if (type=='PATRONINFO'||type=='PATRONMENU'||type=='SELECTMENU')	{
	if (type=='PATRONINFO'||type=='PATRONMENU'||type=='SELECTMENU'||type=='UNLOCKMENU')	{	
// CHANGE END: 20120216-0900 bsears - dine issue via Tami: implement unlock menu
// CHANGE END: 20120201-0200 bsears - add place order xml call		
		
		var ewf	 = ewfObject();
		var url	 = ewf.host+ewf.proxy;
		var dine = ewf.dinehost;
		if(dineversion=='TEST')
			dine = ewf.dinehostTest;
		
		args = 'wsdl='+dine+args;
		//msg(args);
		
		$("#XMLCALL_lastcall").text(args);

		var string = $.ajax({
			type: "GET", 
			url: url, 
			data: args,
			async: false,
			dataType: "script"
			}).responseText;
	
		//msg(string);
		
		$("#XMLCALL_laststring").text(string);
	
	}	else
	if (type=='MENUTYPES')	{
		
		var string = '<?xml version="1.0" encoding="ISO-8859-1" ?> <MENUTYPES> <MENUTYPE menutype="BARIATRIC MAINTENANCE" menuname="Bariatric Maintenance" description="This menu is designed for a patient who has had bariatric surgery in the past. It allows only 3 ounces of food served at one time. Foods are low in sugar and low in fat and should be eaten at regularly spaced intervals throughout the day.  Beverages are non-carbonated." /> <MENUTYPE menutype="BARIATRIC SURGERY" menuname="Bariatric Surgery" description="This menu was designed by your doctor and dietician for post-op bariatric surgery. It consists of 4oz portions of non-carbonated clear liquids and protein supplements. To prevent swallowing air, straws are not allowed." /> <MENUTYPE menutype="BRAT" menuname="Bananas, Rice, Applesauce and Toast" description="This menu consists of Bananas, Rice, Applesauce and Toast. It is a temporary diet intended to assist in managing diarrhea in children." /> <MENUTYPE menutype="CARDIAC" menuname="Cardiac" description="Heart healthy menu based on the American Heart Association guidelines. Daily goals are less than 75g fat, 200mg cholesterol and 2000mg sodium per day.  Foods that are high in fat such as full fat dairy products, eggs, high fat meats and salty condiments are replaced with lower fat and lower sodium options.  Beverages are limited to those that do not contain caffeine or have been decaffeinated. " /> <MENUTYPE menutype="DIABETIC" menuname="Diabetic" description="This is a carbohydrate controlled menu. Items are limited in sugars, starches and saturated fats to prevent your blood sugar from getting too high. Based on your total calorie intake for the day, carbohydrate servings will be 45-75g per meal. " /> <MENUTYPE menutype="DYSPHAGIA ADVANCED" menuname="Dysphagia Advanced" description="Dysphagia means difficulty swallowing. Foods on this menu are chopped and moistened with added sauce or gravy. Liquids may be thickened for safer swallowing." /> <MENUTYPE menutype="DYSPHAGIA MECH. ALTERED" menuname="Dysphagia Mechanically Altered" description="Dysphagia means difficulty swallowing. Foods on this menu are minced, ground or pureed and moistened with added sauce or gravy. Bread-products and rice are not allowed, as well as hard, sticky, or crunchy foods. Liquids may be thickened for safer swallowing." /> <MENUTYPE menutype="DYSPHAGIA PUREED" menuname="Dysphagia Pureed" description="Dysphagia means difficulty swallowing. Foods on this menu are pureed to pudding-like consistency. They are moistened with added sauce or gravy. Liquids may be thickened for safer swallowing. " /> <MENUTYPE menutype="FINGER FOODS" menuname="Finger Foods" description="Foods on this menu do not require use of utensils. They are easy to pick up and eat with your fingers." /> <MENUTYPE menutype="GENERAL" menuname="General" description="The General menu offers our full selection of choices. Order a variety of foods from each food group: grains, fruits, vegetables, dairy, meats and beans. Limit excessive sweets and extra fat." /> <MENUTYPE menutype="GLUTEN FREE" menuname="Gluten Free" description="This Gluten Free menu does not contain wheat, rye, barley or oat products or ingredients. Foods are prepared using separate utensils and a separate toaster to avoid contamination." /> <MENUTYPE menutype="KETOGENIC" menuname="Ketogenic" description="This specialized menu is customized for you by your dietitian. Generally it is very high in fats and very low in carbohydrate foods." /> <MENUTYPE menutype="KOSHER" menuname="Kosher" description="This menu follows the Jewish Dietary Laws. Approved items include pre-packaged meals and snacks." /> <MENUTYPE menutype="LIQUID" menuname="Liquid" description="This menu includes items that are clear liquids such as broth, tea, jell-o and popsicles and full liquids such as soups, yogurt, pudding and ice cream. Full liquids must be liquid at room temperature or body temperature and have a smooth consistency. " /> <MENUTYPE menutype="LOW FAT" menuname="Low Fat" description="The Lowfat menu limits egg yolks, full fat dairy products and high fat meats to maintain 30% or less of your daily calories from fat. Goals are 65 grams of fat or less and 200mg of dietary cholesterol per day. " /> <MENUTYPE menutype="LOW IODINE" menuname="Low Iodine" description="The Low Iodine menu limits foods rich in iodine including seafood, dairy products, soy products and most processed foods." /> <MENUTYPE menutype="LOW RESIDUE" menuname="Low Residue" description="This menu emphasizes foods that are low in fiber and limits whole grains, raw or fibrous fruits, vegetables, beans, nuts and seeds." /> <MENUTYPE menutype="PEDIATRIC" menuname="Pediatric" description="A menu for Pre-School and school-age children. Consists of foods appropriate for age level greater than 3 years, including finger foods." /> <MENUTYPE menutype="RENAL" menuname="Renal" description="A menu to help you manage the effects of kidney disease. Daily intakes of sodium containing foods such as condiments containing salt, salty meats and cheeses and potassium containing foods such as bananas, oranges and potatoes are limited to 2000 mg or less per day.  Phosphorus which is found in foods including sodas and dairy products may be also limited. " /> <MENUTYPE menutype="SODIUM CONTROLLED" menuname="Sodium Controlled" description="This menu contains meals and snacks that provide a total of less than 2000mg sodium per day.  Foods that have added salt such as salad dressings and high sodium meats and cheeses are replaced with lower sodium options." /> <MENUTYPE menutype="SUBSTITUTE" menuname="Substitute" description=" " /> <MENUTYPE menutype="TF" menuname="Tube Feeding" description=" " /> <MENUTYPE menutype="NPO" menuname="NPO" description=" " /> <STATUS success="1" /> </MENUTYPES>';
					
	}
	
// CHANGE BEG: 20120212-1700 bsears - dine issue via Tami: error processing when html error is returned vs. xml with errors	
	if (string.indexOf('<html>') != -1)	{
		$("#XMLCALL_error").text('ERROR: '+string);
		msg(string);
		return string;
	}
// CHANGE END: 20120212-1700 bsears - dine issue via Tami: error processing when html error is returned vs. xml with errors	
	
	xmlString = string.replace(/(\r\n|\n|\r)/g,"");

	//msg(xmlString);
							
	// check for errors
	
	var success = '';
	var error   = '';
	var errorT  = '';
	
	$(xmlString).find('STATUS').each(function(){
		success = $(this).attr('success');
		if (success!='1')	{
			$(xmlString).find('ERROR').each(function(){
				error  = $(this).attr('number');	
				errorT = $(this).attr('text');
				msg('ERROR: '+error+': '+errorT);
				$("#XMLCALL_error").text('ERROR: '+error+': '+errorT);
			});	
		}
	});	

	return xmlString;
}

function getXML(string)	{
	
	var xmlString = $("#XML_"+string).text();
	//var parser = new DOMParser(),
	//var xmlObject = parser.parseFromString(xmlString , "text/xml");
	var xmlObject = xmlString;
	
	return xmlObject;	
}

function getPATRONINFO()	{
	
	var patient = loadJSON('patient');
	var device  = loadJSON('device');
// CHANGE BEG: 20120228-1130 bsears - dine change via Tami: define dine xml call ports as variables and retrieve dynamically at call time	
	var ewf 	= ewfObject();
// CHANGE END: 20120228-1130 bsears - dine change via Tami: define dine xml call ports as variables and retrieve dynamically at call time

	var type 	= 'PATRONINFO';
// CHANGE BEG: 20120228-1130 bsears - dine change via Tami: define dine xml call ports as variables and retrieve dynamically at call time
//	var request = ':8005/hsws/patron_info';
	var request = ':'+ewf.patron_info+'/hsws/patron_info';
// CHANGE END: 20120228-1130 bsears - dine change via Tami: define dine xml call ports as variables and retrieve dynamically at call time
	var name 	= 'name='+patient.userFullName;
	var room 	= 'room='+patient.roomNumber;
	var bed  	= 'bed='+patient.roomBed;
	var mac  	= 'mac='+device.deviceID;
	var args 	= request+'?'+name+'&'+room+'&'+bed+'&'+mac;
	
//	var args = 'patron_info?name=JOHNSMITH&room=2322&bed=p&mac=01AB02FC01';		// TEMP //
	
	$("#XMLCALL_patroninfo").text(args);
	
	var xmlString = ajaxXML(type,args);
	
	$("#XML_patroninfo").text(xmlString);

	return;
}

function getPATRONMENU()	{
	
	var patroninfo 	= getXML('patroninfo');
// CHANGE BEG: 20120228-1130 bsears - dine change via Tami: define dine xml call ports as variables and retrieve dynamically at call time	
	var ewf 		= ewfObject();
// CHANGE END: 20120228-1130 bsears - dine change via Tami: define dine xml call ports as variables and retrieve dynamically at call time
	
	var type 		= 'PATRONMENU';
// CHANGE BEG: 20120228-1130 bsears - dine change via Tami: define dine xml call ports as variables and retrieve dynamically at call time
//	var patron	 	= ':8006/hsws/request_patron_menu';
//	var guest 		= ':8007/hsws/request_guest_menu';
	var patron	 	= ':'+ewf.request_patron_menu+'/hsws/request_patron_menu';
	var guest 		= ':'+ewf.request_guest_menu+'/hsws/request_guest_menu';
// CHANGE END: 20120228-1130 bsears - dine change via Tami: define dine xml call ports as variables and retrieve dynamically at call time
	var mrn 		= 'mrn=';
	var mealid 		= 'mealid=';
	var mealdate	= 'mealdate=';
	var lock  		= 'lock=true';
	var nutri  		= 'nutri=R';
	var menuid 		= 'menuid=33';
	
	$(patroninfo).find('PATRON').each(function(){
		mrn 		= mrn+$(this).attr('mrn');
	});	
	
	$(patroninfo).find('MEAL').each(function(){
		mealid 		= mealid+$(this).attr('id');
		mealdate 	= mealdate+$(this).attr('date');
	});	
	
	var submenu = $("#K_submenu").attr("class");	
	if (submenu=='visitorsmenu')
		var args = guest+'?'+mrn+'&'+menuid+'&'+nutri;
		else
			var args = patron+'?'+mrn+'&'+mealid+'&'+mealdate+'&'+lock+'&'+nutri;	
	
	$("#XMLCALL_patronmenu").text(args);
	
	var xmlString = ajaxXML(type,args);
	
	$("#XML_patronmenu").text(xmlString);
	
	return;
}

function getMENUTYPES()	{
	
	var type = 'MENUTYPES';
	var args = '';
	
	var xmlString = ajaxXML(type,args);
	
	$("#XML_menutypes").text(xmlString);
									  
	return;
}

function getMEAL()	{
	
	getMENUTYPES();
	
	var menutypes  = getXML('menutypes');
	var patroninfo = getXML('patroninfo');
	
	var count 		= 1;
	var meal 		= '';
	var menutype 	= '';
	var menuname 	= '';
	var description = 'Dietary description not available.';
	var mealXML 	= '';
	
	mealXML = mealXML+'<?xml version="1.0" encoding="ISO-8859-1" ?>';
	mealXML = mealXML+'<MEALS>';
	
	$(patroninfo).find('MEAL').each(function(){
		meal 		= $(this).attr('name');
		menutype 	= $(this).attr('menuname');
	});
	
	$(menutypes).find('MENUTYPE').each(function(){
		if (menutype == $(this).attr('menutype'))	{
			menuname 	= $(this).attr('menuname');
			description = $(this).attr('description');
		}
	});
	
	mealXML = mealXML+'<MEAL meal="'+meal+'" menutype="'+menutype+'" menuname="'+menuname+'" description="'+description+'" />';
	mealXML = mealXML+'<STATUS count="'+count+'" />';
	mealXML = mealXML+'</MEALS>';
	
	$("#XML_meal").text(mealXML);

	return;
}

function getNUTRIENTS()	{
	
	var xmlObject = getXML('patronmenu');
	
	var count 			= 0;
	var nutrients 		= '';
	var nutrient 		= '';
	var elements 		= '';
	var nutrientsXML	= '';
	
	nutrientsXML = nutrientsXML+'<?xml version="1.0" encoding="ISO-8859-1" ?>';
	nutrientsXML = nutrientsXML+'<NUTRIENTS>';
	
	$(xmlObject).find('RECIPES').each(function(){
											   
		nutrients = $(this).find('NUTRIENTS').text();
		
		nutrient  = nutrients.split("|");
		
		for(count=0;count<nutrient.length;count++)	{
			if(nutrient[count]!='')	{
				elements = nutrient[count].split("~");
				if (elements[0]=='Fiber/Dtry')
					elements[0]='Fiber';
				nutrientsXML = nutrientsXML+'<NUTRIENT title="'+elements[0]+'" measure="'+elements[2]+'" />';
			}
		}
		
	});	
	
	nutrientsXML = nutrientsXML+'</NUTRIENTS>';
	
	$("#XML_nutrients").text(nutrientsXML);
	
	return;	
}

function getGROUPS()	{
	
	var submenu 	= $("#K_submenu").attr("class");
	
	var xmlObject 	= getXML('patronmenu');
	var groupsDATA  = getXML('groups');
	var groupsXML 	= $.parseXML(groupsDATA);
	
	var count 			= 0;
	var group 			= '';
	var maxselections 	= 0;
	var selections 		= 0;
	
	var groupA 			= Array();
	var groupC 			= 0;
	var groupT 			= 0;
	var exists			= 'N';
	
	$(xmlObject).find('RECIPES').each(function(){
									   						 
		$(this).find('RECIPE').each(function(){
											 
			group 			= $(this).attr('group');
			maxselections 	= $(this).attr('maxselections');
			
//			if(submenu=='visitorsmenu'||group=='Condiments')
//				maxselections = 9;
			if(submenu=='visitorsmenu'||maxselections == 0)
				maxselections = 99;
			
			exists = 'N';
			for(groupC=0;groupC<groupT;groupC++)	{
				if(group==groupA[groupC])
					exists = 'Y';
			}
			
			if (group=='(Unassigned)')
				exists = 'Y';
			
			if (exists=='N')	{
				count++;
				$(groupsXML).find('groups').append($('<group id="group'+count+'" group="'+group+'" maxselections="'+maxselections+'" selections="'+selections+'" /></group>'));
				//groupXML = groupXML+'<group id="group'+count+'" group="'+group+'" maxselections="'+maxselections+'" selections="'+selections+'" />';
				groupA[groupT] = group;
				groupT++;
				
				//msg(group+' '+maxselections);
			}
			
		});
	});	
	
	//groupXML = groupXML+'<STATUS count="'+count+'" />';
	//groupXML = groupXML+'</GROUPS>';
	
	var groupsDATA = (new XMLSerializer()).serializeToString(groupsXML);
	$("#XML_groups").text(groupsDATA);
	
	return;	
}

function getRECIPES(selection)	{
	
	var submenu = $("#K_submenu").attr("class");
	
	var groupsDATA = getXML('groups');
	
	var groupid = '';
	
	$(groupsDATA).find('group').each(function(){					  
		id		= $(this).attr('id');											 
		group 	= $(this).attr('group');
		
		if (id==selection)	{
			selection = group;
			groupid	  = id;
		}
	});	
	
	var xmlObject = getXML('patronmenu');
	
	var count 			= 0;
	var id 				= '';
	var group 			= '';
	var description 	= '';
	var longdescription = '';
	var nutrients 		= '';
	var servings 		= '';
	var price	 		= '';
	var maxquantity	 	= '';
	var recipesXML 		= '';
	
	recipesXML = recipesXML+'<?xml version="1.0" encoding="ISO-8859-1" ?>';
	recipesXML = recipesXML+'<RECIPES>';
	
	$(xmlObject).find('RECIPES').each(function(){
									   						 
		$(this).find('RECIPE').each(function(){
											 
			group = $(this).attr('group');
											 
			if (group==selection)	{			
				id 				= $(this).attr('id');
				description 	= $(this).attr('enticingdescription');
				if(!description)
					description = $(this).attr('shortname');
				longdescription = $(this).attr('publishingtext');
				nutrients 		= $(this).attr('nutrients');
				servings 		= $(this).attr('numservings');	
				price			= 0.00;	
				maxquantity		= 1;
	
				if (submenu=='visitorsmenu')
					price	 	= $(this).attr('price');
				if (submenu=='visitorsmenu'||group=='Condiments')
					maxquantity = 9;
				
				count++;
				
				recipesXML = recipesXML+'<RECIPE id="recipe'+count+'" recipe="'+id+'" groupid="'+groupid+'" group="'+group+'" description="'+description+'" longdescription="'+longdescription+'" nutrients="'+nutrients+'" servings="'+servings+'" price="'+price+'" maxquantity="'+maxquantity+'" />';
			}

		});
	});	
	
	recipesXML = recipesXML+'<STATUS count="'+count+'" />';
	recipesXML = recipesXML+'</RECIPES>';
	
	$("#XML_recipes").text(recipesXML);

	return;	
}

function initORDER(patron)	{
	
	var orderXML 		= '';
	var groupsXML 		= '';
	
	orderXML = orderXML+'<?xml version="1.0" encoding="ISO-8859-1" ?>';
	orderXML = orderXML+'<order></order>';
	$("#XML_order").text(orderXML);	
	
	groupsXML = groupsXML+'<?xml version="1.0" encoding="ISO-8859-1" ?>';
	groupsXML = groupsXML+'<groups></groups>';
	$("#XML_groups").text(groupsXML);
	
	getPATRONINFO();
	var xmlError = $("#XMLCALL_error").text();
	if(xmlError)
		return;
		
// CHANGE BEG: 20120210-1400 bsears - dine issue via Tami: compare call info with returned PATRONINFO info		
		
	if (patron=='mymenu')	{
		validatePATRON();
		var xmlError = $("#XMLCALL_error").text();
		if(xmlError)
			return;
	}
		
// CHANGE BEG: 20120210-1400 bsears - dine issue via Tami: compare call info with returned PATRONINFO info		
	
	if (patron=='mymenu')
		getMEAL();
		
	getPATRONMENU();
	var xmlError = $("#XMLCALL_error").text();
	if(xmlError)
		return;
		
	getNUTRIENTS();
	getGROUPS();

	return;	
}

// CHANGE BEG: 20120210-1400 bsears - dine issue via Tami: compare call info with returned PATRONINFO info	
function validatePATRON()	{
	
	var patient    = loadJSON('patient');
	var patroninfo = getXML('patroninfo');

	var nameI	= patient.userFullName;
	var roomI	= patient.roomNumber;
	var nameO	= '';
	var roomO	= '';
// CHANGE BEG: 20120217-1300 bsears - dine issue via Tami: check for hasselections=1
	var has     = '';
// CHANGE END: 20120217-1300 bsears - dine issue via Tami: check for hasselections=1
		
	$(patroninfo).find('PATRON').each(function(){
		nameO 	= $(this).attr('firstname')+' '+$(this).attr('lastname');
	});	
	
	$(patroninfo).find('ROOM').each(function(){
		roomO 	= $(this).attr('name').substr(0,4);
	});	

// CHANGE BEG: 20120217-1300 bsears - dine issue via Tami: check for hasselections=1
	$(patroninfo).find('MEAL').each(function(){
		has 	= $(this).attr('hasselections');
	});
// CHANGE END: 20120217-1300 bsears - dine issue via Tami: check for hasselections=1
	
	//msg(' name IN: '+nameI+' name OUT: '+nameO+' room IN: '+roomI+' room OUT: '+roomO);
	if (nameI!=nameO||roomI!=roomO)	
		$("#XMLCALL_error").text('ERROR: patron call and returned information conflict... '+' name IN: '+nameI+' name OUT: '+nameO+' room IN: '+roomI+' room OUT: '+roomO);
// CHANGE BEG: 20120217-1300 bsears - dine issue via Tami: check for hasselections=1
		else
		if (has=='1')	
			$("#XMLCALL_error").text('ERROR: patient has already ordered for this meal');
// CHANGE END: 20120217-1300 bsears - dine issue via Tami: check for hasselections=1
		
	return;
}
// CHANGE END: 20120210-1400 bsears - dine issue via Tami: compare call info with returned PATRONINFO info	

function groupMAX(groupid)	{
	
	var groupsDATA  = getXML('groups');
	
	var maxselections = 0;
	var selections 	  = 0;
	var remaining 	  = 0;
	
	$(groupsDATA).find('group').each(function(){					   													 
		if(groupid==$(this).attr('id'))	{
			maxselections = $(this).attr('maxselections');
			selections 	  = $(this).attr('selections');
			remaining = maxselections - selections;
			//msg(remaining+' selections remaining for group: '+groupid);
		}	
	});		

	return remaining;	
}

function updateMAX(groupid,action)	{
	
	var groupsDATA  = getXML('groups');
	var groupsXML 	= $.parseXML(groupsDATA);
	
	var maxselections = 0;
	var selections 	  = 0;
	var remaining 	  = 0;
	
	$(groupsXML).find('group').each(function(){	
											  
		if(groupid==$(this).attr('id'))	{		
			maxselections = $(this).attr('maxselections');
			selections 	  = $(this).attr('selections');
			if(action=='remove')
				selections--;
			else
				if(action=='add')
					selections++;
			$(this).attr('selections',selections);	
		}	
	});	
	
	var groupsDATA = (new XMLSerializer()).serializeToString(groupsXML);
	$("#XML_groups").text(groupsDATA);

	return remaining;	
}

function addORDER(id)	{
	
	var orderDATA  	= getXML('order');
	var orderXML 	= $.parseXML(orderDATA);
	var recipeDATA 	= recipeORDER(id);
	
	var quantity   	= 1;
	
	$(orderXML).find('order').append($('<orderitem recipe="'+recipeDATA.recipe+'" groupid="'+recipeDATA.groupid+'" group="'+recipeDATA.group+'" description="'+recipeDATA.description+'" quantity="'+quantity+'" nutrients="'+recipeDATA.nutrients+'" price="'+recipeDATA.price+'" ></orderitem>'));
	
	var orderDATA = (new XMLSerializer()).serializeToString(orderXML);
	$("#XML_order").text(orderDATA);

	return;	
}

//function updateORDER(id,quantity)	{
//	
//	var orderDATA  = getXML('order');
//	var recipeDATA = recipeORDER(id);
//	
//	//msg(orderDATA);
//	
//	var orderXML = $.parseXML( orderDATA);
//	
//	
//	$(orderXML).find('orderitem').each(function(){							   													 
//		if(recipeDATA.recipe==$(this).attr('recipe'))	{
//			msg('update: '+recipeDATA.recipe);
//			$(this).attr('quantity',quantity);
//		}	
//	});
//	
//	var orderDATA = (new XMLSerializer()).serializeToString(orderXML);
//	
//	//msg(orderDATA);
//	
//	$("#XML_order").text(orderDATA);
//
//	return;	
//}

function removeORDER(id)	{
	
	var orderDATA  	= getXML('order');
	var orderXML 	= $.parseXML( orderDATA);
	var recipeDATA 	= recipeORDER(id);

	$(orderXML).find('orderitem').each(function(){					   													 
		if(recipeDATA.recipe==$(this).attr('recipe'))	{
			//msg('remove: '+recipeDATA.recipe);
			$(this).remove();
		}
	});
	
	var orderDATA = (new XMLSerializer()).serializeToString(orderXML);
	$("#XML_order").text(orderDATA);

	return;	
}

function checkORDER(id)	{
	
	var orderDATA  = getXML('order');
	var recipeDATA = recipeORDER(id);
	
	var ordered = '';
	
	$(orderDATA).find('orderitem').each(function(){					   													 
		if(recipeDATA.recipe==$(this).attr('recipe'))	{
			ordered=true;
		}	
	});		

	return ordered;	
}

function countORDER()	{
	
	var orderDATA  = getXML('order');
	var total	   = 0;
	
	$(orderDATA).find('orderitem').each(function(){			   													 
		total++;	
	});

	return total;	
}

function recipeORDER(id)	{
	
	var recipesDATA = getXML('recipes');
	var recipeDATA  = Array();
	
	recipeDATA['recipe']   		= '';
	recipeDATA['groupid'] 		= '';
	recipeDATA['group'] 		= '';
	recipeDATA['description']  	= '';
	recipeDATA['nutrients'] 	= '';
	recipeDATA['price'] 		= '';
	
	$(recipesDATA).find('RECIPE').each(function(){									   													 
		if(id==$(this).attr('id'))	{
			recipeDATA['recipe'] 		= $(this).attr('recipe');
			recipeDATA['groupid'] 		= $(this).attr('groupid');
			recipeDATA['group'] 		= $(this).attr('group');
			recipeDATA['description']	= $(this).attr('description');
			recipeDATA['nutrients']		= $(this).attr('nutrients');
			recipeDATA['price']			= $(this).attr('price');
		}
	});	

	return recipeDATA;
}

function quantityORDER(recipe,quantity)	{
	
	var orderDATA  = getXML('order');
	var orderXML   = $.parseXML( orderDATA);
	
	$(orderXML).find('orderitem').each(function(){									   													 
		if(recipe==$(this).attr('recipe'))	{
			var groupid = $(this).attr('groupid');
			if(quantity==0)	{		
				$(this).remove();
				updateMAX(groupid,'remove');
			}	else	{
					$(this).attr('quantity',quantity);
				}
		}
			
	});
	
	var orderDATA = (new XMLSerializer()).serializeToString(orderXML);
	$("#XML_order").text(orderDATA);

	return;	
}

function submitORDER()	{
	
// CHANGE BEG: 20120315-1930 bsears - dine change via Tami: prevent ordering after stop time variable
	var ewf 		= ewfObject();
	var d 			= new Date();
	var hour 		= d.getHours();
	var minute		= d.getMinutes();
	var time 		= hour+''+minute;
	msg('hour: '+hour+' minute: '+minute+' time: '+time+' stop: '+ewf.dineStop);
//	if (time>=ewf.dineStop)	{
//		unlockMENU();
//		$("#XMLCALL_error").text('ERROR: Current Time Is Past Dine Stop Time');
//		return;
//	}
// CHANGE END: 20120315-1930 bsears - dine change via Tami: prevent ordering after stop time variable
	
	// BUILD ORDER
	
	var patroninfo 	= getXML('patroninfo');
	var patronmenu 	= getXML('patronmenu');
	var orderDATA   = getXML('order');
	
	var mrn			= '';
	var roomid		= '';
	var menuid		= '';
	var mealid		= '';
	var mealdate	= '';
	var uniquehash	= '';
	var id			= '';
	var numservings	= '';
	
	$(patroninfo).find('PATRON').each(function(){
		mrn 		= $(this).attr('mrn');
	});
	
	$(patroninfo).find('ROOM').each(function(){
		roomid 		= $(this).attr('id');
	});
	
	$(patroninfo).find('MEAL').each(function(){
// CHANGE BEG: 20120207-0900 bsears - dine issue via Tami: correct the menu id value for place order
//		mealid 		= $(this).attr('menuid');
		mealid 		= $(this).attr('id');
// CHANGE END: 20120207-0900 bsears - dine issue via Tami: correct the menu id value for place order
		mealdate 	= $(this).attr('date');
	});
	
	$(patronmenu).find('MEAL').each(function(){
		menuid 		= $(this).attr('menuid');
		uniquehash 	= $(this).attr('uniquehash');
	});
	
	var selectionsXML   = '';
	
	selectionsXML = selectionsXML+'<?xml version="1.0" encoding="ISO-8859-1" ?>';	
	selectionsXML = selectionsXML+'<PATRONSELECTIONS>';
	selectionsXML = selectionsXML+'<MRN>'+mrn+'</MRN>';
	selectionsXML = selectionsXML+'<ROOMID>'+roomid+'</ROOMID>';
	selectionsXML = selectionsXML+'<MENUID>'+menuid+'</MENUID>';
	selectionsXML = selectionsXML+'<MEALID>'+mealid+'</MEALID>';
	selectionsXML = selectionsXML+'<MEALDATE>'+mealdate+'</MEALDATE>';
	selectionsXML = selectionsXML+'<UNIQUEHASH>'+uniquehash+'</UNIQUEHASH>';
	selectionsXML = selectionsXML+'<SELECTIONS>';
	
	$(orderDATA).find('orderitem').each(function(){									   													 
		id 			= $(this).attr('recipe');
		numservings = $(this).attr('quantity');
		selectionsXML = selectionsXML+'<RECIPE id="'+id+'" numservings="'+numservings+'" ></RECIPE>';
	});	
	
	selectionsXML = selectionsXML+'</SELECTIONS>';
	selectionsXML = selectionsXML+'</PATRONSELECTIONS>';	
	
	$("#XML_selections").text(selectionsXML);
	
// CHANGE BEG: 20120201-0200 bsears - add place order xml call	
	//return;		// remove to implement submit order
// CHANGE END: 20120201-0200 bsears - add place order xml call
	
	// SUBMIT ORDER
	
// CHANGE BEG: 20120228-1130 bsears - dine change via Tami: define dine xml call ports as variables and retrieve dynamically at call time	
	var ewf 		= ewfObject();
// CHANGE END: 20120228-1130 bsears - dine change via Tami: define dine xml call ports as variables and retrieve dynamically at call time
	
// CHANGE BEG: 20120201-0200 bsears - add place order xml call
//	var type 		= 'PATRONMENU';							// ??
//	var patron		= ':8008/hsws/select_patron_menu';  	// ??
//	var guest	 	= ':8008/hsws/select_guest_menu';  		// ??
	var type 		= 'SELECTMENU';	
// CHANGE BEG: 20120228-1130 bsears - dine change via Tami: define dine xml call ports as variables and retrieve dynamically at call time
//	var patron	 	= ':8008/hsws/select_patron_menu?xmlstring=';  
//	var guest 		= ':8009/hsws/order_guest_tray?xmlstring=';	
	var patron	 	= ':'+ewf.select_patron_menu+'/hsws/select_patron_menu?xmlstring=';  
	var guest 		= ':'+ewf.order_guest_tray+'/hsws/order_guest_tray?xmlstring=';
// CHANGE END: 20120228-1130 bsears - dine change via Tami: define dine xml call ports as variables and retrieve dynamically at call time
// CHANGE END: 20120201-0200 bsears - add place order xml call
	
	var submenu = $("#K_submenu").attr("class");	
	if (submenu=='visitorsmenu')
// CHANGE BEG: 20120201-0200 bsears - add place order xml call
//		var args = guest+'?'+selectionsXML;
//		else
//			var args = patron+'?'+selectionsXML;
		var args = guest+selectionsXML;
		else
			var args = patron+selectionsXML;
// CHANGE END: 20120201-0200 bsears - add place order xml call
	
	$("#XMLCALL_submitorder").text(args);
	
	var xmlString = ajaxXML(type,args);
	
	$("#XML_submitorder").text(xmlString);

	return;	
}

// CHANGE BEG: 20120216-0900 bsears - dine issue via Tami: implement unlock menu
function unlockMENU()	{
	
	var submenu = $("#K_submenu").attr("class");
	if (submenu=='visitorsmenu')
		return;
	
	var patroninfo 	= getXML('patroninfo');
// CHANGE BEG: 20120228-1130 bsears - dine change via Tami: define dine xml call ports as variables and retrieve dynamically at call time	
	var ewf 		= ewfObject();
// CHANGE END: 20120228-1130 bsears - dine change via Tami: define dine xml call ports as variables and retrieve dynamically at call time
	
	var type 		= 'UNLOCKMENU';
// CHANGE BEG: 20120228-1130 bsears - dine change via Tami: define dine xml call ports as variables and retrieve dynamically at call time
//	var patron	 	= ':8010/hsws/unlock_patron_menu';
	var patron	 	= ':'+ewf.unlock_patron_menu+'/hsws/unlock_patron_menu';
// CHANGE END: 20120228-1130 bsears - dine change via Tami: define dine xml call ports as variables and retrieve dynamically at call time
	var mrn 		= 'mrn=';
	var mealid 		= 'mealid=';
	var mealdate	= 'mealdate=';
	
	$(patroninfo).find('PATRON').each(function(){
		mrn 		= mrn+$(this).attr('mrn');
	});	
	
	$(patroninfo).find('MEAL').each(function(){
		mealid 		= mealid+$(this).attr('id');
		mealdate 	= mealdate+$(this).attr('date');
	});	
	
	var args = patron+'?'+mrn+'&'+mealid+'&'+mealdate;	
	
	$("#XMLCALL_unlockmenu").text(args);
	
	var xmlString = ajaxXML(type,args);
	
	$("#XML_unlockmenu").text(xmlString);
	
	return;
}
// CHANGE END: 20120216-0900 bsears - dine issue via Tami: implement unlock menu

