//
// poswer functions
//

function checkPower() {
    
    msg('Checking power state of TV...');
    
    var onoff = 'OFF';
    
    var version = $("#K_version").text();
    if (version=='PROD')    {
        var TVController = Nimbus.getTVController();
        if (TVController) {
            var power = TVController.getPower();
            if (power==true)    
                onoff = 'ON';
        }
    }
    
    msg('TV is '+onoff);

    return onoff;
}

function setVolume() {
    
    msg('Setting Volume');
    
    var TVController = Nimbus.getTVController();
    if (TVController) {
        TVController.setVolume(65);
        //Super handy undocumented Nimbus API.  We use this to overwrite the switch-on volume in Enseo setting page
        TVController.setSwitchOnVolume(65);
    }
    return;
}

function getPower() {
    
    msg('Getting power state');
    
    var TVController = Nimbus.getTVController();
    if (TVController && TVController.getPower()) {
        $("#K_power").text('ON');
        return true
    }
    $("#K_power").text('OFF');
    return false;
}

function powerON() {
    
    msg('Setting power state to ON');
    
    var TVController = Nimbus.getTVController();
    if (TVController != null) {
        TVController.setPower(true);
        setVolume();
        $("#K_power").text('ON');
    } else {
            msg('ERROR: No Valid TV Controller');
        }
        
    return;
}

function powerOFF() {
    
    msg('Setting power state to OFF');
    
    var TVController = Nimbus.getTVController();
    if (TVController != null) {
        setVolume();
        TVController.setPower(false);
        $("#K_power").text('OFF');
    } else {
            msg('ERROR: No Valid TV Controller');
        }
        
    return;
}

function togglePower() {
    getPower();
    var power = $("#K_power").text();
    
    var TVController = Nimbus.getTVController();
    if (TVController != null) {
        if (power=='ON') {
            powerOFF();
            return false;
        } else {
                powerON();
                return false;
            }
    }
}